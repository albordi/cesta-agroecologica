import * as Font from 'expo-font';
import { useState, useContext, useEffect, useCallback } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet } from 'react-native';
import { useFonts } from 'expo-font';

import * as SplashScreen from 'expo-splash-screen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import UserContextProvider, { UserContext } from './store/context/usercontext';
import ProductsContextProvider from './store/context/productscontext';
import AuthContextProvider, { AuthContext } from './store/context/authcontext';
import DeliveryContextProvider from './store/context/deliverycontext';
import ConsumerGroupContextProvider, { ConsumerGroupContext } from './store/context/consumergroupcontext';
import OrderContextProvider from './store/context/ordercontext';

import { AuthStack } from './navigation/AuthStack';
import { AuthenticatedStack } from './navigation/AuthenticatedStack';
import { ConsumerStack } from './navigation/ConsumerStack';
import GLOBALS from './Globals';

// SplashScreen.preventAutoHideAsync();

const Stack = createNativeStackNavigator();
const Tab = createMaterialTopTabNavigator();

function Navigation() {
  console.log('[Navigation componente started]');
  const authCtx = useContext(AuthContext);
  const userCtx = useContext(UserContext);
  // const consumerGroupCtx = useContext(ConsumerGroupContext);

  // console.log('[App] Consumer Group Conext', consumerGroupCtx);

  // console.log('[App Nav] user', userCtx.user);
  // console.log('[App Nav] authCtx', authCtx);

  // console.log('[App] authCtx.isAuthenticated ====>', authCtx.isAuthenticated);
  // console.log('[App] UserCtx.user.role', userCtx.user.role);

  // if(!userCtx.user.role) {
  //   console.log('[App] ATTENTION - The user needs to have role');
  // }

  return (
    <NavigationContainer>
      {/* <AuthenticatedStack /> */}
      {!authCtx.isAuthenticated && <AuthStack />}
      {authCtx.isAuthenticated &&
        userCtx.user.role === GLOBALS.USER.ROLE.ADMIN && <AuthenticatedStack />}
      {authCtx.isAuthenticated &&
        userCtx.user.role === GLOBALS.USER.ROLE.CONSUMER && <ConsumerStack />}
    </NavigationContainer>
  );
}

export default function App() {
  console.log('[App] app started');
  const [showApp, setShowApp] = useState(true);

  const [fontsLoaded] = useFonts({
    'Inter-Black': require('./assets/fonts/InterBlack900.otf'),
    CheapPotatoesBlackThin: require('./assets/fonts/CheapPotatoesBlackThin.ttf'),
    Antropos: require('./assets/fonts/antrf___.ttf'),
    Cambria: require('./assets/fonts/Cambria.ttf'),
    HVDPosterClean: require('./assets/fonts/HVD_Poster_Clean.ttf'),
  });

  const onDone = () => {
    setShowApp(true);
  };
  const onSkip = () => {
    setShowApp(true);
  };

  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }

  // if (!showApp) {
  //   console.log('Intro Slides');
  //   return <IntroSlider onDone={onDone} onSkip={onSkip} />;
  // }

  return (
    <>
      <StatusBar style='auto' />
      <UserContextProvider>
        <AuthContextProvider>
          <ConsumerGroupContextProvider>
            <ProductsContextProvider>
              <DeliveryContextProvider>
                <OrderContextProvider>
                  <Navigation />
                </OrderContextProvider>
              </DeliveryContextProvider>
            </ProductsContextProvider>
          </ConsumerGroupContextProvider>
        </AuthContextProvider>
      </UserContextProvider>
    </>
  );
}

const styles = StyleSheet.create({});
