export default {
  APP: {
    NAME: 'Cesta Agroecológica',
    INITIALSCREEN_TEXT:
      'xxxxx.',
  },
  BASKET: {
    TYPE: {
      LITTLE: 'little',
      NORMAL: 'normal',
      SPECIAL: 'special',
      DONATION: 'donation',
      EXTRAPRODUCTS: 'extraproducts'
    }
  },

  USER: {
    ATTRIBUTE: {
      ROLE: 'role',
      EMAIL: 'email',
      AUTH_ID: 'authId',
      BALANCE: 'balance',
      CHECKBALANCE: 'checkBalance',
    },
    ROLE: {
      CONSUMER: 'consumer',
      ADMIN: 'admin',
    },
  },
  COLLECTION: {
    GROUPS: 'groups',
    USERS: 'users',
    DELIVERIES: 'deliveries',
    PRODUCTS: 'products',
    ORDERS: 'orders',
  },
  SUB_COLLECTION: {
    PAYMENTS: 'payments',
  },
  FORMAT: {
    // DEFAULT_DATE: 'dd/MM/yyyy',
    DEFAULT_DATE: 'dd/MMM/yyyy',
    DEFAULT_TIME: 'HH:mm',
    DEFAULT_DATE_TIME: 'dd/MM/yyyy HH:mm',
    DD_MM: 'dd/MM',
  },
  ORDER: {
    STATUS: {
      OPENED: 'opened',
      COMPLETED: 'completed',
      CANCELED: 'canceled',
    },
    ATTRIBUTE: {
      PAYMENT_ID: 'paymentId',
      PAYMENT_STATUS: 'paymentStatus',
    },
  },
  PAYMENT: {
    STATUS: {
      OPENED: 'opened',
      COMPLETED: 'completed',
    },
  },
  RECEIPTFILE: {
    SIZE: 2000000,
  },
};
