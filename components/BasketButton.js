import { useState } from 'react';
import { View, Text, StyleSheet, Image, Pressable } from 'react-native';
import { GlobalStyles } from '../constants/styles';
import { Label3 } from './UI/Labels';
import GLOBALS from '../Globals';

function BasketButton({
  buttonTitle,
  image,
  basketDetails,
  basketPrice,
  onPress,
  pressed,
  style,
}) {

  return (
    <Pressable
      // style={[styles.button, pressed ? styles.buttonSelected : null]}
      style={[styles.button, style]}
      android_ripple={{ color: GlobalStyles.colors.buttonHoverEffect }}
      onPress={onPress}
    >
      <Image source={image} style={styles.basketImage} />
      <View style={styles.detailsContainer}>
        <View style={styles.titleContainer}>
          <View style={pressed ? styles.fullBullet : styles.emptyBullet}></View>
          <Text style={styles.buttonTitle}>{buttonTitle}</Text>
          <Image
            source={require('../assets/images/click-icon-vector-12.jpg')}
            style={{ width: 30, height: 30 }}
          />
        </View>
        {basketDetails && (
          <Label3 style={styles.basketDetails}>{basketDetails}</Label3>
        )}
        {basketPrice && <Text style={styles.price}>R$ {basketPrice}</Text>}
      </View>
      {/* 
      <View style={styles.priceContainer}>
        {basketPrice && <Text style={styles.price}>R$ {basketPrice}</Text>}
      </View> */}
    </Pressable>
  );
}

export default BasketButton;

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginVertical: 6,
    paddingVertical: 8,
    alignItems: 'center',
    borderRadius: 10,
    elevation: 4,
    // paddingRight: 10,
    // paddingHorizontal: 5
  },
  // detailsContainer: {
  //   marginRight:10,
  // },
  buttonSelected: {
    // backgroundColor: 'rgba(135, 78, 64, 0.1)',
    backgroundColor: GlobalStyles.colors.primary,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center'
  },
  buttonTitle: {
    fontFamily: 'Cambria',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5,
    marginRight: 5,
    color: GlobalStyles.colors.secondary,
  },
  emptyBullet: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: GlobalStyles.colors.secondary,
    borderRadius: 20,
    marginRight: 10
  },  
  fullBullet:{
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: GlobalStyles.colors.secondary,
    borderRadius: 20,
    marginRight: 10,
    backgroundColor: GlobalStyles.colors.secondary,
  },

  basketImage: {
    margin: 5,
    width: 80,
    height: 80,
    borderRadius: 10,
  },
  basketDetails: { 
    marginRight: 100, 
  },
  // priceContainer: {
  //   flex: 1,
  // },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 6,
    // textAlign: 'right',
    color: GlobalStyles.colors.secondary,
  },
});
