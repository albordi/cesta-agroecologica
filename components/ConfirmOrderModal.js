import { useEffect, useState } from 'react';
import { View, ScrollView, Text, StyleSheet, Modal, Image } from 'react-native';
import PrimaryButton from './PrimaryButton';
import { GlobalStyles } from '../constants/styles';
import { Label1, Label2, Label3, Label8 } from './UI/Labels';
import RadioButton from './RadioButton';
import ImagePic from './ImagePic';
import StoreImageOnFirebase from './StoreImageOnFirebase';
import qrcode from '../assets/images/QRCode.png';
import { Ionicons } from '@expo/vector-icons';
import * as Clipboard from 'expo-clipboard';

const paymentOptions = [
  {
    label: 'Dinheiro',
    information:
      'à vista, no momento da entrega (se puder, deixe o dinheiro separado para facilitar o troco).',
    value: 'dinheiro',
  },
  {
    label: 'PIX',
    information:
      'pelo número de celular: 18 99752-9678 (Nome do PIX: Naélia Cristina Forato).',
    value: 'pix',
  },
];

const deliveryLocalOptions = [
  {
    label: 'Dracena',
    subLabel: 'Retirada na casa da Naná (Av. Rui Barbosa, 2292)',
    value: 'Dracena',
  },
  {
    label: 'Nova Guataporanga',
    subLabel: 'Rua Brasil, altura do número 380.',
    value: 'NovaGuataporanga',
  },
  {
    label: 'Em domicílio',
    subLabel: '',
    value: 'Em domicílio',
  },
  // { label: 'Panorama', subLabel: 'Entrega a domicílio.', value: 'Panorama' },
  // { label: 'Paulicéia', subLabel: 'Entrega a domicílio.', value: 'Paulicéia' },
  // {
  //   label: 'Tupi Paulista',
  //   subLabel: 'Entrega a domicílio.',
  //   value: 'TupiPaulista',
  // },
];

const ConfirmOrderModal = ({ showModal, confirmOrder, cancelOrder, order }) => {
  // console.log('[Confirmation Order Modal]', JSON.stringify(order, null, 2));
  const [payment, setPayment] = useState({ receipt: '', option: null });
  const [deliveryLocal, setDeliveryLocal] = useState(null);

  // console.log('[Confirm Order Modal ] order', JSON.stringify(order, null, 2));

  useEffect(() => {
    order.deliveryLocal ? setDeliveryLocal(order.deliveryLocal) : null;
    order.payment ? setPayment(order.payment) : { receipt: null, option: null };
  }, []);

  const handleDeliveryLocalSelectOption = (value) => {
    console.log(value);
    // const deliveryLocalAux = { ...deliveryLocal };
    deliveryLocalAux = value;
    setDeliveryLocal(deliveryLocalAux);
  };

  const handlePaymentSelectOption = (value) => {
    console.log(value);
    const paymentAux = { ...payment };
    paymentAux.option = value;
    setPayment(paymentAux);
  };

  const imagePicker = async () => {
    console.log('[Confirm Order Modal] image picker');
    try {
      const image = await ImagePic();
      // console.log(image);
      const paymentAux = { ...payment };
      const imageUrl = await StoreImageOnFirebase(
        'paymentreceipt',
        image.assets[0].uri
      );
      paymentAux.receipt = imageUrl;
      setPayment(paymentAux);
    } catch (error) {
      console.log(
        'Houve um erro ao carregar o comprovante de pagamento',
        error
      );
    }
  };

  return (
    <Modal
      style={styles.modal}
      animationType='slide'
      transparent={true}
      visible={showModal !== null}
    >
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <ScrollView style={styles.scrollView}>
            <Label1
              style={{
                marginLeft: 10,
                color: GlobalStyles.colors.textColor,
              }}
            >
              Aqui está a relação de itens que escolheu.
            </Label1>
            <View style={styles.basketProductsContainer}>
              <View>
                {order.normalBasket && order.normalBasket.length > 0 && (
                  <View>
                    <Label3>Cesta Normal({order.normalBasketPrice})</Label3>
                    <Text style={{ marginLeft: 10 }}>
                      Brinde: {order.productInAbundance.name}
                    </Text>
                    <Text style={{ marginLeft: 10 }}>
                      {order.normalBasket.map((product, index) => {
                        return product.name + ', ';
                      })}
                    </Text>
                  </View>
                )}

                {order.littleBasket && order.littleBasket.length > 0 && (
                  <View>
                    {/* <Label3>Dentro da cesta</Label3> */}
                    <Label3>
                      Cesta Pequena (R${order.littleBasketPrice.toFixed(2)})
                    </Label3>
                    <Text style={{ marginLeft: 10 }}>
                      Brinde: {order.productInAbundance.name}
                    </Text>
                    <Text style={{ marginLeft: 10 }}>
                      {order.littleBasket.map((product, index) => {
                        return product.name + ', ';
                      })}
                    </Text>
                  </View>
                )}
                {order.extraProducts && <Label3>Itens adicionais</Label3>}

                {order.extraProducts &&
                  order.extraProducts.map((extraProduct, index) => {
                    return (
                      <View key={index}>
                        <Text style={{ marginLeft: 10 }}>
                          {extraProduct.quantity} {extraProduct.name}-R${' '}
                          {(extraProduct.price * extraProduct.quantity).toFixed(
                            2
                          )}
                        </Text>
                      </View>
                    );
                  })}
                {order.specialBasketProducts && (
                  <View>
                    <Label3>
                      Cesta Especial({order.specialBasketPrice.toFixed(2)})
                    </Label3>
                    <Text style={{ marginLeft: 10 }}>
                      {order.specialBasketProducts.map(
                        (specialProduct, index) => {
                          return specialProduct.name + ', ';
                        }
                      )}
                    </Text>
                  </View>
                )}
                {order.donationBasketProducts && (
                  <Label3>
                    Cesta de Doação({order.donationBasketPrice.toFixed(2)})
                  </Label3>
                )}
                {/* <Label1>Totalização do pedido - {order.totalOrderPrice }</Label1> */}
              </View>
            </View>

            <Label1 style={{ marginLeft: 10 }}>
              Onde entregamos a cesta para você?
            </Label1>
            <View style={styles.deliveryLocalContainer}>
              <RadioButton
                style={{ marginLeft: 10 }}
                options={deliveryLocalOptions}
                selectedOption={deliveryLocal}
                onSelect={handleDeliveryLocalSelectOption}
              />
            </View>
            <View style={styles.paymentContainer}>
              <Label8>
                VALOR TOTAL: R$ {order.totalOrderPrice.toFixed(2)}
              </Label8>
              <Label2>Como você prefere fazer o pagamento?</Label2>
              <View style={styles.selectionContainer}>
                <RadioButton
                  options={paymentOptions}
                  selectedOption={payment.option}
                  onSelect={handlePaymentSelectOption}
                  orientation='row'
                  subLabel={true}
                />
                {payment.option === 'pix' && (
                  <View>
                    <View style={styles.qrcodeContainer}>
                      <Image source={qrcode} style={styles.qrcodeImage} />
                      <View>
                        <Text>Copiar QRCode</Text>
                      </View>
                      <Ionicons
                        style={styles.updateIcon}
                        name='copy'
                        size={24}
                        color={GlobalStyles.colors.secondary}
                        onPress={() =>
                          Clipboard.setStringAsync(
                            '00020126360014BR.GOV.BCB.PIX0114+55189975296785204000053039865802BR5922Naelia Cristina Forato6009SAO PAULO621405107pHc9qmaI06304786B'
                          )
                        }
                      />
                    </View>

                    <View style={styles.receiptContainer}>
                      <PrimaryButton onPress={imagePicker}>
                        Anexar comprovante
                      </PrimaryButton>
                      {payment.receipt ? (
                        <Image
                          source={{ uri: payment.receipt }}
                          style={{ width: 80, height: 80 }}
                        />
                      ) : null}
                    </View>
                  </View>
                )}
              </View>
              {/* <View style={styles.selectionContainer}>
                <RadioButton
                  options={paymentOptions}
                  selectedOption={payment.option}
                  onSelect={handlePaymentSelectOption}
                />
                {payment.option === 'pix' && (
                  <View style={styles.receiptContainer}>
                    <PrimaryButton onPress={imagePicker}>
                      Anexar comprovante
                    </PrimaryButton>
                    {payment.receipt ? (
                      <Image
                        source={{ uri: payment.receipt }}
                        style={{ width: 80, height: 80 }}
                      />
                    ) : null}
                  </View>
                )}
              </View> */}
            </View>
          </ScrollView>
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonContainer}>
              <PrimaryButton
                onPress={() => confirmOrder(payment, deliveryLocal)}
              >
                Confirmar
              </PrimaryButton>
            </View>
            <View style={styles.buttonContainer}>
              <PrimaryButton
                onPress={() => cancelOrder()}
                // style={GlobalStyles.mb10}
              >
                Voltar
              </PrimaryButton>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default ConfirmOrderModal;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)', // Semi-transparent background
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%', // Cover the entire screen
    zIndex: 1,
  },
  modalContent: {
    marginTop: 45,
    width: '90%',
    padding: 5,
    backgroundColor: GlobalStyles.colors.backGroundColor,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.19,
    shadowRadius: 5.62,
    elevation: 6,
    borderRadius: 10,
    borderWidth: 1,
  },
  scrollView: {
    height: '90%',
  },
  basketProductsContainer: {
    margin: 5,
    padding: 5,
    backgroundColor: 'white',
    borderColor: GlobalStyles.colors.primary,
    borderRadius: 5,
    borderWidth: 1,
  },
  deliveryLocalContainer: {
    margin: 5,
    padding: 5,
    backgroundColor: 'white',
    borderColor: GlobalStyles.colors.primary,
    borderRadius: 5,
    borderWidth: 1,
  },
  radioButtonContainer: {
    flexDirection: 'row',
  },
  selectionContainer: {
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'center',
  },
  paymentContainer: {
    margin: 5,
    padding: 5,
    backgroundColor: 'white',
    borderColor: GlobalStyles.colors.primary,
    borderRadius: 5,
    borderWidth: 1,
  },
  qrcodeContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  qrcodeImage: {
    margin: 5,
    width: 60,
    height: 60,
    borderRadius: 10,
  },
  receiptContainer: {
    // flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center',
    // borderWidth: 2,
  },
  buttonContainer: {
    flex: 1,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
