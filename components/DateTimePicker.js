import { useState } from "react";
import { View } from "react-native";
import DateTimePickerModal from "@react-native-community/datetimepicker";

function DateTimePicker() {
  const [date, setDate] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState();
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    setSelectedDate(date);
    hideDatePicker();
  };

  const onChange = (event, selectedDate) => {
    setShowDate(false);

    // on cancel set date value to previous date
    if (event?.type === "dismissed") {
      setDate(date);
      return;
    }
    setDate(selectedDate);
  };

  return (
      <DateTimePickerModal
        value={date}
        date={selectedDate}
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        // onChange={onChange}
      />
  );
}

export default DateTimePicker;
