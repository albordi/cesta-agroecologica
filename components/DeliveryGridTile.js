import { Pressable, View, Text, StyleSheet, Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { GlobalStyles } from '../constants/styles';

function DeliveryGridTile({ title, subTitle, color, onPress }) {
  return (
    <View style={[styles.gridItem]}>
      <Pressable
        android_ripple={{ color: '#ccc' }}
        style={ styles.button }
        onPress={onPress}
      >
        {/* <Ionicons
          style={styles.updateIcon}
          name='create'
          size={24}
          color={GlobalStyles.colors.secondary}
          // onPress={console.log('pressed')}
        /> */}
        <View style={[styles.innerContainer, { backgroundColor: color }]}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subTitle}>{subTitle}</Text>
        </View>
      </Pressable>
    </View>
  );
}

export default DeliveryGridTile;

const styles = StyleSheet.create({
  gridItem: {
    flex: 1,
    margin: 16,
    height: 100,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },

  button: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.primary,
  },
  innerContainer: {
    flex: 1,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    color: GlobalStyles.colors.secondary,
  },
  subTitle: {
    fontWeight: 'bold',
    fontSize: 12,
    textAlign: 'center',
  },
  updateIcon: {
    marginTop: 5,
    marginRight: 5,
    textAlign: 'right',
  },
});
