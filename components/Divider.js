import { Text, View, StyleSheet } from 'react-native';

const Divider = () => {
  return (
      <View style={styles.divider}>
      </View>
  );
};

export default Divider;

const styles = StyleSheet.create({

  divider: {
    alignSelf: 'center',
    width: '80%', // Reduce the divider to 80% width
    height: 2,
    backgroundColor: 'gray',
    marginVertical: 10,
  },
});
