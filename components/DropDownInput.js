import { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { GlobalStyles } from '../constants/styles';

function DropDownInput({ itemsToShow, selectedItem, onSelectItemHandle }) {
  const [selectedProduct, setSelectedProduct] = useState(selectedItem);

  useEffect(() => {
    setSelectedProduct(selectedItem);
  }, [selectedItem]);

  return (
    <View style={styles.container}>
      <Picker
        style={styles.picker}
        selectedValue={selectedProduct ? selectedProduct.name : ''}
        onValueChange={(itemValue, itemIndex) => {
          // Assuming itemValue is the product name, update selectedProduct accordingly
          const selected = itemsToShow.find(
            (product) => product.name === itemValue
          );
          setSelectedProduct(selected);
          onSelectItemHandle(selected); // Pass the selected product to the parent component
        }}
        dropdownIconColor={GlobalStyles.colors.secondary}
      >
        <Picker.Item label='Selecione um produto' value='' />
        {itemsToShow.map((product) => (
          <Picker.Item
            key={product.name}
            label={product.name}
            value={product.name}
          />
        ))}
      </Picker>
    </View>
  );
}

export default DropDownInput;

const styles = StyleSheet.create({
  container: {
    height: 40,
    borderBottomColor: '#ddb52f',
    borderBottomWidth: 2,
    borderRadius: 5,
    backgroundColor: '#fff',
  },
  picker: {
    marginTop: -5,
    color: '#333',
  },
});
