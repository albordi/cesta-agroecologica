import { View, StyleSheet, Text } from 'react-native';
import { GlobalStyles } from '../constants/styles';
import PrimaryButton from './PrimaryButton';

function ErrorOverlay({ message, onConfirm }) {
  return (
    <View style={styles.errorOverlayContainer}>
      <Text styles={[styles.title, styles.text]}>Ocorreu um erro</Text>
      <Text styles={[styles.message, styles.text]}>{message}</Text>
      <PrimaryButton onPress={onConfirm}>OK</PrimaryButton>
    </View>
  );
}

export default ErrorOverlay;

const styles = StyleSheet.create({
  errorOverlayContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
  },
  text: {
    textAlign: 'center',
    marginBottom: 8,
  },
  title: {
    color: GlobalStyles.colors.secondary,
    fontSize: 20,
    fontWeight: 'bold',
  },
  message: {
    textAlign: 'center',
    color: GlobalStyles.colors.secondary,
    fontSize: 8,
  },
});
