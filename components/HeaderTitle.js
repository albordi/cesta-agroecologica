import { View, Text, StyleSheet } from 'react-native';
import { GlobalStyles } from '../constants/styles';

const HeaderTitle = ({ style, title }) => {
  return (
    <View style={styles.headerTitle}>
      {/* <Text>{title}</Text> */}
      <Text style={[style, styles.title]}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  headerTitle: {
    // backgroundColor: 'red',
  },
  title: {
    // flex: 1,
    fontFamily: 'Antropos',
    fontSize: 18,
    color: GlobalStyles.colors.secondary,
    flexWrap: 'wrap',
    // width: '100%',
  },
});

export default HeaderTitle;
