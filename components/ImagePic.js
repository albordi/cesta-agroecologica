import * as ImagePicker from 'expo-image-picker';

const ImagePic = async (multipleImages) => {
  // const multiImages = multipleImages || false;
  let result = '';
  try {
    result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All, // We can specify whether we need only Images or Videos
      allowsMultipleSelection: multipleImages || false,
      // allowsEditing: true,
      aspect: [2, 2],
      quality: 0.5, // 0 means compress for small size, 1 means compress for maximum quality
      // selectionLimit: 1,
    });
  } catch (error) {
    console.error('Error picking image:', error);
  }
  return result;
};

export default ImagePic;
