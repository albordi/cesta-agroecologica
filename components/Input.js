// Custom text or number input
import { TextInput, StyleSheet, View } from 'react-native';
import { GlobalStyles } from '../constants/styles';

function Input({
  style,
  label,
  value,
  isValid,
  textInputConfig,
  secureTextEntry,
  placeholder
}) {
  // console.log(textInputConfig);
  return (
    <View style={styles.inputContainer}>
      <TextInput
        textAlignVertical='top'
        {...textInputConfig.secureTextEntry!==true ?  multiline=true : null}
        value={value}
        style={[styles.input, style, !isValid && styles.invalidInput]}
        {...textInputConfig}
        placeholder={placeholder}
      />
    </View>
  );
}

export default Input;

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
  },
  input: {
    padding: 6,
    borderBottomColor: '#ddb52f',
    borderBottomWidth: 2,
    color: GlobalStyles.colors.textColor,
    fontSize: 16,
    backgroundColor: 'white',
    borderRadius: 6,
  },
  invalidLabel: {
    color: GlobalStyles.colors.error,
  },
  invalidInput: {
    backgroundColor: GlobalStyles.colors.error,
    color: 'black',
    opacity: 0.8,
  },
});
