import {
  View,
  Text,
  Pressable,
  StyleSheet,
  Platform,
  Image,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { GlobalStyles } from '../constants/styles';
import fullBasketImage from '../assets/images/fullbasket.png';

function OrderButton({ userOrder, onClick }) {
  // console.log(
  //   'Order Button',
  //   JSON.stringify(userOrder.order, null, 2)
  // );

  let string = '';
  if (userOrder.order) {
    userOrder.order.littleBasket ? (string = string + '+ Cesta pequena') : null;
    userOrder.order.normalBasket ? (string = string + '+ Cesta normal') : null;
    userOrder.order.specialBasketProducts
      ? (string = string + '+ Cesta especial')
      : null;
    userOrder.order.donationBasketProducts
      ? (string = string + '+ Cesta doação')
      : null;
    userOrder.order.extraProducts
      ? (string = string + '+ Produtos extras')
      : null;
  }

  return (
    <Pressable onPress={() => onClick(userOrder.userId, userOrder.order)}>
      <View style={styles.userCardContainer}>
        <View style={styles.nameContainer}>
          <Text style={styles.title}>{userOrder.userName}</Text>
          {string && <Image source={fullBasketImage} style={styles.basketImage} />}
        </View>
        <View style={styles.subTitle}>
          <Text style={styles.text}>{string}</Text>
        </View>
      </View>
    </Pressable>
  );
}

export default OrderButton;

const styles = StyleSheet.create({
  userCardContainer: {
    // flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    marginHorizontal: 20,
    marginVertical: 4,
    padding: 5,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  title: {
    fontSize: 20,
  },
  iconsContainer: {
    flexDirection: 'row',
  },
  basketImage: {
    width: 30,
    height: 30,
  },
  nameContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
