import { View, Text, StyleSheet, Modal, Pressable, Image } from 'react-native';
import { Label1 } from './UI/Labels';
import { GlobalStyles } from '../constants/styles';

const OrderConfirmedModal = ({ showModal, onPress }) => {
  console.log('[Order Confirmed Modal] started');

  return (
    <Modal
      style={styles.modal}
      animationType='slide'
      transparent={true}
      visible={showModal !== null}
    >
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <Pressable style={styles.button} onPress={onPress}>
            <Image
              source={require('../assets/images/ConfirmedOrderImage.png')}
              style={{ width: '100%', height: '90%', borderRadius: 20 }}
            />
            <Pressable style={styles.goBackButton}>
              <Pressable style={styles.button} onPress={onPress}>
                <Label1>Voltar</Label1>
              </Pressable>
            </Pressable>
          </Pressable>
          {/* <Text>Somos Gratos</Text>
          <View style={styles.buttonContainer}>
            <Button onPress={() => showPreviousScreen()}>OK</Button>
          </View> */}
        </View>
      </View>
    </Modal>
  );
};

export default OrderConfirmedModal;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)', // Semi-transparent background
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%', // Cover the entire screen
    zIndex: 1,
  },
  modalContent: {
    width: '100%',
    height: '58%',
    padding: 5,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.19,
    shadowRadius: 5.62,
    elevation: 6,
    borderRadius: 10,
    borderWidth: 1,
  },
  scrollView: {
    height: '80%',
  },
  modalView: {
    // height: '40%',
  },
  basketProductsContainer: {
    margin: 5,
    padding: 5,
    // backgroundColor: 'yellow',
    borderRadius: 5,
    borderWidth: 1,
  },
  deliveryLocalContainer: {
    margin: 5,
    padding: 5,
    // backgroundColor: 'yellow',
    borderRadius: 5,
    borderWidth: 1,
  },
  radioButtonContainer: {
    flexDirection: 'row',
  },
  selectionContainer: {
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'center',
  },
  paymentContainer: {
    margin: 5,
    padding: 5,
    // backgroundColor: 'red',
    borderRadius: 5,
    borderWidth: 1,
  },
  receiptContainer: {
    // flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center',
    // borderWidth: 2,
  },
  buttonContainer: {
    flex: 1,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  goBackButton: {
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
    marginTop: -25,
  },
});
