import { View, Text, Image, StyleSheet, Platform } from 'react-native';
import { Label1, Label2, Label3 } from './UI/Labels';
import { GlobalStyles } from '../constants/styles';

const PersonalCard = ({ data }) => {
  return (
    <View style={styles.screenContainer}>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={data.photo} />
      </View>
      <View style={styles.details}>
        <Label1 style={GlobalStyles.boldText}>{data.name}</Label1>
        <Label2>{data.description}</Label2>
        {/* <Text>{data.email}</Text> */}
        {/* <Text>Telefone</Text> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // backgroundColor: 'white',
    marginHorizontal: 2,
    marginVertical: 4,
    padding: 5,
    borderRadius: 8,
    // elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  imageContainer: {
    width: '35%',
    alignSelf: 'center',
  },
  image: {
    width: 90,
    height: 120,
    borderRadius: 15,
  },
  details: { flexDirection: 'column', width: '80%' },
});

export default PersonalCard;
