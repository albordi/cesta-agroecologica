import React, { useState, useEffect } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import PagerView from 'react-native-pager-view';
import logo from '../assets/images/logo.png';

const AUTOMATIC_TRANSITION_INTERVAL = 5000;

const PhotoCarousel = ({ images }) => {
  const [currentPage, setCurrentPage] = useState(0);
  const [imageToShow, setImageToShow] = useState();

  // console.log('Photo Carousel', JSON.stringify(images, null, 2));
  // console.log('[Image to show]',imageToShow);

  useEffect(() => {
    if (images) {
      // setImageToShow(images[1]);
      const interval = setInterval(() => {
        const nextPage = (currentPage + 1) % images.length;
        // console.log('trocou', nextPage);
        // console.log('trocou', currentPage);

        setCurrentPage(nextPage);
        setImageToShow(images[currentPage]);
      }, AUTOMATIC_TRANSITION_INTERVAL);
      return () => clearInterval(interval);
    }
  }, [currentPage]);

  return (
    <View style={styles.componentContainer}>
      {/* <PagerView style={styles.viewPager} initialPage={0}>
        {images.map((image, index) => {
          return (
            <View style={styles.page} key={index}>
              <Image
                source={{ uri: 'data:image/jpeg;base64,' + image }}
                style={{ width: 200, height: 200 }}
              />
              <Text>First page</Text>
              <Text>Swipe ➡️</Text>
            </View>
          );
        })}
      </PagerView> */}
      <View style={styles.imageContainer}>
        <Image source={{ uri: imageToShow }} style={styles.image} />
      </View>
    </View>
  );
};

export default PhotoCarousel;

const styles = StyleSheet.create({
  componentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // marginHorizontal: 40,
  },
  imageContainer: {
    // marginHorizontal: 20,
    flex: 1,
    height: 400,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.48,
    shadowRadius: 11.95,
    elevation: 18,
  },
  image: {
    width: 330,
    height: 196,
    borderRadius: 10,
  },
  viewPager: {
    flex: 1,
  },
  page: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

{
  /* <PagerView style={{ flex: 1 }} initialPage={0}>
      {photos.map((photo, index) => (
        <View key={index} style={{ flex: 1 }}>
          <Image source={{uri: photo}} />
        </View>
      ))}
    </PagerView> */
}
