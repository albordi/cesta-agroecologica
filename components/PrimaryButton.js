import { View, Text, StyleSheet, Pressable } from "react-native";
import { GlobalStyles } from '../constants/styles';

function PrimaryButton(props) {

  const stylePressed = [styles.buttonInnerContainer, styles.pressed, props.style]


  return (
    <View style={styles.buttonOuterContainer}>
      <Pressable
        style={({ pressed }) =>
          pressed
            ? stylePressed
            : [styles.buttonInnerContainer, props.style]
        }
        onPress={props.onPress}
        android_ripple={{ color: GlobalStyles.colors.buttonHoverEffect }}
      >
        <Text style={[styles.buttonText, props.style]}>{props.children}</Text>
      </Pressable>
    </View>
  );
}

export default PrimaryButton;

const styles = StyleSheet.create({
  buttonOuterContainer: {
    borderRadius: 8,
    margin: 4,
    overflow: "hidden",
  },
  buttonInnerContainer: {
    backgroundColor: GlobalStyles.colors.tertiary,
    paddingVertical: 8,
    paddingHorizontal: 16,
    elevation: 2,
  },
  buttonText: {
    fontFamily: 'Antropos',
    color: 'white',
    textAlign: "center",
  },
  /* For IOS*/
  pressed: {
    opacity: 0.75,
  },
});
