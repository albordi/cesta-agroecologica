import { useEffect, useState } from 'react';
import {
  View,
  Text,
  TextInput,
  FlatList,
  StyleSheet,
  Image,
  Platform,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { useNavigation } from '@react-navigation/native';

import { GlobalStyles } from '../constants/styles';

function ProductList({ products, onChangeMaxProdHandle }) {
  const [productsAux, setProductsAux] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    const newProducts = products.map((product) => ({ ...product }));
    setProductsAux(newProducts);
  }, [products]);

  const onChangeQuantityHandler = (productId, value) => {
    console.log('[Product List On Change value]', value);
    if (/^\d*$/.test(value)) { // Allow only numeric values
      const numericValue = parseInt(value, 10); // Convert string to number
      const newProductsAux = [...productsAux];
      const index = newProductsAux.findIndex((obj) => obj.id === productId);
      newProductsAux[index].availableProducts = numericValue || 0;
      if (newProductsAux[index].availableProducts < 0) {
        newProductsAux[index].availableProducts = 0;
        return;
      }
      setProductsAux(newProductsAux);
      onChangeMaxProdHandle(productId, newProductsAux[index].availableProducts);
    }
  };

  function renderProductItem(itemData) {
    return (
      <View style={styles.productContainer}>
        <View style={styles.details}>
          <Image
            style={styles.image}
            source={{ uri: itemData.item.imageUrl }}
          />
          <View>
            <Text style={styles.productName}>
              {itemData.item.name.toUpperCase()}
            </Text>
          </View>
        </View>

        <View style={styles.quantityContainer}>
          <View style={styles.textInputContainer}>
            <Ionicons
              name='remove'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() =>
                onChangeQuantityHandler(
                  itemData.item.id,
                  (itemData.item.availableProducts - 1).toString()
                )
              }
            />
            <TextInput
              value={itemData.item.availableProducts.toString()}
              keyboardType='numeric'
              onChangeText={(value) =>
                onChangeQuantityHandler(itemData.item.id, value)
              }
              style={styles.textInput}
            />
            <Ionicons
              name='add'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() =>
                onChangeQuantityHandler(
                  itemData.item.id,
                  (itemData.item.availableProducts + 1).toString()
                )
              }
            />
          </View>
        </View>
      </View>
    );
  }

  return (
    <View>
      <FlatList
        style={styles.flatListContainer}
        data={productsAux}
        renderItem={renderProductItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

export default ProductList;

const styles = StyleSheet.create({
  productContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 4,
    paddingVertical: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  details: {
    width: '70%',
    flexDirection: 'row',
  },
  productName: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  quantityContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  textInputContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  textInput: {
    textAlign: 'center',
    color: GlobalStyles.colors.secondary,
    fontSize: 20,
    borderColor: '#ccc',
    borderWidth: 1,
    width: 60, // Adjust the width as needed
  },
  flatListContainer: {
    flexGrow: 1,
  },
  image: {
    width: 60,
    height: '100%',
  },
});
