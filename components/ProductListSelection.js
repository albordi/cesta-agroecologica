import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  Platform,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { GlobalStyles } from '../constants/styles';

function ProductListSelection({ products, onChangeQuantity }) {
  // console.log('[Product List Selection Component]', JSON.stringify(products, null, 2));

  function renderProductItem(itemData) {
    return (
      <View style={styles.productContainer}>
        <View style={styles.details}>
          <Image
            style={styles.image}
            source={{ uri: itemData.item.imageUrl }}
          />
          <View>
            <Text style={styles.productName}>
              {itemData.item.name.toUpperCase()}{' ('}
              {itemData.item.price.toFixed(2)}{')'}
            </Text>
          </View>
        </View>
        <View style={styles.quantityContainer}>
          <View style={styles.textInputContainer}>
            <Ionicons
              name='remove'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => onChangeQuantity(itemData.item.id, -1)}
            />
            <Text style={styles.textInput}>
              {itemData.item.quantity.toString()}
            </Text>
            <Ionicons
              name='add'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => onChangeQuantity(itemData.item.id, 1)}
            />
          </View>
        </View>
      </View>
    );
  }

  return (
    <View>
      <FlatList
        style={styles.flatListContainer}
        data={products}
        renderItem={renderProductItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

export default ProductListSelection;

const styles = StyleSheet.create({
  productContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 4,
    // paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  details: {
    width: '70%',
    flexDirection: 'row',
  },
  productName: {
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  productInformation: {
    fontSize: 10,
    paddingHorizontal: 15,
    // backgroundColor: 'red'
  },
  quantityContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    // paddingHorizontal: 10,
  },
  textInputContainer: {
    // flex: 1,
    // paddingHorizontal: 15,
    // borderWidth: 1,
    // width: 10,
    // borderColor: 'black',
    alignItems: 'center',
    flexDirection: 'row',
    textAlign: 'left',
    justifyContent: 'flex-end',
  },
  textInput: {
    textAlign: 'center',
    color: GlobalStyles.colors.secondary,
    fontSize: 20,
  },
  flatListContainer: {
    flexGrow: 1,
    // height: '65%',
  },
  image: {
    width: 60,
    height: '100%',
  },
});
