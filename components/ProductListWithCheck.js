import Checkbox from 'expo-checkbox';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  StyleSheet,
  Image,
  Platform,
} from 'react-native';

import { GlobalStyles } from '../constants/styles';
import Popup from './UI/Popup';

function ProductListWithCheck({ products, checkHandler }) {
  // console.log('[Product List With Check Component]');
  // console.log('[Product List Component]', products);

  function renderProductItem(itemData) {
    // console.log(
    //   '[Product List component] item data',
    //   JSON.stringify(itemData, null, 2)
    // );
    return (
      <View style={styles.productContainer}>
        <View style={styles.details}>
          <Image
            style={styles.image}
            source={{ uri: itemData.item.imageUrl }}
          />
          <View>
            <Text style={styles.productName}>
              {itemData.item.name.toUpperCase()}
            </Text>
          </View>
          <View>
            {itemData.item.description && (
              <Popup message={itemData.item.description} />
              // <Image
              //   source={require('../assets/images/novoproduto.png')}
              //   style={styles.icon}
              // />
            )}
          </View>
          {itemData.item.isNew && (
            <Image
              source={require('../assets/images/icons/novoproduto.png')}
              style={styles.icon}
            />
          )}
          {itemData.item.isVegan && (
            <Image
              source={require('../assets/images/icons/vegan.png')}
              style={styles.icon}
            />
          )}
          {itemData.item.isGlutenFree && (
            <Image
              source={require('../assets/images/icons/semgluten.png')}
              style={styles.icon}
            />
          )}
           {itemData.item.isProcessed && (
            <Image
              source={require('../assets/images/icons/produtoprocessado.png')}
              style={styles.icon}
            />
          )}
        </View>
        <View style={styles.checkboxContainer}>
          <Checkbox
            style={styles.checkbox}
            // value={itemData.item.available}
            value={itemData.item.checked}
            onValueChange={() => checkHandler(itemData.item.id)}
            color={
              itemData.item.checked ? GlobalStyles.colors.secondary : undefined
            }
          />
        </View>
      </View>
    );
  }

  return (
    <View>
      <FlatList
        style={styles.flatListContainer}
        // data={products}
        data={products.sort((a, b) =>
          a.name.toLowerCase().localeCompare(b.name.toLowerCase())
        )}
        renderItem={renderProductItem}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={true}
      />
    </View>
  );
}

export default ProductListWithCheck;

const styles = StyleSheet.create({
  productContainer: {
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 4,
    paddingVertical: 5,
    backgroundColor: GlobalStyles.colors.backGroundColor,
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  details: {
    // width: '70%',
    flexDirection: 'row',
    // justifyContent: 'flex-start',
    // alignItems: 'flex-start',
    // borderColor: 'red',
    // borderWidth: 1,
    // backgroundColor: 'red',
  },
  productName: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    // borderColor: 'red',
    // borderWidth: 1,
  },
  flatListContainer: {
    flexGrow: 1,
    // height: '65%',
  },
  image: {
    width: 60,
    height: '100%',
    borderRadius: 5,
  },
  checkboxContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginRight: 10,
  },
  checkbox: {
    borderColor: GlobalStyles.colors.secondary,
    borderWidth: 1,
  },
  icon: {
    width: 30,
    height: '100%',
  },
});
