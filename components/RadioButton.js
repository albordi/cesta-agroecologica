import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { GlobalStyles } from '../constants/styles';
import { Label1, Label2, Label3 } from './UI/Labels';

const RadioButton = ({ options, selectedOption, onSelect, orientation }) => {
  console.log('[Radio Button Started] selected option', options);
  return (
    <View
      style={{ flexDirection: orientation, justifyContent: 'space-around' }}
    >
      {options.map((option, index) => (
        <View key={index}>
          <TouchableOpacity
            key={option.value}
            style={styles.radioButton}
            onPress={() => onSelect(option.value)}
          >
            <View
              style={[
                styles.radioCircle,
                {
                  backgroundColor:
                    option.value === selectedOption
                      ? GlobalStyles.colors.primary
                      : '#ffffff',
                },
              ]}
            >
              {option.value === selectedOption && (
                <View style={styles.selectedRb} />
              )}
            </View>
            <View>
              <Label2 style={styles.radioText}>{option.label}</Label2>
            </View>
            {/* <View style={{width: '50%', flexDirection: 'row'}}>
              <Text >{option.information}</Text>
            </View> */}
          </TouchableOpacity>
          {option.subLabel && <Text>{option.information}</Text>}
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  radioButton: {
    flexDirection: 'row',
    alignItems: 'center',
    // marginVertical: 10,
  },
  radioCircle: {
    width: 15,
    height: 15,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: GlobalStyles.colors.secondary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedRb: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: GlobalStyles.colors.primary,
  },
  radioText: {
    marginLeft: 10,
    fontSize: 16,
  },
});

export default RadioButton;
