import { View, ActivityIndicator, StyleSheet } from 'react-native';
import { GlobalStyles } from '../constants/styles';

function Spinner() {
  return(
    <View style={styles.spinnerContainer}>
      <ActivityIndicator size={'large'} color={GlobalStyles.colors.secondary} />
    </View>
  )
}

export default Spinner;

const styles = StyleSheet.create({
  spinnerContainer: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // padding: 24
  }

});