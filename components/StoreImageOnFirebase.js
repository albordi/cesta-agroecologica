import firebase from 'firebase/compat/app';

const StoreImageOnFirebase = async (path, imageUri) => {
  console.log('[Store Image On Firebase]', imageUri);
  let imageUrl;
  try {
    if (imageUri) {
      const response = await fetch(imageUri);
      const blob = await response.blob();
      // const blob = uriToBlob(imageUri);
      const filename = imageUri.substring(imageUri.lastIndexOf('/') + 1);
      // console.log(filename);

      const ref = firebase.storage().ref().child(`${path}/${filename}`);
      await ref.put(blob);
      console.log('Image uploaded successfully');
      let imageUrl = await ref.getDownloadURL();
      console.log('Image uploaded successfully', imageUrl);
      return imageUrl;
    }
  } catch (error) {
    console.error('Error uploading image:', error);
    return 'Imagem não foi armazenada';
  }
};

export default StoreImageOnFirebase;