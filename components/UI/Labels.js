import { Text, StyleSheet } from 'react-native';
import { GlobalStyles } from '../../constants/styles';

export function Label1(props) {
  return <Text style={[styles.textLabel, props.style]}>{props.children}</Text>;
}

export function Label2(props) {
  return <Text style={[styles.textLabel2, props.style]}>{props.children}</Text>;
}

export function Label3(props) {
  return <Text style={[styles.textLabel3, props.style]}>{props.children}</Text>;
}

export function Label4(props) {
  return <Text style={[styles.textLabel4, props.style]}>{props.children}</Text>;
}

export function Label5(props) {
  return <Text style={[styles.textLabel5, props.style]}>{props.children}</Text>;
}

export function Label6(props) {
  return <Text style={[styles.textLabel6, props.style]}>{props.children}</Text>;
}

export function Label7(props) {
  return <Text style={[styles.textLabel6, props.style]}>{props.children}</Text>;
}

export function Label8(props) {
  return <Text style={[styles.textLabel8, props.style]}>{props.children}</Text>;
}
const styles = StyleSheet.create({
  textLabel: {
    // fontFamily: 'Roboto_200Thin',
    fontSize: 20,
    // color: 'black',
    color: GlobalStyles.colors.secondary,
    // textAlign: "left",
  },
  textLabel2: {
    fontSize: 15,
    color: GlobalStyles.colors.secondary,
    textAlign: 'left',
    // marginLeft: 20,
  },
  textLabel3: {
    fontSize: 15,
    color: GlobalStyles.colors.secondary,
  },
  textLabel4: {
    fontFamily: 'Antropos',
    fontSize: 18,
    // fontWeight: 600,
    color: GlobalStyles.colors.secondary,
  },
  textLabel5: {
    fontFamily: 'Cambria',
    fontSize: 18,
    fontWeight: 'bold',
    color: GlobalStyles.colors.secondary,
  },
  textLabel6: {
    fontFamily: 'Cambria',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffffff',
  },
  textLabel7: {
    fontFamily: '',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffffff',
  },
  textLabel8: {
    fontFamily: '',
    fontSize: 18,
    fontWeight: 'bold',
    color: GlobalStyles.colors.tertiary,
  },
});
