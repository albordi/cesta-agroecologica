import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Modal, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { GlobalStyles } from '../../constants/styles';

const Popup = ({message}) => {
  // console.log('[popup component] started');

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={toggleModal}>
        <Ionicons name="information-circle-outline" size={24} color={GlobalStyles.colors.secondary} />
      </TouchableOpacity>

      <Modal visible={isModalVisible} animationType="slide" transparent>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text>{message}</Text>
            <TouchableOpacity onPress={toggleModal}>
              <Text style={styles.closeButton}>Fechar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default Popup;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    // marginHorizontal: 15,
  },
  modalContent: {
    backgroundColor: GlobalStyles.colors.backGroundColor,
    padding: 20,
    borderRadius: 10,
    elevation: 5,
    marginHorizontal: '5%',
    // zIndex: 3,
  },
  closeButton: {
    marginTop: 10,
    color: GlobalStyles.colors.secondary,
  },
});