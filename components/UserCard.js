import { View, Text, Pressable, StyleSheet, Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { GlobalStyles } from '../constants/styles';

function UserCard({ user, onUpdateUserHandle, onDeleteUserHandle }) {
  return (
    <Pressable onPress={() => onUpdateUserHandle(user)}>
      <View style={styles.userCardContainer}>
        <Text style={styles.text}>{user.name}</Text>
        <View style={styles.iconsContainer}>
          <View style={styles.iconContainer}>
            <Pressable onPress={() => onUpdateUserHandle(user)}>
              <Ionicons
                name='create'
                size={24}
                color={GlobalStyles.colors.secondary}
              />
            </Pressable>
          </View>
          {/* <View style={styles.iconContainer}>
          <Pressable onPress={() => onDeleteUserHandle(user.id)}>
            <Ionicons
              name='trash'
              size={24}
              color={GlobalStyles.colors.secondary}
            />
          </Pressable>
        </View> */}
        </View>
      </View>
    </Pressable>
  );
}

export default UserCard;

const styles = StyleSheet.create({
  userCardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    marginHorizontal: 20,
    marginVertical: 4,
    padding: 5,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  text: {
    fontSize: 20,
  },
  iconsContainer: {
    flexDirection: 'row',
  },
});
