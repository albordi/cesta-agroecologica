export const GlobalStyles = {
  colors: {
    primary: '#f2c08d',
    // secondary: '#8e2e30',
    secondary: '#874e40',
    tertiary: '#658a3c',
    backGroundColor: '#f7f9e1',
    littleBasketBackground: '#f0cfb0',
    normalBasketBackground: '#f1e5bb',
    donationBasketBackground: '#d6dab7',
    specialBasketBackground: '#dbd1ae',
    quaternary: '#f8931f',
    quinternary:'#86672c',
    alert: '#8e2e30',
    error: '#F75151',
    backGroundColorHeader: '#f2c08d',
    headerTitleColor: '#2d6535',
    buttonHoverEffect: '#96785E',
    textColor: '#753734',
    inactiveIconColor: '#C8D6E0',
    activeIconColor: '#FC6111',
    color1: '#f5cda4',
    color2: '#d4c37b',
    color3: '#c8a73d'
  },
  standardLabel: {
    fontSize: 32,
    color: '#753734',
    textAlign: "left",
  },
  boldText: {
    fontWeight: 'bold',
  },
  normalText: {
    fontSize: 16,
    color: 'black', // Customize the color if needed
  },
  centerText: {
    textAlign: 'center',
  },
  m10:{
    margin: 10,
  },
  mt10:{
    marginTop: 10,
  },
  ml10:{
    marginLeft: 10,
  },
  mb10:{
    marginBottom: 10,
  },
  ml20:{
    marginLeft: 20,
  },
  mh10:{
    marginHorizontal: 10,
  }
};