// import { users } from './InitialUsers';
const { products } = require('./dummy-data');

const admin = require('firebase-admin');
// const { users } = require('./InitialUsers.js');

// Prod DB
// const serviceAccount = require('../../cestascooperflorabarao-firebase-adminsdk-kg42n-264249460c.json');

// Dev DB
const serviceAccount = require('../../cestaagroecologica-dev-firebase-adminsdk-mk8y0-a15c0fc2da.json');
// const databaseURL = 'https://cestascooperflorabarao-dev-default-rtdb.asia-southeast1.firebasedatabase.app';

// // Bordi database ==============
// const serviceAccount = require('../../cestas-cooperflora-dev2-firebase-adminsdk-f8nor-6bf231082b.json');

// const databaseURL =
//   'https://cestas-cooperflora-dev2-default-rtdb.firebaseio.com';
// =============================

console.log('Iniciando a criação do banco de dados do app Rancho Y Iara');

// const db = admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   databaseURL,
// });

const db = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});


console.log('Inclusão dos produtos extras no banco de dados.');
console.log(products);
products.map((product) => {
  console.log(product);
  db.firestore()
    .collection('products')
    .add(product)
    .then((docRef) => {
      console.log('Produto criado com sucesso: ', product);
      console.log('ID: ', docRef.id);
    })
    .catch((error) => {
      console.error('Erro ao incluir o produto: ', error);
    });
});

// db.firestore()
//   .collection('groups')
//   .get()
//   .then((docs) => {
//     docs.forEach((doc) => {
//       console.log(doc.data());
//     });
//   });

console.log('Criação do banco de dados com sucesso.');