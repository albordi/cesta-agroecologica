// import Delivery from '../models/delivery';

module.exports.users = [
  {
    name: 'Rancho Y Iara',
    city: 'Campinas',
    email: 'ranchoyiara@gmail.com',
    name: 'Organizador',
    role: 'organizer',
  },
  {
    email: 'albordignon@gmail.com',
    name: 'André',
    role: 'consumer',
    consumerGroupId: 'vhvp5xf4PNESoy0qR2Yx',
    balance: 0,
  },
  {
    email: 'souzasmimi@gmail.com',
    name: 'Yasmin',
    role: 'consumer',
    consumerGroupId: 'vhvp5xf4PNESoy0qR2Yx',
    balance: 0,
  },
  {
    email: 'luana.matallogg@gmail.com',
    name: 'Luana',
    role: 'consumer',
    consumerGroupId: 'vhvp5xf4PNESoy0qR2Yx',
    balance: 0,
  },
  {
    email: 'na.ambar@gmail.com',
    name: 'Natalia',
    role: 'consumer',
    consumerGroupId: 'vhvp5xf4PNESoy0qR2Yx',
    balance: 0,
  },
  {
    email: 'gra.monteiro0@gmail.com',
    name: 'Gra',
    role: 'consumer',
    consumerGroupId: 'vhvp5xf4PNESoy0qR2Yx',
    balance: 0,
  },
  {
    email: 'viniciuscregaco@gmail.com',
    name: 'Vinicius',
    role: 'consumer',
    consumerGroupId: 'vhvp5xf4PNESoy0qR2Yx',
    balance: 0,
  },
  {
    email: 'mauricioesposito.eco@gmail.com',
    name: 'Maurício',
    role: 'consumer',
    consumerGroupId: 'vhvp5xf4PNESoy0qR2Yx',
    balance: 0,
  },
];

module.exports.products = [
  {
    name: 'Abóbora',
    description: 'Abóbora',
    price: 4.5,
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://www.proativaalimentos.com.br/image/cache/catalog/img_prod/beneficios-da-abobora-para-saude-18990-640-427[1]-1000x1000.jpg',
  },
  {
    name: 'Abobrinha',
    price: 4.5,
    description: 'Abobrinha',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://images.squarespace-cdn.com/content/v1/5b8edfa12714e508f756f481/1538594048521-EYBDQQ788NHG7LMPXIHB/abobrinha-brasileira.jpg?format=1000w',
  },
  {
    name: 'Alface',
    price: 4.5,
    description: 'Alface',
  isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://st.depositphotos.com/1276052/1414/i/600/depositphotos_14146812-stock-photo-fresh-green-lettuce-isolated.jpg',
  },
  {
    name: 'Tomate',
    price: 8.0,
    description: 'Tomate',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://static4.depositphotos.com/1017505/320/i/600/depositphotos_3201839-stock-photo-three-tomatoes.jpg',
  },
  {
    name: 'Bolacha',
    price: 8.0,
    description: 'Bolacha mineira',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://receitasdetudo.com/wp-content/uploads/2022/08/bolacha-mineira.webp',
  },
  {
    name: 'Cenoura',
    price: 12.0,
    description: 'Cenoura',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://static9.depositphotos.com/1642482/1148/i/450/depositphotos_11489080-stock-photo-fresh-carrots.jpg',
  },
  {
    name: 'Arroz Branco 5kg',
    price: 36.5,
    description: 'Alface',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://pt.petitchef.com/imgupl/recipe/arroz-branco--336801p670894.jpg',
  },
  {
    name: 'Banana nanica',
    price: 4.5,
    description: 'Banana',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://cdn.awsli.com.br/2500x2500/18/18885/produto/102523124/6ba7072878.jpg',
  },
  {
    name: 'Berinjela',
    price: 4.5,
    description: 'Berinjela',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://cdn.shoppub.io/cdn-cgi/image/w=1000,h=1000,q=80,f=auto/cenourao/media/uploads/produtos/foto/8eb5ac81576efile.png',
  },

  {
    name: 'Jilo',
    price: 2.5,
    description: 'Jilo',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://www.proativaalimentos.com.br/image/cache/catalog/img_prod/jilo-beneficios[1]-1000x1000.jpg',
  },
  {
    name: 'Batata',
    price: 7.0,
    description: 'Batata',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://static.mundoeducacao.uol.com.br/mundoeducacao/conteudo_legenda/01325ea5fd7fd4ecab7e209393bf6188.jpg',
  },
  {
    name: 'Doce de Leite',
    price: 14.0,
    description: 'Doce de leite',
    isNew: true,
    isGlutenFree: true,
    isVegan: false,
    imageUrl:
      'https://images.tcdn.com.br/img/img_prod/1159315/doce_pastoso_leite_com_cafe_93_1_b466a3cc51a3c7700652fcd3dcc9d9e6.jpeg',
  },
  {
    name: 'Abacate',
    price: 4.5,
    description: 'Abacate',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://s2-ge.glbimg.com/atIp_axsQ9CQci2-CQ6zGIqLTUU=/1200x/smart/filters:cover():strip_icc()/s.glbimg.com/es/ge/f/original/2014/11/20/abacate.jpg',
  },
  {
    name: 'Cebolinha',
    price: 3.5,
    description: 'Cebolinha',
    isNew: false,
    isGlutenFree: false,
    isVegan: false,
    imageUrl:
      'https://ekkogreen.com.br/wp-content/uploads/2021/03/cebolinha_02-768x512.jpeg',
  },
  // { name: 'Beringela', price: 4.5 },
  // { name: 'Bolinho de chuva com canela', price: 10.0 },
  // { name: 'Bolacha Maisena', price: 10.0 },
  // { name: 'Broa pequena de fubá', price: 10.0 },
  // { name: 'Cachaça carvalho', price: 40.0 },
  // { name: 'Cachaça prata', price: 40.0 },
  // { name: 'Cachaça amburana', price: 40.0 },
  // { name: 'Café Cecília moído', price: 18.0 },
  // { name: 'Café Cecília grãos', price: 18.0 },
  // { name: 'Café Terra de Sabores', price: 18.0 },
  // { name: 'Chicória', price: 4.5 },
  // { name: 'Cheiro Verde', price: 4.5 },
  // { name: 'Coentro', price: 4.5 },
  // { name: 'Couve', price: 4.5 },
  // { name: 'Doce de leite em pasta', price: 12.0 },
  // { name: 'Feijão 1kg', price: 15.0 },
  // { name: 'Flocão de milho não transgênico', price: 7.0 },
  // { name: 'Frango caipira', price: 40.0 },
  // { name: 'Frango caipirão', price: 45.0 },
  // { name: 'Geleia de uva', price: 13.0 },
  // { name: 'Geleia de amora', price: 13.0 },
  // { name: 'Mandioca com casca', price: 4.5 },
  // { name: 'Mandioca sem casca', price: 6.0 },
  // { name: 'Mel 300 mL', price: 13.0 },
  // { name: 'Molho de tomate orgânico', price: 23.0 },
  // { name: 'Nozinho de coco', price: 10.0 },
  // { name: 'Pão branco', price: 10.0 },
  // { name: 'Pão integral', price: 10.0 },
  // { name: 'Pão de mandioca', price: 10.0 },
  // { name: 'Queijo meia cura grande', price: 30.0 },
  // { name: 'Queijo Fresco P', price: 15.0 },
  // { name: 'Queijo Fresco G', price: 28.0 },
  // { name: 'Rosca com recheio de goiabada', price: 15.0 },
  // { name: 'Suco de Goiaba 1L', price: 15.0 },
  // { name: 'Suco de Maçã 1L', price: 15.0 },
  // { name: 'Suco de Manga 1L', price: 15.0 },
  // { name: 'Suco de uva 1L', price: 15.0 },
  // { name: 'Suco de Uva 1,5L', price: 18.0 },
  // { name: 'Tempero Caseiro', price: 10.0 },
  // { name: 'Tempero Caseiro de salsinha', price: 10.0 },
  // { name: 'Tempero Caseiro de coentro', price: 10.0 },
  // { name: 'Tempero Caseiro de pimenta dedo de moça', price: 10.0 },
];

// export const DELIVERIES = [
//   new Delivery('d1', 'Cebolinha', '#f5428d'),
//   new Delivery('d2', 'Alface', '#f54242'),
//   new Delivery('d3', 'Abacate', '#f5a442'),
//   new Delivery('d4', 'Rúcula', '#f5d142'),
//   new Delivery('d5', 'Cebolinha', '#368dff'),
//   new Delivery('d6', 'Salsinha', '#41d95d'),
//   new Delivery('d7', 'Mandioca', '#9eecff'),
//   new Delivery('d8', 'Alface', '#b9ffb0'),
//   new Delivery('d9', 'Cebolinha', '#ffc7ff'),
//   new Delivery('d10', 'Mandioca', '#47fced'),
// ];
