// https://stackoverflow.com/questions/66665969/expo-app-environments-for-dev-uat-and-production
import Constants from 'expo-constants';

const ENV = {
  dev: {
    FirebaseConfig: {
      apiKey: "AIzaSyB3C7-LwO8GrDlPK8F9rFxjP3GsTEQr6j4",
      authDomain: "cestaagroecologica-dev.firebaseapp.com",
      projectId: "cestaagroecologica-dev",
      storageBucket: "cestaagroecologica-dev.appspot.com",
      messagingSenderId: "712254310150",
      appId: "1:712254310150:web:d1aa939dafb37fbb95c087"
    },
  },
  prod: {
    FirebaseConfig: {
      apiKey: "AIzaSyAiwhsZwGWWqAsBidqARHbyKy-4WbyCXdA",
      authDomain: "cestaagrocologica.firebaseapp.com",
      projectId: "cestaagrocologica",
      storageBucket: "cestaagrocologica.appspot.com",
      messagingSenderId: "476885063059",
      appId: "1:476885063059:web:5aa1f714382a6c3fca0466"
    },
  },
}; 

// const getEnvVars = (env = Constants.manifest.releaseChannel) => {
const getEnvVars = (env = Constants.expoConfig.releaseChannel) => {
  if (__DEV__ || env === 'dev') {
    console.log('Dev environment');
    return ENV.dev;
  }
  console.log('Prod environment');
  return ENV.prod;
};

export default getEnvVars;