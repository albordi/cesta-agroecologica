import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  getReactNativePersistence,
  initializeAuth,
} from 'firebase/auth/react-native';
import firebase from 'firebase/compat/app';
import 'firebase/compat/storage';
import { initializeApp } from 'firebase/app';
import { getStorage } from 'firebase/storage';
import { getFirestore } from 'firebase/firestore';
import getEnvVars from './env';

// Initialize Firebase

// const FirebaseConfig = {
//   apiKey: 'AIzaSyBTP1hiIhHhHrmX1CuLjO5zZYw1zQWhAWo',
//   authDomain: 'ranchoiyara.firebaseapp.com',
//   projectId: 'ranchoiyara',
//   storageBucket: 'ranchoiyara.appspot.com',
//   messagingSenderId: '679748049955',
//   appId: '1:679748049955:web:8946c80e59964eec612596',
// };

const { FirebaseConfig } = getEnvVars();

if (!firebase.apps.length) {
  firebase.initializeApp(FirebaseConfig);
}

const app = initializeApp(FirebaseConfig);
const authentication = initializeAuth(app, {
  persistence: getReactNativePersistence(AsyncStorage),
});
const storage = getStorage(app);
const db = getFirestore(app);
// const authentication = getAuth(app);

export { app, db, storage, authentication };

// For more information on how to access Firebase in your project,
// see the Firebase documentation: https://firebase.google.com/docs/web/setup#access-firebase
