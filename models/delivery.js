class Delivery {
  constructor(id, title, color, baseProduct, price, date, deliveryFee,extraProducts, ordersLimitDate) {
    this.id = id;
    this.title = title;
    this.color = color;
    this.baseProduct = baseProduct;
    this.price = price; 
    this.date =date; 
    this.deliveryFee = deliveryFee;
    this.extraProducts = extraProducts; 
    this.ordersLimitDate = ordersLimitDate; 
  }
}

export default Delivery;
