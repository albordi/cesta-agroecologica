import { useContext } from 'react';
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import { DeliveryTabs } from './DeliveryTabs';
import DeliveryScreen, {
  deliveryScreenOptions,
} from '../screens/Delivery/DeliveryScreen';
import OrderScreen from '../screens/Order/OrderScreen';
import ConsumerGroupScreen, {
  consumerGroupScreenOptions,
} from '../screens/ConsumerGroupScreen';
import ProductsScreen, {
  productScreenOptions,
} from '../screens/ProductsScreen';
import UsersScreen, { usersScreenOptions } from '../screens/User/UsersScreen';
import AboutScreen, { aboutScreenOptions } from '../screens/AboutScreen';

import { AuthContext } from '../store/context/authcontext';

import { GlobalStyles } from '../constants/styles';
import UIScreen, { UIScreenOptions } from '../screens/UIScreen';

const Drawer = createDrawerNavigator();

function CustomDrawerContent(props) {
  const authCtx = useContext(AuthContext);

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem label='Sair' onPress={() => authCtx.signout()} />
    </DrawerContentScrollView>
  );
}

export const DrawerNavigator = () => {
  console.log(JSON.stringify(deliveryScreenOptions, null, 2));
  return (
    <Drawer.Navigator
      screenOptions={{
        // headerShown:false
        headerStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
        },
        headerTitleStyle: {
          color: GlobalStyles.colors.secondary,
        },
        headerTintColor: GlobalStyles.colors.secondary,
        drawerActiveTintColor: GlobalStyles.colors.secondary,
        drawerInactiveTintColor: GlobalStyles.colors.secondary,
        drawerStyle: {
          backgroundColor: GlobalStyles.colors.primary,
          width: 240,
        },
      }}
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen
        name='Entregas'
        component={DeliveryScreen}
        label='Criar Entrega'
        // options={deliveryScreenOptions}
        options={deliveryScreenOptions}
      />
      <Drawer.Screen
        name='Gerenciar Grupo de Consumo'
        component={ConsumerGroupScreen}
        options={consumerGroupScreenOptions}
      />
      {/* <Drawer.Screen
        name='Criar Nova Entrega'
        component={DeliveryTabs}
        // label='Criar Entrega'
        options={{ title: 'Common Header Title' }}
      /> */}
      {/* <Drawer.Screen name='Fazer Pedido' component={DeliveryScreen} /> */}

      {/* <Drawer.Screen name='Fazer Pedido' component={OrderScreen} /> */}

      <Drawer.Screen
        name='Gerenciar Produtos'
        component={ProductsScreen}
        options={productScreenOptions}
      />
      <Drawer.Screen
        name='Gerenciar Usuários'
        component={UsersScreen}
        options={usersScreenOptions}
      />
      <Drawer.Screen
        name='Sobre o Projeto'
        component={AboutScreen}
        options={aboutScreenOptions}
      />
      <Drawer.Screen
        name='Tela de elementos de interface'
        component={UIScreen}
        options={UIScreenOptions}
      />
    </Drawer.Navigator>
  );
};
