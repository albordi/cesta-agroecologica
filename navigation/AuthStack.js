import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AuthScreen from '../screens/User/AuthScreen';
import SignupScreen from '../screens/User/SignupScreen';
import IntroSlider from '../screens/Opening/IntroSlider';
const Stack = createNativeStackNavigator();

export const AuthStack = () => {
  console.log('[Auth Nav Stack (to atuthenticate)]');
  return (
    <Stack.Navigator
    // screenOptions={{
    //   headerStyle: { backgroundColor: Colors.primary500 },
    //   headerTintColor: 'white',
    //   contentStyle: { backgroundColor: Colors.primary100 },
    // }}
    >
      {/* <Stack.Screen
        name='OpeningScreen'
        component={OpeningScreen}
        options={{ headerShown: false }}
      /> */}
      <Stack.Screen
        name='IntroSlider'
        component={IntroSlider}
        options={{ headerShown: false }}
      />
      {/* <Stack.Screen
        name='OpeningScreen1' 
        component={OpeningScreen1}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='OpeningScreen2' 
        component={OpeningScreen2}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='OpeningScreen3' 
        component={OpeningScreen3}
        options={{ headerShown: false }}
      /> */}
      <Stack.Screen
        name='AuthScreen'
        component={AuthScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SignupScreen'
        component={SignupScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};
