import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { GlobalStyles } from '../constants/styles';
import { DrawerNavigator } from './AdminDrawerNavigator';
import UserScreen, { userScreenOptions } from '../screens/User/UserScreen';
import DeliveryScreen from '../screens/Delivery/DeliveryScreen';
import OrderReportScreen, {
  orderReportScreenOptions,
} from '../screens/Reports/OrderReportScreen';
import ManageProductsScreen, {
  manageProductScreenOptions,
} from '../screens/ManageProductsScreen';
import { DeliveryTabs, deliveryTabsScreenOptions } from './DeliveryTabs';
import OrderScreen, { orderScreenOptions } from '../screens/Order/OrderScreen';
import TotalProductsByDeliveryReportScreen, {
  TotalProductsByDeliveryReportScreenOptions,
} from '../screens/Reports/TotalProductsByDeliveryReportScreen';
import UsersOrdersScreen, {
  usersOrdersScreenOptions,
} from '../screens/Order/UsersOrdersScreen';
import AboutScreen, { aboutScreenOptions } from '../screens/AboutScreen';
import UIScreen, { UIScreenOptions } from '../screens/UIScreen';
const Stack = createNativeStackNavigator();

export const AuthenticatedStack = () => {
  // console.log('[Authenticated nav stack]');
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
        },
        headerTintColor: GlobalStyles.colors.secondary,
      }}
    >
      <Stack.Screen
        name='drawer'
        component={DrawerNavigator}
        options={{ headerShown: false }} // hide the header
      />
      <Stack.Screen name='Deliveries' component={DeliveryScreen} />
      <Stack.Screen
        name='OrderReport'
        component={OrderReportScreen}
        options={orderReportScreenOptions}
      />
      <Stack.Screen
        name='TotalProductsByDeliveryReport'
        component={TotalProductsByDeliveryReportScreen}
        options={TotalProductsByDeliveryReportScreenOptions}
      />
      <Stack.Screen
        name='DeliveryTabs'
        component={DeliveryTabs}
        options={deliveryTabsScreenOptions}
      />
      <Stack.Screen
        name='UserScreen'
        component={UserScreen}
        options={userScreenOptions}
        // options={{
        //   title: 'Atualizar Pessoa Consumidora',
        //   // presentation: 'modal'
        // }}
      />
      <Stack.Screen
        name='OrderScreen'
        component={OrderScreen}
        options={orderScreenOptions}
      />
      <Stack.Screen
        name='ManageProductsScreen'
        component={ManageProductsScreen}
        options={manageProductScreenOptions}
        // options={{
        //   title: 'Adicionar Produto',
        //   // presentation: 'modal'
        // }}
      />
      <Stack.Screen
        name='UsersOrdersScreen'
        component={UsersOrdersScreen}
        options={usersOrdersScreenOptions}
      />
      <Stack.Screen
        name='AboutScreen'
        component={AboutScreen}
        options={aboutScreenOptions}
      />
      <Stack.Screen
        name='UIScreen'
        component={UIScreen}
        options={UIScreenOptions}
      />
    </Stack.Navigator>
  );
};
