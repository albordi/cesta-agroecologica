import { useContext } from 'react';
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import DeliveryScreen, {
  deliveryScreenOptions,
} from '../screens/Delivery/DeliveryScreen';
import UserScreen, { userScreenOptions } from '../screens/User/UserScreen';
import AboutScreen, { aboutScreenOptions } from '../screens/AboutScreen';
import { AuthContext } from '../store/context/authcontext';
import { GlobalStyles } from '../constants/styles';
import Header from '../components/Header';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { OrderContext } from '../store/context/ordercontext';
import { DeliveryContext } from '../store/context/deliverycontext';
import { UserContext } from '../store/context/usercontext';
import HeaderTitle from '../components/HeaderTitle';
import fullBasketImage from '../assets/images/fullbasket.png';
import emptyBasketImage from '../assets/images/emptybasket.png';

const Drawer = createDrawerNavigator();

function CustomDrawerContent(props) {
  const authCtx = useContext(AuthContext);

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem label='Sair' onPress={() => authCtx.signout()} />
    </DrawerContentScrollView>
  );
}

export const DrawerNavigator = () => {
  const orderCtx = useContext(OrderContext);
  const deliveryCtx = useContext(DeliveryContext);
  const usersCtx = useContext(UserContext);
  const navigation = useNavigation();

  // console.log(
  //   '[Consumer Drawer Navigator] order',
  //   JSON.stringify(orderCtx.order, null, 2)
  // );

  // console.log(
  //   '[Consumer Drawer Navigator] order',
  //   Object.keys(orderCtx.order).length
  // );

  const goToUserOrderScreen = () => {
    console.log('[ConsumerDrawer Navigator] go to user order screen');
    navigation.navigate('OrderScreen', {
      deliveryId: deliveryCtx.nextDelivery.id,
      userId: usersCtx.user.id,
      order: orderCtx.order,
    });
  };

  return (
    <Drawer.Navigator
      screenOptions={{
        // headerShown:false
        headerStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
        },
        headerTitleStyle: {
          color: GlobalStyles.colors.secondary,
        },
        headerTintColor: GlobalStyles.colors.secondary,
        drawerActiveTintColor: GlobalStyles.colors.secondary,
        drawerInactiveTintColor: GlobalStyles.colors.secondary,
        drawerStyle: {
          backgroundColor: GlobalStyles.colors.primary,
          width: 240,
        },
      }}
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen
        name='Entregas'
        component={DeliveryScreen}
        label='Fazer Pedido'
        // options={deliveryScreenOptions}
        options={{
          headerTitle: () => <HeaderTitle title='Entregas' />,
          headerRight: () => (
            <View>
              <TouchableOpacity
                style={{ marginRight: 15 }}
                onPress={goToUserOrderScreen}
              >
                {Object.keys(orderCtx.order).length > 0 ? (
                  <View style={{ flexDirection: 'row' }}>
                    <Image
                      source={fullBasketImage}
                      style={styles.basketImage}
                    />

                    {/* <FontAwesome5
                      style={{ zIndex: 3 }}
                      name='shopping-basket'
                      size={24}
                      color={GlobalStyles.colors.secondary}
                    /> */}
                    <View
                      style={{
                        width: 8,
                        height: 8,
                        backgroundColor: 'red',
                        borderRadius: 15,
                        position: 'absolute',
                        left: 15,
                        top: 25,
                        zIndex: 2,
                      }}
                    ></View>
                  </View>
                ) : (
                  // <FontAwesome5
                  //   name='shopping-basket'
                  //   size={24}
                  //   color={GlobalStyles.colors.secondary}
                  // />
                  <Image source={emptyBasketImage} style={styles.basketImage} />
                )}
              </TouchableOpacity>
            </View>
          ),
          headerStyle: {
            height: 60,
            backgroundColor: GlobalStyles.colors.primary,
          },
        }}
      />
      <Drawer.Screen
        name='Meus Dados'
        component={UserScreen}
        options={userScreenOptions}
      />
      <Drawer.Screen
        name='Sobre o projeto'
        component={AboutScreen}
        options={aboutScreenOptions}
      />
    </Drawer.Navigator>
  );
};

const styles = StyleSheet.create({
  basketImage: {
    width: 40,
    height: 40,
  },
});
