import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { GlobalStyles } from '../constants/styles';
import { DrawerNavigator } from './ConsumerDrawerNavigator';
import UserScreen from '../screens/User/UserScreen';
import DeliveryScreen from '../screens/Delivery/DeliveryScreen';
import { DeliveryTabs } from './DeliveryTabs';
import OrderScreen, { orderScreenOptions } from '../screens/Order/OrderScreen';
import UsersOrdersScreen from '../screens/Order/UsersOrdersScreen';
const Stack = createNativeStackNavigator();

export const ConsumerStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: GlobalStyles.colors.backGroundColorHeader,
        },
        headerTintColor: GlobalStyles.colors.secondary,
      }}
    >
      <Stack.Screen
        name='drawer'
        component={DrawerNavigator}
        options={{ headerShown: false }} // hide the header
      />
      <Stack.Screen name='Deliveries' component={DeliveryScreen} />
      <Stack.Screen
        name='OrderScreen'
        component={OrderScreen}
        options={orderScreenOptions}
      />

      <Stack.Screen name='DeliveryTabs' component={DeliveryTabs} />
      <Stack.Screen
        name='UserScreen'
        component={UserScreen}
        options={{
          title: 'Atualizar Pessoa Consumidora',
          // presentation: 'modal'
        }}
      />
    </Stack.Navigator>
  );
};
