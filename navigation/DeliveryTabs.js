import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { View } from 'react-native';
import ManageDeliveryScreen, {
  manageDeliveryScreenOptions,
} from '../screens/Delivery/ManageDeliveryScreen';
import BaseProductsBasketScreen from '../screens/Delivery/BaseProductsBasketScreen';
import DonationBasketScreen from '../screens/Delivery/DonationBasketScreen';
import SpecialBasketScreen from '../screens/Delivery/SpecialBasketScreen';
import ExtraProductsBasketScreen from '../screens/Delivery/ExtraProductsScreen';
import HeaderTitle from '../components/HeaderTitle';
import { GlobalStyles } from '../constants/styles';

const Tab = createMaterialTopTabNavigator();

export const DeliveryTabs = ({ route }) => {
  console.log('[Delivery Tabs] started', );
  return (
    <Tab.Navigator
      screenOptions={{
        // tabBarItemStyle: { width: 100 },
        tabBarLabelStyle: { color: GlobalStyles.colors.secondary, fontSize: 10, fontWeight: 'bold'  },
        // tabBarInactiveLabelStyle: { color: 'gray' },
        tabBarStyle: { backgroundColor: GlobalStyles.colors.backGroundColor },
        tabBarIndicatorStyle: {
          backgroundColor: GlobalStyles.colors.secondary,
        },
      }}

      // name='DeliveryTabs'
      // screenOptions={({ route }) => ({
      //   tabBarIcon: ({ focused, color, size }) => {
      //     // Customize tab icons as needed based on route
      //     // Access navigation parameters using route.params
      //   },
      // })}
    >
      <Tab.Screen
        name='Data e Produtos'
        component={ManageDeliveryScreen}
        initialParams={{ route: route }}
        // options={manageDeliveryScreenOptions}
      />
      <Tab.Screen name='Normal e Pequena' component={BaseProductsBasketScreen} />

      <Tab.Screen name='Doação' component={DonationBasketScreen} />
      <Tab.Screen name='Especial' component={SpecialBasketScreen} />
      <Tab.Screen
        name='Produtos Extras'
        component={ExtraProductsBasketScreen}
      />
    </Tab.Navigator>
  );
};

export const deliveryTabsScreenOptions = {
  headerTitle: () => (
    <View>
      <HeaderTitle title='Criar Nova Entrega' />
    </View>
  ),
};
