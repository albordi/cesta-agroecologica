import React from 'react';
import { StyleSheet, View, Image, Share, Button } from 'react-native';

import { GlobalStyles } from '../constants/styles';
import HeaderTitle from '../components/HeaderTitle';
import {
  Label1,
  Label2,
  Label3,
  Label4,
  Label6,
} from '../components/UI/Labels';
import PersonalCard from '../components/PersonalCard';
import { ScrollView } from 'react-native-gesture-handler';
import { Video, ResizeMode } from 'expo-av';
import PrimaryButton from '../components/PrimaryButton';
import Ionicons from '@expo/vector-icons/Ionicons';

const appJson = require('../app.json');

const authors = [
  {
    name: 'André L. Bordignon',
    email: 'albordignon@gmail.com',
    photo: require('../assets/images/andreluisbordignon.png'),
  },
  {
    name: 'Dani Carla',
    email: 'xxx',
    photo: require('../assets/images/danicarla.jpeg'),
  },
  {
    name: 'Luciana Teruel',
    email: 'xxx',
    photo: require('../assets/images/lucianateruel.png'),
  },
  {
    name: 'Naélia Forato',
    email: 'xxx',
    photo: require('../assets/images/naeliaforato.png'),
  },
];

const farmers = [
  {
    name: 'Márcia e Áureo',
    description: '',
    photo: require('../assets/images/aureomarciasmall.png'),
  },
  {
    name: 'Dona Cida',
    description: '',
    photo: require('../assets/images/donacidasmall.png'),
  },
  {
    name: 'Devanir',
    description: '',
    photo: require('../assets/images/devanirsmall.png'),
  },
  {
    name: 'Dona Maria',
    description: '',
    photo: require('../assets/images/donamariasmall.png'),
  },
  {
    name: 'Pretinha',
    description: '',
    photo: require('../assets/images/pretinhasmall.png'),
  },
  {
    name: 'Joana',
    description: '',
    photo: require('../assets/images/joana.jpg'),
  },
  {
    name: 'Noemia',
    description: '',
    photo: require('../assets/images/noemia.jpeg'),
  },
];

const appUrl =
  'https://play.google.com/store/apps/details?id=br.pro.bordignon.cestaagroecologica';

function AboutScreen() {
  console.log('[About Screen Started]');
  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: 'Cesta Agroecológica: ' + '\n' + appUrl,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('Shared with activity type of: ', result.activityType);
        } else {
          console.log('Shared');
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('Dismissed');
      }
    } catch (error) {
      console.log('Error = ', error);
    }
  };

  return (
    <ScrollView style={styles.screenContainer}>
      <Image
        style={[styles.logo, GlobalStyles.mb10]}
        source={require('../assets/images/logo.png')}
      />
      <Label4 style={[GlobalStyles.centerText, GlobalStyles.mb10]}>
        {' '}
        Cesta Agroecológica
      </Label4>
      <Label3 style={GlobalStyles.centerText}>
        Version {appJson.expo.version}
      </Label3>
      <View style={styles.summaryContainer}>
        <View style={styles.item}>
          <View style={[styles.bullet, { backgroundColor: '#86670c' }]}></View>
          <Label6>Afinal o que é agroecológico</Label6>
        </View>
        <View style={styles.item}>
          <View style={[styles.bullet, { backgroundColor: '#d04002' }]}></View>
          <Label6>Quem são nossas agricultoras</Label6>
        </View>
        <View style={styles.item}>
          <View style={[styles.bullet, { backgroundColor: '#824034' }]}></View>
          <Label6>Onde é plantada nossa comida</Label6>
        </View>
        <View style={styles.item}>
          <View style={[styles.bullet, { backgroundColor: '#f3c08d' }]}></View>
          <Label6>Nossa história (vídeo)</Label6>
        </View>
        <View style={styles.item}>
          <View style={[styles.bullet, { backgroundColor: '#658a3d' }]}></View>
          <Label6>Compartilhe o projeto com os amigos</Label6>
        </View>
        <View style={styles.item}>
          <View style={[styles.bullet, { backgroundColor: '#8e2e2f' }]}></View>
          <Label6>Contato</Label6>
        </View>
      </View>
      <View style={styles.containter01}>
        <Label4 style={GlobalStyles.mb10}>Afinal o que é agroecológico</Label4>
        <Label1 style={GlobalStyles.mb10}>
          Se por um lado a agricultura orgânica traz a ênfase para um plantio
          mais saudável, sem agrotóxicos, as platações agroecológicas seguem num
          contexto mais amplo.
        </Label1>
        <Label1 style={GlobalStyles.mb10}>
          Isso porque o cultivo agroecológico prega pela diversificação de
          plantações, respeitando o perfil biológico de cada solo e bioma.
        </Label1>
        <Label1 style={GlobalStyles.mb10}>
          A cultura agroecológica é vista por muitos como uma forma de manter a
          agricultura familiar fortalecida, valorizando os diferentes tipos de
          cultivos. Assim, além de garantir um meio ambiente mais saudável,
          oferece diversidade de alimentos e produtos naturais.{' '}
        </Label1>
        <Label1 style={GlobalStyles.mb10}>
          Outro ponto bastante mencionado nesse mercado é o fortalecimento da
          economia comunitária, fomentando o contato direto entre produtores e
          consumidores
        </Label1>
        <Image
          style={{ width: 250, height: 180, alignSelf: 'center' }}
          source={require('../assets/images/image5.png')}
        />
      </View>
      <View style={styles.containter02}>
        <Label4 style={GlobalStyles.m10}>Quem são nossas agricultoras</Label4>

        {farmers.map((farmer, index) => {
          return <PersonalCard key={index} data={farmer} />;
        })}
      </View>
      <View style={styles.containter03}>
        <Label4>Onde é plantada nossa comida</Label4>
        <Image
          style={{ width: 200, height: 150, alignSelf: 'center' }}
          source={require('../assets/images/jacipora.png')}
        />
        <Label4>Horta da Pretinha em Jaciporã</Label4>
        {/* <Label1>
          Se por um lado a agricultura orgânica traz a ênfase para um plantio
          mais saudável, sem agrotóxicos, as plantações agroecológicas seguem
          num contexto mais amplo.
        </Label1> */}
        <Image
          style={{ width: 200, height: 150, alignSelf: 'center' }}
          source={require('../assets/images/pauliceia.png')}
        />
        <Label4>Assentamento Regência em Paulicéia</Label4>
        {/* <Label1>
          Se por um lado a agricultura orgânica traz a ênfase para um plantio
          mais saudável, sem agrotóxicos, as plantações agroecológicas seguem
          num contexto mais amplo.
        </Label1> */}
      </View>
      <View style={styles.videoContainer}>
        <Label4>Nossa história</Label4>
        <Video
          ref={video}
          style={styles.video}
          source={require('../assets/videos/videodeapresentacao.mp4')}
          useNativeControls
          resizeMode={ResizeMode.CONTAIN}
          isLooping
          onPlaybackStatusUpdate={(status) => setStatus(() => status)}
        />
      </View>
      {/* <WebView
        style={styles.video}
        source={{ uri: 'https://youtu.be/lJuDAR08t-g' }}
      /> */}
      {/* <View style={styles.containter05}>
        <Label1>Onde é plantada nossa comida</Label1>
        <Image
          style={{ width: 200, height: 150, alignSelf: 'center' }}
          source={require('../assets/images/jacipora.png')}
        />
        <Label4>Horta da Pretinha em Jaciporã</Label4>
        <Label1>
          Se por um lado a agricultura orgânica traz a ênfase para um plantio
          mais saudável, sem agrotóxicos, as plantações agroecológicas seguem
          num contexto mais amplo.
        </Label1>
        <Image
          style={{ width: 200, height: 150, alignSelf: 'center' }}
          source={require('../assets/images/pauliceia.png')}
        />
        <Label4>Assentamento Regência em Paulicéia</Label4>
        <Label1>
          Se por um lado a agricultura orgânica traz a ênfase para um plantio
          mais saudável, sem agrotóxicos, as plantações agroecológicas seguem
          num contexto mais amplo.
        </Label1>
      </View> */}
      <View style={styles.containter06}>
        <Label1 style={[GlobalStyles.centerText, styles.projectDetails]}>
          Esse projeto foi desenvolvido por um grupo que comercializa cestas
          agroecológicas nos municípios de Dracena, Nova Guataporanga, Panorama
          Paulicéia e Tupi Paulista.
        </Label1>
        <Label3 style={GlobalStyles.centerText}>
          As pessoas que participaram do desenvolvimento do aplicativo são:
        </Label3>
        {authors.map((author, index) => {
          return <PersonalCard key={index} data={author} />;
        })}
      </View>
      <View style={styles.containter06}>
        <Label4 style={[GlobalStyles.centerText, styles.projectDetails]}>
          Compartilhe o projeto com seus amigos e amigas
        </Label4>
        {/* <Button style={styles.button} title='Share' onPress={onShare} />
        <PrimaryButton title='Share' onPress={onShare} /> */}
        <PrimaryButton onPress={onShare}>Compartilhar</PrimaryButton>
      </View>
      <View style={styles.containter06}>
        <Label4 style={[GlobalStyles.centerText, styles.projectDetails]}>
          Contato
        </Label4>
        <View style={{ flexDirection: 'row' }}>
          <Ionicons name='mail' size={24} color='black' />
          <Label3 style={[GlobalStyles.centerText]}>
            cesta.agroecologica@gmail.com
          </Label3>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Ionicons name='phone-portrait' size={24} color='black' />
          <Label3 style={[GlobalStyles.centerText]}>+55 18 99752-9678</Label3>
        </View>
      </View>
    </ScrollView>
  );
}

export const aboutScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Sobre o Projeto' />
    </View>
  ),
};

export default AboutScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.primary,
    // padding: 10,
  },
  logo: {
    // flexDirection: 'row',
    alignSelf: 'center',
    // alignContent: 'center',
    // justifyContent: 'center',
    width: 100,
    height: 100,
  },
  summaryContainer: {
    backgroundColor: GlobalStyles.colors.quaternary,
    marginHorizontal: 10,
    borderRadius: 15,
  },
  containter01: {
    flex: 1,
    backgroundColor: '#DDCFB2',
    padding: 30,
  },
  containter02: {
    flex: 1,
    backgroundColor: '#F3E5B3',
    padding: 20,
  },
  containter03: {
    flex: 1,
    backgroundColor: '#86B996',
    padding: 30,
  },
  videoContainer: {
    flex: 1,
    backgroundColor: '#F3E5B3',
    padding: 30,
  },
  containter05: {
    flex: 1,
    backgroundColor: '#F3E5B3',
    padding: 30,
  },
  containter06: {
    flex: 1,
    backgroundColor: '#DDCFB2',
    padding: 30,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  bullet: {
    width: 20,
    height: 20,
    borderRadius: 10,
    margin: 10,
  },
  projectDetails: {
    marginVertical: 10,
  },
  video: {
    height: 600,
  },
  button: {
    backgroundColor: GlobalStyles.colors.primary,
  },
});
