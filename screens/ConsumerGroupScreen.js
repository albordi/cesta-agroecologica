import { useContext, useEffect, useLayoutEffect, useState } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Image,
  Alert,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Input from '../components/Input';
import PrimaryButton from '../components/PrimaryButton';
import { GlobalStyles } from '../constants/styles';
import { Label2 } from '../components/UI/Labels';
import Spinner from '../components/Spinner';
import ErrorOverlay from '../components/ErrorOverlay';
import HeaderTitle from '../components/HeaderTitle';
import ImagePic from '../components/ImagePic';
import StoreImageOnFirebase from '../components/StoreImageOnFirebase';
import { ConsumerGroupContext } from '../store/context/consumergroupcontext';

function ConsumerGroupScreen({ navigation }) {
  console.log('[Consumer Group Screen] started');
  // console.log('[Consumer Group Screen]', consumerGroupInputs);

  const [isLoading, setIsLoading] = useState(true);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState(null);
  const [consumerGroupInputs, setConsumerGroupInputs] = useState({});
  const [images, setImages] = useState([]);

  // console.log(
  //   '[Consumer Group Screen] Images',
  //   JSON.stringify(images, null, 2)
  // );

  const consumerGroupCtx = useContext(ConsumerGroupContext);
  // console.log(
  //   '[Consumer Group Screen] Consumer Group Data',
  //   JSON.stringify(consumerGroupCtx.consumerGroup, null, 2)
  // );
  // console.log(
  //   '[Consumer Group Screen] Consumer Inputs',
  //   JSON.stringify(consumerGroupInputs, null, 2)
  // );

  const getAndSetConsumerGroup = async () => {
    setIsLoading(true);
    await consumerGroupCtx.getConsumerGroup();
    console.log(
      '[Consumer Group Screen] Consumer Group Data',
      JSON.stringify(consumerGroupCtx.consumerGroup, null, 2)
    );
    try {
      // const consumerGroupAux = await getCollection('consumergroup');
      // console.log('Consumer Group Aux', consumerGroupAux[0]);
      setConsumerGroupInputs({
        id: consumerGroupCtx.consumerGroup.id,
        information: {
          value: consumerGroupCtx.consumerGroup.information,
          isValid: true,
        },
        message: {
          value: consumerGroupCtx.consumerGroup.message,
          isValid: true,
        },
        normalBasketPrice: {
          value: consumerGroupCtx.consumerGroup.normalBasketPrice,
          isValid: true,
        },
        littleBasketPrice: {
          value: consumerGroupCtx.consumerGroup.littleBasketPrice,
          isValid: true,
        },
        specialBasketPrice: {
          value: consumerGroupCtx.consumerGroup.specialBasketPrice,
          isValid: true,
        },
        donationBasketPrice: {
          value: consumerGroupCtx.consumerGroup.donationBasketPrice,
          isValid: true,
        },
        quantityLittleBasket: {
          value: consumerGroupCtx.consumerGroup.quantityLittleBasket,
          isValid: true,
        },
        quantityNormalBasket: {
          value: consumerGroupCtx.consumerGroup.quantityNormalBasket,
          isValid: true,
        },
        productNumberLittleBasket: {
          value: consumerGroupCtx.productNumberLittleBasket,
          isValid: true,
        },
      });
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setError('Houve um erro ao carregar o grupo de consumo !!!');
    }
  };

  useLayoutEffect(() => {
    getAndSetConsumerGroup();
  }, []);

  function informationChangeHandler(value) {
    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      information: { value: value, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  function messageChangeHandler(value) {
    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      message: { value: value, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  function normalBasketPriceChangeHandler(value) {
    const formattedValue = value.replace(',', '.');
    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      normalBasketPrice: { value: formattedValue, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  function littleBasketPriceChangeHandler(value) {
    const formattedValue = value.replace(',', '.');
    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      littleBasketPrice: { value: formattedValue, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  function specialBasketPriceChangeHandler(value) {
    const formattedValue = value.replace(',', '.');

    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      specialBasketPrice: { value: formattedValue, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  function donationBasketPriceChangeHandler(value) {
    const formattedValue = value.replace(',', '.');
    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      donationBasketPrice: { value: formattedValue, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  function quantityLittleBasketChangeHandler(value) {
    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      quantityLittleBasket: { value: value, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  function quantityNormalBasketChangeHandler(value) {
    const newConsumerGroupInputs = {
      ...consumerGroupInputs,
      quantityNormalBasket: { value: value, isValid: true },
    };
    setConsumerGroupInputs(newConsumerGroupInputs);
  }

  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let images = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsMultipleSelection: true,
      base64: true,
      // allowsEditing: true,
      aspect: [4, 3],
      quality: 0.2,
    });
    // console.log(JSON.stringify(images, null, 2));

    if (!images.canceled) {
      // const size = await getImageSize(images.asset[0].uri);
      setImages(images.assets);
    }
  };

  const imagePicker = async () => {
    console.log('[Confirm Order Modal] image picker');
    const multipleImages = true;
    const images = await ImagePic(multipleImages);
    // console.log(JSON.stringify(images, null, 2));
    if (!images.canceled) {
      images.assets.map(async (image) => {
        const imageUrl = await StoreImageOnFirebase('consumergroup', image.uri);
        image.url = imageUrl;
        console.log(imageUrl);
      });
      setImages(images.assets);
    } else {
      console.log('Erro para selecionar imagens');
    }
    // const imageUrl = await StoreImageOnFirebase(image);
    // paymentAux.receipt = imageUrl;
    // setPayment(paymentAux)
  };

  // console.log(
  //   '[Consumer Group Screen]',
  //    JSON.stringify(consumerGroupInputs, null, 2)
  // );

  // Update
  const confirmButtonHandler = async () => {
    setIsSubmitting(true);
    // console.log(
    //   '[Consumer Group Screen] Add Button pressed',
    //   JSON.stringify(consumerGroupInputs, null, 2)
    // );
    const informationIsValid =
      consumerGroupInputs.information.value.trim().length > 0;
    const messageIsValid = true;
    const normalBasketPriceIsValid =
      !isNaN(consumerGroupInputs.normalBasketPrice.value) &&
      consumerGroupInputs.normalBasketPrice.value >= 0;
    const littleBasketPriceIsValid =
      !isNaN(consumerGroupInputs.littleBasketPrice.value) &&
      consumerGroupInputs.littleBasketPrice.value >= 0;
    const specialBasketPriceIsValid =
      !isNaN(consumerGroupInputs.specialBasketPrice.value) &&
      consumerGroupInputs.specialBasketPrice.value >= 0;
    const donationBasketPriceIsValid =
      !isNaN(consumerGroupInputs.donationBasketPrice.value) &&
      consumerGroupInputs.donationBasketPrice.value >= 0;
    const quantityLittleBasketIsValid =
      !isNaN(consumerGroupInputs.quantityLittleBasket.value) &&
      consumerGroupInputs.quantityLittleBasket.value >= 0;
    const quantityNormalBasketIsValid =
      !isNaN(consumerGroupInputs.quantityNormalBasket.value) &&
      consumerGroupInputs.quantityNormalBasket.value >= 0;

    const imagesAux = [];
    images.map((image) => {
      imagesAux.push(image.url);
    });

    // console.log(
    //   '[Consumer Group Screen]',
    //   informationIsValid,
    //   normalBasketPriceIsValid,
    //   littleBasketPriceIsValid,
    //   specialBasketPriceIsValid,
    //   donationBasketPriceIsValid
    // );
    if (
      !informationIsValid ||
      !messageIsValid ||
      !normalBasketPriceIsValid ||
      !littleBasketPriceIsValid ||
      !specialBasketPriceIsValid ||
      !donationBasketPriceIsValid ||
      !quantityLittleBasketIsValid ||
      !quantityNormalBasketIsValid
    ) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setConsumerGroupInputs({
        information: {
          value: consumerGroupInputs.information.value,
          isValid: informationIsValid,
        },
        message: {
          value: consumerGroupInputs.message.value,
          isValid: messageIsValid,
        },
        normalBasketPrice: {
          value: consumerGroupInputs.normalBasketPrice.value,
          isValid: normalBasketPriceIsValid,
        },
        littleBasketPrice: {
          value: consumerGroupInputs.littleBasketPrice.value,
          isValid: littleBasketPriceIsValid,
        },
        specialBasketPrice: {
          value: consumerGroupInputs.specialBasketPrice.value,
          isValid: specialBasketPriceIsValid,
        },
        donationBasketPrice: {
          value: consumerGroupInputs.donationBasketPrice.value,
          isValid: donationBasketPriceIsValid,
        },
        quantityLittleBasket: {
          value: consumerGroupInputs.quantityLittleBasket.value,
          isValid: quantityLittleBasketIsValid,
        },
        quantityNormalBasket: {
          value: consumerGroupInputs.quantityNormalBasket.value,
          isValid: quantityNormalBasketIsValid,
        },
      });
      setIsSubmitting(false);
      return;
    }
    console.log('---', consumerGroupInputs.normalBasketPrice.value);
    console.log('---', parseFloat(consumerGroupInputs.normalBasketPrice.value));

    const consumerGroupToUpdate = {
      information: consumerGroupInputs.information.value,
      message: consumerGroupInputs.message.value,
      normalBasketPrice: parseFloat(
        consumerGroupInputs.normalBasketPrice.value
      ),
      littleBasketPrice: parseFloat(
        consumerGroupInputs.littleBasketPrice.value
      ),
      specialBasketPrice: parseFloat(
        consumerGroupInputs.specialBasketPrice.value
      ),
      donationBasketPrice: parseFloat(
        consumerGroupInputs.donationBasketPrice.value
      ),
      quantityLittleBasket: parseInt(
        consumerGroupInputs.quantityLittleBasket.value
      ),
      quantityNormalBasket: parseInt(
        consumerGroupInputs.quantityNormalBasket.value
      ),
      images: imagesAux,
    };
    const consumerGroupId = consumerGroupInputs.id;
    console.log(
      'before update or include',
      JSON.stringify(consumerGroupToUpdate, null, 2)
    );

    try {
      // const id = await addDocument('consumergroup', productToAddOrUpdate);
      const error = await consumerGroupCtx.updateConsumerGroup(
        consumerGroupId,
        consumerGroupToUpdate
      );
      console.log(error);
      if (error) {
        Alert.alert(
          'O tamanho das imagens não pode excender 1MB',
          'Por favor, carregue imagens menores.'
        );
      } else {
        navigation.goBack();
      }
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
      setError('Ocorreu um erro inesperado', e);
    }
    setIsSubmitting(false);
  };

  function cancelButtonHandler() {
    navigation.goBack();
  }

  // const formIsInvalid =
  //   !informationIsValid ||
  //   !normalBasketPriceIsValid ||
  //   !littleBasketPriceIsValid ||
  //   !specialBasketPriceIsValid;

  function errorHandler() {
    setError(null);
    navigation.goBack();
  }

  if (error && !isSubmitting) {
    return <ErrorOverlay message={error} onConfirm={errorHandler} />;
  }

  if (isLoading) {
    console.log('isLoading...');
    return <Spinner />;
  }

  return (
    <ScrollView style={styles.screenContainer}>
      {/* <Label1 style={styles.title}>Informações sobre o Grupo de Consumo</Label1> */}
      <Text style={[styles.inputLabel, { marginLeft: 20 }]}>Informações</Text>
      <View style={styles.fieldContainer}>
        <Input
          value={consumerGroupInputs.information.value}
          isValid={consumerGroupInputs.information.isValid}
          textInputConfig={{
            onChangeText: informationChangeHandler,
            autoCapitalize: 'sentences',
            numberOfLines: 3,
            multiline: true
          }}
        />
      </View>
      <Text style={[styles.inputLabel, { marginLeft: 20 }]}>
        Mensagem para o grupo
      </Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Mensagem para o grupo'
          value={consumerGroupInputs.message.value}
          isValid={consumerGroupInputs.message.isValid}
          style={{ textAlign: 'left' }}
          textInputConfig={{
            onChangeText: messageChangeHandler,
            autoCapitalize: 'sentences',
            keyboardType: 'ascii-capable',
            numberOfLines: 3,
          }}
        />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.inputLabel}>
          Quantidade de Items da Cesta Normal
        </Text>
        <Input
          value={consumerGroupInputs.quantityNormalBasket.value.toString()}
          isValid={consumerGroupInputs.quantityNormalBasket.isValid}
          style={{ textAlign: 'right' }}
          textInputConfig={{
            onChangeText: quantityNormalBasketChangeHandler,
            keyboardType: 'numeric',
          }}
        />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.inputLabel}>
          Quantidade de Items da Cesta Pequena
        </Text>
        <Input
          value={consumerGroupInputs.quantityLittleBasket.value.toString()}
          isValid={consumerGroupInputs.quantityLittleBasket.isValid}
          style={{ textAlign: 'right' }}
          textInputConfig={{
            onChangeText: quantityLittleBasketChangeHandler,
            keyboardType: 'numeric',
          }}
        />
      </View>

      <View style={styles.fieldContainer}>
        <Text style={styles.inputLabel}>Preço da Cesta Normal - R$</Text>
        <Input
          label='Preço da Cesta Normal'
          value={consumerGroupInputs.normalBasketPrice.value.toString()}
          isValid={consumerGroupInputs.normalBasketPrice.isValid}
          style={{ textAlign: 'right' }}
          textInputConfig={{
            onChangeText: normalBasketPriceChangeHandler,
            keyboardType: 'numeric',
          }}
        />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.inputLabel}>Preço da Cesta Pequena - R$</Text>
        <Input
          label='Preço da Cesta Pequena'
          value={consumerGroupInputs.littleBasketPrice.value.toString()}
          isValid={consumerGroupInputs.littleBasketPrice.isValid}
          style={{ textAlign: 'right' }}
          textInputConfig={{
            onChangeText: littleBasketPriceChangeHandler,
            keyboardType: 'numeric',
          }}
        />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.inputLabel}>Preço da Cesta Especial - R$</Text>
        <Input
          label='Preço da Cesta Especial'
          value={consumerGroupInputs.specialBasketPrice.value.toString()}
          isValid={consumerGroupInputs.specialBasketPrice.isValid}
          style={{ textAlign: 'right' }}
          textInputConfig={{
            onChangeText: specialBasketPriceChangeHandler,
            keyboardType: 'numeric',
          }}
        />
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.inputLabel}>Preço da Cesta Doação - R$</Text>
        <Input
          label='Preço da Cesta Especial'
          value={consumerGroupInputs.donationBasketPrice.value.toString()}
          isValid={consumerGroupInputs.donationBasketPrice.isValid}
          style={{ textAlign: 'right' }}
          textInputConfig={{
            onChangeText: donationBasketPriceChangeHandler,
            keyboardType: 'numeric',
          }}
        />
      </View>
      <Label2 style={{ textAlign: 'center' }}>
        Você pode selecionar imagens para aparecer no carrossel das entregas.
      </Label2>
      <View style={styles.imagesButtonContainer}>
        <PrimaryButton
          title='Pick an image from camera roll'
          // onPress={pickImage}
          onPress={imagePicker}
        >
          Selecione as imagens
        </PrimaryButton>
      </View>
      <Label2 style={{ textAlign: 'center' }}>
        A soma do tamanho de todas as imagens não pode superar 1MB
      </Label2>
      <ScrollView horizontal={true} style={styles.imagesContainer}>
        {images &&
          images.map((image, index) => {
            return (
              <View key={index}>
                <Image source={{ uri: image.uri }} style={styles.image} />
              </View>
            );
          })}
      </ScrollView>
      <View style={styles.buttonsContainer}>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={confirmButtonHandler}>
            Atualizar
          </PrimaryButton>
        </View>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={cancelButtonHandler}>Cancel</PrimaryButton>
        </View>
      </View>
    </ScrollView>
  );
}

export const consumerGroupScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Gerenciar Grupo de Consumo' />
    </View>
  ),
};

export default ConsumerGroupScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    marginTop: 15,
    // backgroundColor: GlobalStyles.colors.primary,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 40,
    color: GlobalStyles.colors.secondary,
  },
  fieldContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginVertical: 2,
  },
  inputLabel: {
    color: GlobalStyles.colors.secondary,
    textAlign: 'left',
    textAlignVertical: 'center',
    width: '70%',
    // backgroundColor: 'grey',
  },
  imagesButtonContainer: {
    marginHorizontal: 20,
  },
  buttonsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 20,
  },
  buttonContainer: {
    flex: 1,
  },
  deleteIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  errorText: {
    textAlign: 'center',
    color: GlobalStyles.colors.error,
    margin: 8,
  },
  imagesContainer: {
    flexDirection: 'row',
    // justifyContent: 'center',
    // flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  image: {
    width: 100,
    height: 100,
    margin: 5,
  },
});
