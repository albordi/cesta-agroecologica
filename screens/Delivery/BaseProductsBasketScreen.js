import { useContext, useCallback, useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  FlatList,
  Image,
  Text,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import ProductListWithCheck from '../../components/ProductListWithCheck';
import { DeliveryContext } from '../../store/context/deliverycontext';
import Spinner from '../../components/Spinner';
import { Label2 } from '../../components/UI/Labels';
import Input from '../../components/Input';
import { GlobalStyles } from '../../constants/styles';

function BaseProductsBasketScreen() {
  console.log('[Base Products Basket Screen Selection] started ');
  const [isFetchingProducts, setIsFetchingProducts] = useState(false);
  const [error, setError] = useState(null);
  const [productsBaseList, setProductsBaseList] = useState([]);

  const deliveryCtx = useContext(DeliveryContext);

  // console.log(
  //   '[Base Products basket] delivery',
  //   JSON.stringify(deliveryCtx.delivery, null, 2)
  // );

  function createProductsBaseList() {
    console.log('[Base Producst Basket Screen] create Base Products List');
    // console.log(
    //   '[Donation Basket Screen] create Product Donation List products donation list before',
    //   JSON.stringify(productsDonationList, null, 2)
    // );
    // console.log(
    //   '[Donation Basket Screen] create Product Donation List delivery',
    //   JSON.stringify(deliveryCtx.delivery, null, 2)
    // );
    const newProductsBaseList = [];
    deliveryCtx.delivery.products.map((product) => {
      if (product.availableProducts > 0) {
        const newProduct = {
          id: product.id,
          name: product.name,
          imageUrl: product.imageUrl,
          checked: false,
        };

        let index = deliveryCtx.delivery.baseBasketProducts.findIndex(
          (productId) => productId === newProduct.id
        );
        // console.log(index);
        if (index >= 0) {
          // console.log('new product', newProduct);
          newProduct.checked = true;
          // console.log('new product', newProduct);
        }
        newProductsBaseList.push(newProduct);
      }
    });
    setProductsBaseList(newProductsBaseList);
  }

  useFocusEffect(
    useCallback(() => {
      console.log('Base Products screen is focused');
      createProductsBaseList();
    }, [deliveryCtx.delivery])
  );

  // function onMessageChangeHandle(value) {
  //   console.log('value', value);
  //   const deliveryAux = {
  //     ...deliveryCtx.delivery,
  //     donationBasketMessage: value,
  //   };
  //   deliveryCtx.setDelivery(deliveryAux);
  // }

  const checkHandler = (productId) => {
    console.log('product checked', productId);
    deliveryCtx.setBasketProduct('base', productId);
    createProductsBaseList();
  };

  return (
    <View style={StyleSheet.screenContainer}>
      {productsBaseList.length <= 0 ? (
        <Label2>Não existem itens para serem selecionados</Label2>
      ) : (
        <View style={StyleSheet.screenContainer}>
          <View style={styles.screenTitle}>
            <Label2 style={GlobalStyles.centerText}>
              Escolha os itens da cesta normal e pequena
            </Label2>
          </View>
          <View style={styles.productListContainer}>
            <ProductListWithCheck
              products={productsBaseList}
              checkHandler={checkHandler}
            />
          </View>
        </View>
      )}
    </View>
  );
}

export default BaseProductsBasketScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: 'white',
    // paddingBottom: 20,
  },
  productListContainer: {
    paddingBottom: 80,
    // height: '70%',
    // marginBottom: 40,
  },
});
