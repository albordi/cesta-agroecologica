import { useEffect, useContext, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';

import {
  FlatList,
  StyleSheet,
  View,
  Text,
  Pressable,
  Alert,
  Platform,
  Button,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { DeliveryContext } from '../../store/context/deliverycontext';
import { ConsumerGroupContext } from '../../store/context/consumergroupcontext';
import { UserContext } from '../../store/context/usercontext';
import { OrderContext } from '../../store/context/ordercontext';

import DeliveryGridTile from '../../components/DeliveryGridTile';
import PrimaryButton from '../../components/PrimaryButton';
import { GlobalStyles } from '../../constants/styles';
import Spinner from '../../components/Spinner';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import { Label1, Label2, Label3, Label5 } from '../../components/UI/Labels';
import { format } from 'date-fns';
import GLOBALS from '../../Globals';
import PhotoCarousel from '../../components/PhotoCarousel';
import { ScrollView } from 'react-native-gesture-handler';
import HeaderTitle from '../../components/HeaderTitle';

const stardardImages = [
  'https://firebasestorage.googleapis.com/v0/b/cestaagrocologica.appspot.com/o/consumergroup%2Fimage5.jpeg?alt=media&token=ade48051-147a-401a-be50-e2a6bd234f12',
  'https://firebasestorage.googleapis.com/v0/b/cestaagrocologica.appspot.com/o/consumergroup%2Fimage6.jpeg?alt=media&token=00aa2909-1107-493c-aed5-e2a2c2e59486',
  // 'https://firebasestorage.googleapis.com/v0/b/cestaagroecologica-dev.appspot.com/o/standardimages%2Fimage1.jpeg?alt=media&token=77a2c665-23bf-4ecc-8dfe-998865e7df26',
  // 'https://firebasestorage.googleapis.com/v0/b/cestaagroecologica-dev.appspot.com/o/standardimages%2Fimage2.jpeg?alt=media&token=ce4b8ce7-172f-4de2-93c5-bda1cef25878',
];

function DeliveryScreen() {
  console.log('[Delivery Screen Started]');
  const consumerGroupCtx = useContext(ConsumerGroupContext);
  const deliveryCtx = useContext(DeliveryContext);
  const usersCtx = useContext(UserContext);
  const orderCtx = useContext(OrderContext);
  const navigation = useNavigation();

  // console.log(
  //   '[Deliveries Screen] consumer group infor',
  //   JSON.stringify(consumerGroupCtx.consumerGroup, null, 2)
  // );

  // console.log(
  //   '[Deliveries Screen] deliveries',
  //   JSON.stringify(deliveryCtx.deliveries, null, 2)
  // );

  // console.log(
  //   '[Deliveries Screen] Next Delivery',
  //   JSON.stringify(deliveryCtx.nextDelivery, null, 2)
  // );

  //   console.log(
  //   '[Deliveries Screen] Users',
  //   JSON.stringify(usersCtx, null, 2)
  // );

  useFocusEffect(
    useCallback(() => {
      deliveryCtx.getDeliveries();
      getOrder();
    }, [])
  );

  const getOrder = async () => {
    //Verify if there is an order for the user
    if (usersCtx.user.role === GLOBALS.USER.ROLE.CONSUMER) {
      let orderAux = {};
      try {
        orderAux = await orderCtx.getOrder(
          deliveryCtx.nextDelivery.id,
          usersCtx.user.id
        );
      } catch (error) {
        console.log(
          '[Delivery Screen] Houve um erro para carregar o pedido existente do consumidor',
          error
        );
      }
    } else {
      console.log(
        '[Delivery Screen] Problemas para definir o role do usuário.'
      );
    }
  };

  // useEffect(() => {
  //   deliveryCtx.getDeliveries();
  //   getOrder();
  // }, []);

  function showOrUpdateDelivery(deliveryId) {
    // const isAdmin = false;
    // console.log('[Delivery Screen] user is admin', usersCtx.user);
    if (usersCtx.user.role === GLOBALS.USER.ROLE.ADMIN) {
      // navigation.navigate('ManageDeliveryScreen', {
      //   deliveryId: itemData.item.id,
      // });
      navigation.navigate('DeliveryTabs', {
        action: 'update',
        deliveryId: deliveryId,
      });
    } else {
      navigation.navigate('OrderScreen', {
        deliveryId: deliveryId,
      });
    }
  }

  function showOrderReport(deliveryId) {
    navigation.navigate('OrderReport', {
      deliveryId: deliveryId,
    });
  }

  function showTotalProductsByDeliveryReport(deliveryId) {
    navigation.navigate('TotalProductsByDeliveryReport', {
      deliveryId: deliveryId,
    });
  }

  // function renderDeliveryItem(itemData) {
  //   return (
  //     <DeliveryGridTile
  //       title={format(
  //         new Date(itemData.item.date),
  //         GLOBALS.FORMAT.DEFAULT_DATE
  //       )}
  //       // subTitle={itemData.item.message}
  //       // color={GlobalStyles.colors.tertiary}
  //       color={'white'}
  //       onPress={() => showOrUpdateDelivery(itemData.item.id)}
  //     ></DeliveryGridTile>
  //   );
  // }

  const buttonPressHandler = () => {
    navigation.navigate('DeliveryTabs', { action: 'create' });
  };

  const showConsumersOrders = async () => {
    console.log('[Delivery Screen]  show consumers orders or user order');
    if (usersCtx.user.role === GLOBALS.USER.ROLE.ADMIN) {
      console.log('[Delivery Screen]  consumers orders');

      navigation.navigate('UsersOrdersScreen', {
        deliveryId: deliveryCtx.nextDelivery.id,
      });
    } else if (usersCtx.user.role === GLOBALS.USER.ROLE.CONSUMER) {
      // console.log('[Delivery Screen] user order');
      // console.log('[Delivery Screen] user', usersCtx.user);

      // Verify if the date to Order is over
      const currentDate = new Date();
      const dateToOrder = new Date(deliveryCtx.nextDelivery.dateToOrder);
      console.log('[Delivery Screen] Today', currentDate);
      console.log('[Delivery Screen] Date to Order', dateToOrder);

      if (dateToOrder < currentDate) {
        console.log('Não é possível fazer pedidos');
        Alert.alert(
          'Não é mais possível fazer ou alterar pedidos!',
          'A data para fazer um pedido ou alterá-lo já passou. Nos desculpe.'
        );
        return;
      }

      console.log('[Delivery Screen] Fetch user order');
      let orderAux = {};
      try {
        orderAux = await orderCtx.getOrder(
          deliveryCtx.nextDelivery.id,
          usersCtx.user.id
        );
        // console.log(
        //   '[Delivery Screen] user specific order',
        //   JSON.stringify(orderAux, null, 2)
        // );
        navigation.navigate('OrderScreen', {
          deliveryId: deliveryCtx.nextDelivery.id,
          userId: usersCtx.user.id,
          order: orderAux[0],
        });
      } catch (error) {
        console.log(
          '[Delivery Screen] Houve um erro para carregar o pedido existente do consumidor',
          error
        );
      }
    } else {
      console.log(
        '[Delivery Screen] Problemas para definir o role do usuário.'
      );
    }
  };
  if (deliveryCtx.isLoading) {
    console.log('isLoading...');
    return <Spinner />;
  }

  console.log(usersCtx.user.role);

  // Não apagar.
  // const sendPushNotificationTest = () => {
  //   usersCtx.sendNotification(
  //     'ExponentPushToken[3y3v0qALTn_3l2FdDH6at1]',
  //     'teste push notification',
  //     'messageBody'
  //   );
  // };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View>
          <Label1
            style={[
              styles.welcomeText,
              GlobalStyles.centerText,
              GlobalStyles.m10,
            ]}
          >
            Bem-vinda (o) ao grupo da
          </Label1>
          <Text style={styles.basketTitle}>CESTA</Text>
          <Text style={styles.basketSubTitle}>AGROECOLÓGICA</Text>
          {/* <Button
          title='Send Push Notification'
          onPress={() => sendPushNotificationTest()}
        /> */}

          <View style={styles.informationMessageContainer}>
            <Label2 style={[GlobalStyles.centerText, GlobalStyles.m10]}>
              {consumerGroupCtx.consumerGroup.information}
            </Label2>
          </View>
          <View style={styles.messageContainer}>
            <Label2 style={[GlobalStyles.centerText, GlobalStyles.m10]}>
              {consumerGroupCtx.consumerGroup.message}
            </Label2>
          </View>
        </View>
        {deliveryCtx.nextDelivery ? (
          <View>
            <Label5 style={[GlobalStyles.centerText, GlobalStyles.mt10]}>
              Pŕoxima Entrega
            </Label5>
            {/* <Label3 style={GlobalStyles.centerText}>
            Clique sobre a data para fazer ou atualizar um pedido.
          </Label3> */}
            <Text style={GlobalStyles.centerText}>
              Clique sobre a data para fazer ou atualizar um pedido.
            </Text>
            <View style={[styles.nextDelivery]}>
              {usersCtx.user.role === 'admin' ? (
                <View style={styles.iconsContainer}>
                  <Ionicons
                    style={styles.updateIcon}
                    name='create'
                    size={24}
                    color={GlobalStyles.colors.secondary}
                    onPress={() =>
                      showOrUpdateDelivery(deliveryCtx.nextDelivery.id)
                    }
                  />
                  <Ionicons
                    style={styles.updateIcon}
                    name='list'
                    size={24}
                    color={GlobalStyles.colors.secondary}
                    onPress={() => showOrderReport(deliveryCtx.nextDelivery.id)}
                  />
                  <Ionicons
                    style={styles.updateIcon}
                    name='list'
                    size={24}
                    color={GlobalStyles.colors.secondary}
                    onPress={() =>
                      showTotalProductsByDeliveryReport(
                        deliveryCtx.nextDelivery.id
                      )
                    }
                  />
                </View>
              ) : null}
              <Pressable
                android_ripple={{ color: '#ccc' }}
                style={styles.button}
                // style={({ pressed }) => [
                //   styles.button,
                //   pressed ? styles.buttonPressed : null,
                // ]}
                onPress={() => showConsumersOrders()}
              >
                <View style={[styles.innerContainer]}>
                  <Text style={styles.title}>
                    {format(
                      new Date(deliveryCtx.nextDelivery.date),
                      GLOBALS.FORMAT.DEFAULT_DATE
                    )}
                  </Text>
                  <Label2>
                    Pedidos até -
                    {format(
                      new Date(deliveryCtx.nextDelivery.dateToOrder),
                      GLOBALS.FORMAT.DEFAULT_DATE_TIME
                    )}
                  </Label2>

                  <Text style={styles.subTitle}>
                    {deliveryCtx.nextDelivery.message}
                  </Text>
                </View>
              </Pressable>
            </View>
          </View>
        ) : (
          <View style={styles.buttonContainer}>
            {usersCtx.user.role === 'admin' ? (
              <View>
                <PrimaryButton onPress={buttonPressHandler}>
                  Criar nova entrega
                </PrimaryButton>
              </View>
            ) : (
              <View>
                <Label1 style={GlobalStyles.centerText}>
                  Bem vindo, bem vinda ao grupo da consumo de cestas
                  agroecológicas.
                </Label1>
                <Label1 style={{ textAlign: 'center' }}>
                  Não há entregas agendadas
                </Label1>
              </View>
            )}
          </View>
        )}

        <View style={styles.photoCarouselContainer}>
          <PhotoCarousel
            images={
              consumerGroupCtx.consumerGroup.images.length > 0
                ? consumerGroupCtx.consumerGroup.images
                : stardardImages
            }
          />
          {/* <PhotoCarousel images={consumerGroupCtx.consumerGroup.images} /> */}
        </View>

        <Label5 style={GlobalStyles.centerText}>Entregas passadas</Label5>
        {deliveryCtx.deliveries.map((delivery) => {
          return (
            <DeliveryGridTile
              key={delivery.id}
              title={format(
                new Date(delivery.date),
                GLOBALS.FORMAT.DEFAULT_DATE
              )}
              // subTitle={itemData.item.message}
              // color={GlobalStyles.colors.tertiary}
              color={'white'}
              onPress={() => showOrUpdateDelivery(delivery.id)}
            ></DeliveryGridTile>
          );
        })}
        {/* <FlatList
          data={deliveryCtx.deliveries}
          keyExtractor={(item) => item.id}
          renderItem={renderDeliveryItem}
        ></FlatList> */}
      </ScrollView>
    </View>
  );
}

export const deliveryScreenOptions = {
  headerTitle: () => (
    <View>
      <HeaderTitle title='Entregas' />
    </View>
  ),
};

export default DeliveryScreen;

const styles = StyleSheet.create({
  headeContainer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    justifyContent: 'space-between',
    // height: 60, // Adjust the height as needed
    width: '100%', // Ensures the header takes up the full width
  },
  welcomeText: {
    backgroundColor: GlobalStyles.colors.color3,
    color: 'white',
    borderRadius: 5,
    marginHorizontal: 20,
  },
  basketTitle: {
    fontFamily: 'CheapPotatoesBlackThin',
    fontSize: 15,
    alignSelf: 'flex-start',
    marginLeft: 40,
    color: GlobalStyles.colors.quinternary,
  },
  basketSubTitle: {
    fontFamily: 'CheapPotatoesBlackThin',
    fontSize: 30,
    alignSelf: 'flex-start',
    marginLeft: 40,
    marginTop: -10,
    // marginBottom: 20,
    color: GlobalStyles.colors.secondary,
  },
  label: {
    color: 'white',
    fontSize: 20,
  },
  icon: {
    marginLeft: 8, // Adjust the spacing between the label and icon
  },

  container: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  informationMessageContainer: {
    backgroundColor: '#c7c9b5',
    marginHorizontal: 20,
    borderRadius: 8,
  },
  messageContainer: {
    backgroundColor: '#c7c9b5',
    marginHorizontal: 20,
    marginTop: 10,
    borderRadius: 8,
  },
  buttonContainer: {
    // flex: 1,
    // marginTop: 100,
    padding: 16,
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInputContainer: {
    marginHorizontal: 10,
    alignItems: 'center',
  },
  iconsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  nextDelivery: {
    margin: 16,
    height: 100,
    borderRadius: 8,
    elevation: 4,
    backgroundColor: GlobalStyles.colors.primary,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  updateIcon: {
    marginTop: 5,
    marginRight: 5,
    textAlign: 'right',
    zIndex: 1000,
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    color: GlobalStyles.colors.secondary,
  },
  subTitle: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 12,
  },
  photoCarouselContainer: {
    marginBottom: 15,
    height: 200,
    // marginHorizontal: 20,
    // borderWidth: 1,
    // borderColor: 'black',
  },
});
