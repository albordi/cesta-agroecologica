import { useContext, useCallback, useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  FlatList,
  Image,
  Text,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import ProductListWithCheck from '../../components/ProductListWithCheck';
import { DeliveryContext } from '../../store/context/deliverycontext';
import Spinner from '../../components/Spinner';
import { Label2 } from '../../components/UI/Labels';
import Input from '../../components/Input';
import { GlobalStyles } from '../../constants/styles';

function DonationBasketScreen() {
  console.log('[Donation Basket Screen] started ');
  const [isFetchingProducts, setIsFetchingProducts] = useState(false);
  const [error, setError] = useState(null);
  const [productsDonationList, setProductsDonationList] = useState([]);

  const deliveryCtx = useContext(DeliveryContext);

  console.log(
    '[Donation basket products] delivery',
    JSON.stringify(deliveryCtx.delivery, null, 2)
  );

  function createProductsDonationList() {
    console.log('[Donation Basket Screen] create Product Donation List');
    // console.log(
    //   '[Donation Basket Screen] create Product Donation List products donation list before',
    //   JSON.stringify(productsDonationList, null, 2)
    // );
    // console.log(
    //   '[Donation Basket Screen] create Product Donation List delivery',
    //   JSON.stringify(deliveryCtx.delivery, null, 2)
    // );
    const newProductsDonationList = [];
    deliveryCtx.delivery.products.map((product) => {
      if (product.availableProducts > 0) {
        const newProduct = {
          id: product.id,
          name: product.name,
          imageUrl: product.imageUrl,
          checked: false,
        };

        let index = deliveryCtx.delivery.donationBasketProducts.findIndex(
          (productId) => productId === newProduct.id
        );
        // console.log(index);
        if (index >= 0) {
          console.log('new product', newProduct);
          newProduct.checked = true;
          console.log('new product', newProduct);
        }
        newProductsDonationList.push(newProduct);
      }
    });
    setProductsDonationList(newProductsDonationList);
  }

  useFocusEffect(
    useCallback(() => {
      console.log('Donation screen is focused');
      createProductsDonationList();
    }, [deliveryCtx.delivery])
  );

  function onMessageChangeHandle(value) {
    console.log('value', value);
    const deliveryAux = {
      ...deliveryCtx.delivery,
      donationBasketMessage: value,
    };
    deliveryCtx.setDelivery(deliveryAux);
  }

  const checkHandler = (productId) => {
    console.log('product checked', productId);
    deliveryCtx.setBasketProduct('donation', productId);
    createProductsDonationList();
  };

  return (
    <View style={StyleSheet.screenContainer}>
      {productsDonationList.length <= 0 ? (
        <Label2>Não existem itens para serem selecionados</Label2>
      ) : (
        <View>
          {/* <Label2 style={GlobalStyles.centerText}>Texto da Cesta de Doação</Label2> */}
          <View style={styles.messageContainer}>
            <Input
              style={styles.message}
              isValid={true}
              textInputConfig={{
                onChangeText: onMessageChangeHandle,
                value: deliveryCtx.delivery.donationBasketMessage,
                placeholder: 'Contribua com aquelas e aqueles que precisam.',
                keyboardType: 'default',
                multiline:true  // Allows multiple lines

              }}
            />
          </View>
          <Label2 style={GlobalStyles.centerText}>Escolha os itens da cesta de doação</Label2>
          <View style={styles.productListContainer}>
            <ProductListWithCheck
              products={productsDonationList}
              checkHandler={checkHandler}
            />
          </View>
        </View>
      )}
    </View>
  );
}

export default DonationBasketScreen;

const styles = StyleSheet.create({
  screenContainer: {
    backgroundColor: 'white',
  },
  messageContainer: {
    marginHorizontal: 15,
    height: 120,
  },
  message: {
    height: 100,
    textAlign: 'left',
    textAlignVertical: 'top',
  },
  productListContainer: {
    height: '70%',
    marginBottom: 40,

  },
});
