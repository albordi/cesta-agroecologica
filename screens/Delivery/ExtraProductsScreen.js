import { useContext, useCallback, useState, useEffect } from 'react';
import { View, StyleSheet, FlatList, Image, Text, Alert } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import ProductListWithCheck from '../../components/ProductListWithCheck';
import { DeliveryContext } from '../../store/context/deliverycontext';
import Spinner from '../../components/Spinner';
import { Label2 } from '../../components/UI/Labels';
import PrimaryButton from '../../components/PrimaryButton';
import { Ionicons } from '@expo/vector-icons';
import { GlobalStyles } from '../../constants/styles';
import { UserContext } from '../../store/context/usercontext';

function ExtraProductsBasketScreen({ navigation, route }) {
  console.log('[Extra Products Basket Screen] started ');
  const [isLoading, setIsLoading] = useState(false);
  const [extraProductsList, setExtraProductsList] = useState([]);

  const deliveryCtx = useContext(DeliveryContext);
  const userCtx = useContext(UserContext);

  // console.log(
  //   '[Extra products screen] delivery',
  //   JSON.stringify(deliveryCtx.delivery, null, 2)
  // );
  // console.log(
  //   '[Extra products screen] delivery',
  //   JSON.stringify(userCtx.users, null, 2)
  // );

  function createExtraProductsList() {
    const newExtraProductsList = [];
    console.log('Criando product list');
    deliveryCtx.delivery.products.map((product) => {
      if (product.availableProducts > 0) {
        // Verificar se o produto já existe na lista de produtos extras.
        const newProduct = {
          id: product.id,
          name: product.name,
          imageUrl: product.imageUrl,
          // checked: true,
        };
        let index = deliveryCtx.delivery.extraProducts.findIndex(
          (productId) => productId === newProduct.id
        );
        if (index >= 0) {
          // Product already exists in the list
          // console.log('[Extra Product Screen] product exists]');
          newProduct.checked = true;
          // console.log('new product', newProduct);
        }
        newExtraProductsList.push(newProduct);
      }
    });
    setExtraProductsList(newExtraProductsList); 

    setIsLoading(false);
  }

  useEffect(() => {
    // userCtx.fetchUsers();
    setIsLoading(true);
    deliveryCtx.delivery.products ? createExtraProductsList() : null;
  }, [deliveryCtx.delivery]);

  // useFocusEffect(
  //   useCallback(() => {
  //     console.log('Extra Products Basket screen is focused');
  //     createExtraProductsList();
  //   }, [deliveryCtx.delivery])
  // );

  const checkHandler = (productId) => {
    // console.log('product checked', productId);
    deliveryCtx.setBasketProduct('extraproduct', productId);
    createExtraProductsList();
  };

  async function createOrUpdateDelivery() {
    try {
      if (deliveryCtx.delivery.id) {
        // console.log('[Extra Products Screen] updating delivery');
        const error = await deliveryCtx.updateDelivery(deliveryCtx.delivery.id);
        if (error.length > 0 ) {
          // console.log('[Extra Product Screen] error', JSON.stringify(error, null, 2));
          Alert.alert('ERRO', error.join('\n'));
          return;
        }
        title = 'Atualização de Entrega';
        subTitle = 'Entrega atualizada';
      } else {
        console.log('[Extra Products Screen] adding delivery');
        const error = await deliveryCtx.addDelivery();
        if (error.length > 0) {
          // console.log('[Extra Product Screen] error',JSON.stringify(error, null, 2));
          Alert.alert('ERRO', error.join('\n'));
          return;
        }
        title = 'Nova Entrega';
        subTitle = 'Nova Entrega Cadastrada';
      }
      Alert.alert(title, subTitle);
      //Send notification
      userCtx.users.map((user) => {
        if (user.notificationToken && user.notificationToken !== '') {
          userCtx.sendNotification(
            user.notificationToken,
            'Nova entrega agendada',
            'texto da nova entrega'
          );
        }
      });
      navigation.navigate('Entregas');
    } catch (error) {
      console.log('Problema ao cadastrar ou atualizar a entrega', error);
      Alert.alert('Entrega', 'Problema ao cadastrar ou atualizar a entrega');
    }
  }

  function deleteDelivery() {
    Alert.alert(
      'Deleção',
      'Você tem certeza que deseja apagar essa entrega?',
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        {
          text: 'SIM',
          onPress: () => {
            deliveryCtx.removeDelivery(deliveryCtx.delivery.id);
            navigation.navigate('Entregas');
          },
        },
      ],
      { cancelable: false }
    );
  }

  if (isLoading) {
    console.log('isLoading...');
    return <Spinner />;
  }

  return (
    <View style={styles.container}>
      {extraProductsList.length <= 0 ? (
        <Label2>Não existem itens para serem selecionados</Label2>
      ) : (
        <View style={styles.container}>
          <View style={styles.screenTitle}>
            <Label2 style={GlobalStyles.centerText}>
              Escolha os itens extras
            </Label2>
          </View>
          <View style={styles.productsContainer}>
            <ProductListWithCheck
              products={extraProductsList}
              
              checkHandler={checkHandler}
            />
          </View>
          <View style={styles.buttonsContainer}>
            <PrimaryButton onPress={createOrUpdateDelivery}>
              {deliveryCtx.delivery.id ? 'Atualizar Entrega' : 'Criar entrega'}
            </PrimaryButton>
            {deliveryCtx.delivery.id ? (
              <View style={styles.deleteIconContainer}>
                <Ionicons
                  name='trash'
                  size={36}
                  color={GlobalStyles.colors.alert}
                  onPress={deleteDelivery}
                />
              </View>
            ) : null}
          </View>
        </View>
      )}
    </View>
  );
}

export default ExtraProductsBasketScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: 'row', // Arrange children in a row
  },
  screenTitle: {
    // flex: 1,
    marginBottom: 10,
    // borderWidth: 2,
    // borderBlockColor: 'red',
  },
  productsContainer: {
    flex: 1,
    marginBottom: 10,
    // borderWidth: 2,
    // borderBlockColor: 'red',
  },
  buttonsContainer: {
    // flex: 1,
    // borderWidth: 2,
    // borderBlockColor: 'red',
    marginBottom: 10,
  },

  deleteIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  // productListContainer: {
  //   height: '76%',
  //   marginBottom: 15,
  // },
  // buttonsContainer: {
  //   // backgroundColor: 'red'
  // },
  // line: {
  //   flex: 1,
  //   // flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  // },
});
