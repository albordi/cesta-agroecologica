import React, { useState, useEffect, useContext } from 'react';
import { View, Text, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { format } from 'date-fns';
import { Ionicons } from '@expo/vector-icons';
import Input from '../../components/Input';
import GLOBALS from '../../Globals';
import DropDownInput from '../../components/DropDownInput';
import Label, { Label2, Label3 } from '../../components/UI/Labels';
// import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import ProductList from '../../components/ProductList';
import { DeliveryContext } from '../../store/context/deliverycontext';
import { GlobalStyles } from '../../constants/styles';
import Spinner from '../../components/Spinner';

function ManageDeliveryScreen({ navigation, route }) {
  console.log('[Manage Delivery Screen] started.');
  const deliveryCtx = useContext(DeliveryContext);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showDateTimePicker, setShowDateTimePicker] = useState(false);

  // const [delivery, setDelivery] = useState({});
  // const [isLoading, setIsLoading] = useState(false);

  const [deliveryDate, setDeliveryDate] = useState({
    value: new Date(deliveryCtx.delivery.date),
    isValid: true,
  });
  const [deliveryMessage, setDeliveryMessage] = useState({
    value: '',
    isValid: true,
  });

  // console.log(
  //   '[Manage Delivery Screen] route',
  //   route.params.route.params.deliveryId
  // );

  // console.log(new Date(deliveryCtx.delivery.date));
  // console.log(new Date());

  // console.log(
  //   '[Manage Delivery Screen]',
  //   JSON.stringify(deliveryCtx.delivery, null, 2)
  // );

  // console.log(
  //   '[Manage Delivery Screen]',
  //   JSON.stringify(deliveryCtx.delivery.products, null, 2)
  // );

  useEffect(() => {
    route.params.route.params.action === 'update'
      ? deliveryCtx.getDelivery(route.params.route.params.deliveryId)
      : deliveryCtx.startNewDelivery();
  }, []);

  function onMessageChangeHandle(value) {
    const deliveryAux = { ...deliveryCtx.delivery, message: value };
    deliveryCtx.setDelivery(deliveryAux);
  }

  // const handleConfirm = (date) => {
  //   console.log('[Handle confirm]', new Date(date.nativeEvent.timestamp));
  //   hideDatePicker();
  //   // const deliveryDateValue = format(
  //   //   new Date(date.nativeEvent.timestamp),
  //   //   GLOBALS.FORMAT.DEFAULT_DATE
  //   // );
  //   // setDeliveryDate({ value: value, isValid: true });
  //   // deliveryCtx.setDeliveryDate(value);
  //   const deliveryAux = {
  //     ...deliveryCtx.delivery,
  //     date: new Date(date.nativeEvent.timestamp),
  //   };
  //   // console.log(
  //   //   '[Manage Delivery] --------',
  //   //   JSON.stringify(deliveryAux, null, 2)
  //   // );
  //   deliveryCtx.setDelivery(deliveryAux);
  // };

  const onSelectProdInAbundanceHandle = (product) => {
    // console.log(
    //   '[Manage Delivery Screen] onSelectProdinAbundance',
    //   JSON.stringify(product, null, 2)
    // );
    // deliveryCtx.setProductInAbundance(value);
    const deliveryAux = {
      ...deliveryCtx.delivery,
      productInAbundance: { id: product.id, name: product.name },
    };
    deliveryCtx.setDelivery(deliveryAux);
  };

  function onChangeMaxProdHandle(productId, value) {
    // console.log('[Manage Delivery Screen] increase quantity', productId);
    deliveryCtx.setMaxProd(productId, value);
    // console.log(
    //   '[Manage Delivery Screen]',
    //   JSON.stringify(deliveryCtx.delivery, null, 2)
    // );
  }

  // console.log('[Manage Delivery] isStartingNewDelivery', deliveryCtx.isStartingNewDelivery);

  // console.log(
  //   '[Manage Delivery] new delivery',
  //   JSON.stringify(deliveryCtx.delivery, null, 2)
  // );

  if (deliveryCtx.isStartingNewDelivery) {
    return <Spinner />;
  }

  // function errorHandler() {
  //   setError(null);
  //   navigation.goBack();
  // }

  // console.log('[Create Delivery Screen] erro status', error, isFetchingProducts);

  // if (error && !deliveryCtx.isFetchingProducts) {
  //   return <ErrorOverlay message={error} onConfirm={errorHandler} />;
  // }

  const handleConfirm = (date, format) => {
    console.log('[Handle date confirm]', date, format);
    const dateAux = moment(date).format('YYYY-MM-DD HH:mm');
    let deliveryAux = null;
    if (format === 'date') {
      deliveryAux = {
        ...deliveryCtx.delivery,
        date: dateAux,
      };
    }
    if (format === 'datetime') {
      deliveryAux = {
        ...deliveryCtx.delivery,
        dateToOrder: dateAux,
      };
    }
    hideDatePicker();
    deliveryCtx.setDelivery(deliveryAux);
  };

  const hideDatePicker = () => {
    setShowDateTimePicker(false);
  };

  const showDatePickerHandler = () => {
    console.log(showDatePicker);
    setShowDatePicker(!showDatePicker);
  };

  const showDateTimePickerHandler = () => {
    setShowDateTimePicker(!showDateTimePicker);
  };

  // const handleConfirmDateAndTime = (datetime) => {
  //   console.log('[Handle date an time confirm]', datetime);
  //   // const dateAux = moment(date).format('YYYY-MM-DD HH:mm');
  //   console.log('dateaux', dateAux)
  //   const deliveryAux = {
  //     ...deliveryCtx.delivery,
  //     dateToOrder: dateAux,
  //   };
  //   hideDatePicker();
  //   deliveryCtx.setDelivery(deliveryAux);
  // };

  return (
    <View style={styles.screenContainer}>
      <View style={styles.messageContainer}>
        <Input
          style={styles.message}
          isValid={true}
          textInputConfig={{
            onChangeText: onMessageChangeHandle,
            value: deliveryCtx.delivery.message,
            placeholder: 'Digite aque o texto de abertura da entrega.',
            keyboardType: 'default',
            multiline: true,
          }}
        />
      </View>
      <Label3 style={styles.label}>Data da entrega</Label3>
      <View style={styles.dateInputContainer}>
        <Text style={styles.dateField}>
          {format(
            new Date(deliveryCtx.delivery.date),
            GLOBALS.FORMAT.DEFAULT_DATE
          )}
        </Text>
        <Ionicons
          style={styles.dateIcon}
          // style={{ position: 'absolute', top: 8, right: 3, zIndex: 1 }}
          name='calendar'
          size={24}
          color={GlobalStyles.colors.primary}
          onPress={showDatePickerHandler}
        />
      </View>
      {showDatePicker && (
        <DateTimePickerModal
          value={new Date(deliveryCtx.delivery.date)}
          isVisible={showDatePicker}
          mode='date'
          display='spinner'
          minimumDate={new Date()}
          onConfirm={(date) => handleConfirm(date, 'date')}
          onCancel={hideDatePicker}
          positiveButton={{
            label: 'Confirmar',
            textColor: GlobalStyles.colors.textColor,
          }}
          negativeButton={{
            label: 'Cancelar',
            textColor: GlobalStyles.colors.textColor,
          }}
        />
      )}
      <Text style={styles.label}>Data Máxima para Pedidos</Text>
      <View style={styles.dateInputContainer}>
        <Text style={styles.dateField}>
          {format(
            new Date(deliveryCtx.delivery.dateToOrder),
            GLOBALS.FORMAT.DEFAULT_DATE_TIME
          )}
        </Text>
        <Ionicons
          style={styles.dateIcon}
          // style={{ position: 'absolute', top: 8, right: 3, zIndex: 1 }}
          name='calendar'
          size={24}
          color={GlobalStyles.colors.primary}
          onPress={showDateTimePickerHandler}
        />
      </View>
      {showDateTimePicker && (
        <DateTimePickerModal
          isVisible={showDateTimePicker}
          mode='datetime'
          display='spinner'
          onConfirm={(date) => handleConfirm(date, 'datetime')}
          minimumDate={new Date()}
          onCancel={hideDatePicker}
          positiveButton={{
            label: 'Confirmar',
            textColor: GlobalStyles.colors.textColor,
          }}
          negativeButton={{
            label: 'Cancelar',
            textColor: GlobalStyles.colors.textColor,
          }}
        />
      )}
      {deliveryCtx.isFetchingProducts ? (
        <Spinner />
      ) : (
        <View>
          <Label2 style={GlobalStyles.ml20}>Produto em abundância</Label2>

          <View style={styles.dropDownInput}>
            <DropDownInput
              label='Produto em abundância'
              itemsToShow={deliveryCtx.delivery.products}
              selectedItem={deliveryCtx.delivery.productInAbundance}
              onSelectItemHandle={onSelectProdInAbundanceHandle}
            />
          </View>

          <Text style={styles.label}>
            Selecione os produtos que estarão disponíveis para essa entrega
          </Text>

          <View style={styles.productListContainer}>
            <ProductList
              products={deliveryCtx.delivery.products.sort((a, b) =>
                a.name.toLowerCase().localeCompare(b.name.toLowerCase())
              )}
              onChangeMaxProdHandle={onChangeMaxProdHandle}
            />
            {deliveryCtx.isFetchingProducts &&
            deliveryCtx.delivery.products.length <= 0 ? (
              <Label style={styles.text}>
                Não existem produtos cadastrados
              </Label>
            ) : null}
          </View>
        </View>
      )}

      {/* <View style={styles.buttonContainer}>
        <PrimaryButton>Criar Entrega</PrimaryButton>
      </View> */}
    </View>
  );
}

export const manageDeliveryScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Criar Nova Entrega' />
    </View>
  ),
  // headerBackImage: () => <BackArrow />,
  // headerBackTitleVisible: false,
  // headerStyle: {
  //   backgroundColor: 'transparent',
  //   elevation: 0,
  //   shadowOpacity: 0,
  //   borderBottomWidth: 0,
  // },
};

export default ManageDeliveryScreen;

const styles = StyleSheet.create({
  screenContainer: {
    // flex: 1,
    paddingTop: 5,
    backgroundColor: GlobalStyles.colors.backGroundColor,
    // flexDirection: 'row',
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: "center",
    // backgroundColor: GlobalStyles.colors.backGroundColor,
    // overflow: "hidden",
  },
  messageContainer: {
    marginHorizontal: 15,
    height: 50,
  },
  message: {
    height: 50,
    textAlign: 'left',
    textAlignVertical: 'top',
  },
  label: {
    color: GlobalStyles.colors.secondary,
    textAlign: 'left',
    marginLeft: 20,
    zIndex: -10,
  },
  dateIcon: {
    marginHorizontal: 10,
    color: GlobalStyles.colors.secondary,
  },
  dateField: {
    flex: 1,
    backgroundColor: 'white',
    padding: 6,
    borderBottomColor: '#ddb52f',
    borderBottomWidth: 2,
    color: GlobalStyles.colors.textColor,
    fontSize: 16,
    backgroundColor: 'white',
    borderRadius: 6,
  },
  dropDownInput: {
    marginHorizontal: 20,
    marginBottom: 5,
  },
  dateInputContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginBottom: 10,
    zIndex: -1,
  },

  productListContainer: {
    zIndex: -1,
    height: 270,
    marginBottom: 60,
  },
  // buttonContainer: {
  //   // backgroundColor: 'red',
  //   // flex: 1,
  //   // justifyContent: 'center',
  //   // alignItems: 'center',
  //   paddingVertical: 5,
  //   alignSelf: 'center',
  //   // flexDirection: 'row',
  //   width: '80%',
  // },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginHorizontal: 20,
    zIndex: -1,
  },
  // text: {
  //   textAlign: 'center',
  // },
});
