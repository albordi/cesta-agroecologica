import { useContext, useCallback, useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TextInput,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import ProductListWithCheck from '../../components/ProductListWithCheck';
import { DeliveryContext } from '../../store/context/deliverycontext';
import Spinner from '../../components/Spinner';
import { Label2 } from '../../components/UI/Labels';
import Input from '../../components/Input';
import { GlobalStyles } from '../../constants/styles';

function SpecialBasketScreen() {
  console.log('[Donation Basket Screen] started ');
  const [isFetchingProducts, setIsFetchingProducts] = useState(false);
  const [error, setError] = useState(null);
  const [productsSpecialList, setProductsSpecialList] = useState([]);

  const deliveryCtx = useContext(DeliveryContext);

  // console.log(
  //   '[Special basket products] delivery',
  //   JSON.stringify(deliveryCtx.delivery, null, 2)
  // );

  function createProductsSpecialList() {
    // console.log('[Donation Basket Screen] create Product Donation List');
    // console.log(
    //   '[Donation Basket Screen] create Product Donation List products donation list before',
    //   JSON.stringify(productsDonationList, null, 2)
    // );
    // console.log(
    //   '[Donation Basket Screen] create Product Donation List delivery',
    //   JSON.stringify(deliveryCtx.delivery, null, 2)
    // );
    const newProductsSpecialList = [];
    deliveryCtx.delivery.products.map((product) => {
      if (product.availableProducts > 0) {
        const newProduct = {
          id: product.id,
          name: product.name,
          imageUrl: product.imageUrl,
          checked: false,
        };
        let index = deliveryCtx.delivery.specialBasketProducts.findIndex(
          (productId) => productId === newProduct.id
        );
        if (index >= 0) {
          console.log('new product', newProduct);
          newProduct.checked = true;
          console.log('new product', newProduct);
        }
        newProductsSpecialList.push(newProduct);
      }
    });
    setProductsSpecialList(newProductsSpecialList);
  }

  useFocusEffect(
    useCallback(() => {
      console.log('Special Basket screen is focused');
      createProductsSpecialList();
    }, [deliveryCtx.delivery])
  );

  function onMessageChangeHandle(value) {
    const deliveryAux = {
      ...deliveryCtx.delivery,
      specialBasketMessage: value,
    };
    deliveryCtx.setDelivery(deliveryAux);
  }

  const checkHandler = (productId) => {
    console.log('product checked', productId);
    deliveryCtx.setBasketProduct('special', productId);
    createProductsSpecialList();
  };

  return (
    <View style={styles.screenContainer}>
      {productsSpecialList.length <= 0 ? (
        <Label2>Não existem itens para serem selecionados</Label2>
      ) : (
        <View style={styles.screenContainer}>
          <View style={styles.messageContainer}>
            <TextInput
              style={styles.message}
              textAlignVertical='top'
              onChangeText={onMessageChangeHandle}
              value={deliveryCtx.delivery.specialBasketMessage}
              placeholder='Uma cesta feira com carinho para você.'
              keyboardType='default'
              multiline={true}  // Allows multiple lines
            />
            {/* <Input
              style={styles.message}
              isValid={true}
              textInputConfig={{
                onChangeText: onMessageChangeHandle,
                value: deliveryCtx.message,
                placeholder: 'Uma cesta feira com carinho para você.',
                keyboardType: 'default',
              }}
            /> */}
          </View>
          <View style={styles.screenTitle}>
            <Label2 style={GlobalStyles.centerText}>
              Escolha os itens da cesta especial
            </Label2>
          </View>
          <View style={styles.productListContainer}>
            <ProductListWithCheck
              products={productsSpecialList}
              checkHandler={checkHandler}
            />
          </View>
        </View>
      )}
    </View>
  );
}

export default SpecialBasketScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  messageContainer: {
    marginHorizontal: 15,
    height: 120,
    // flex: 1,
    // marginHorizontal: 15,
    // height: 120,
    // borderColor: 'red',
    // borderWidth: 1,
  },
  message: {
    height: 100,
    padding: 6,
    borderBottomColor: '#ddb52f',
    borderBottomWidth: 2,
    color: GlobalStyles.colors.textColor,
    fontSize: 16,
    backgroundColor: 'white',
    borderRadius: 6,
    // flex: 1,
    // height: 100,
    textAlign: 'left',
    textAlignVertical: 'top',
    // borderColor: 'red',
    // borderWidth: 1,
  },
  screenTitle: {
    // flex: 1,
    // borderColor: 'red',
    // borderWidth: 1,
  },
  productListContainer: {
    flex: 1,
    // borderColor: 'red',
    // borderWidth: 1,
  },
});
