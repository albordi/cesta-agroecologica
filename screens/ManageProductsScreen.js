import { useContext, useEffect, useLayoutEffect, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Pressable,
  Alert,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import Checkbox from 'expo-checkbox';
import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import firebase from 'firebase/compat/app';
import { isValidUrl } from '../util/validation';
import Input from '../components/Input';
import PrimaryButton from '../components/PrimaryButton';
import { GlobalStyles } from '../constants/styles';
import { ProductsContext } from '../store/context/productscontext';
import { Label1 } from '../components/UI/Labels';
import {
  addDocument,
  updateDocument,
  deleteDocument,
  getDocument,
} from '../util/firebase';
import Spinner from '../components/Spinner';
import ErrorOverlay from '../components/ErrorOverlay';
import HeaderTitle from '../components/HeaderTitle';
import { ScrollView } from 'react-native-gesture-handler';

function ManageProductsScreen({ route, navigation }) {
  console.log('[Manage Products Screen] started');

  const [imageUri, setImageUri] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState(null);
  const [productInputs, setProductInputs] = useState({
    name: { value: '', isValid: true },
    description: { value: '', isValid: true },
    price: { value: '', isValid: true },
    imageUrl: { value: '', isValid: true },
    isVegan: { value: false, isValid: true },
    isNew: { value: false, isValid: true },
    isGlutenFree: { value: false, isValid: true },
    isProcessed: { value: false, isValid: true },
  });
  // console.log(
  //   '[Manage Products Screen] product',
  //   JSON.stringify(productInputs, null, 2)
  // );

  // const [isUpdating, setIsUpdating] = useState();
  const productsCtx = useContext(ProductsContext);

  // For update purposes
  const productId = route.params?.productId;
  // productId
  //   ? console.log('[Manage Products Screen] updating')
  //   : console.log('[Manage Products Screen] adding');
  // let isUpdating = !!productId;

  // console.log('[Manage Products Screen] ', productId);

  const getProductToUpdate = async () => {
    const productToUpdate = await getDocument('products', productId);
    setProductInputs({
      name: { value: productToUpdate.name, isValid: true },
      description: { value: productToUpdate.description, isValid: true },
      price: { value: productToUpdate.price, isValid: true },
      isNew: { value: productToUpdate.isNew, isValid: true },
      isVegan: { value: productToUpdate.isVegan, isValid: true },
      isGlutenFree: { value: productToUpdate.isGlutenFree, isValid: true },
      isProcessed: {
        value: productToUpdate.isProcessed || false,
        isValid: true,
      },
      imageUrl: { value: productToUpdate.imageUrl, isValid: true },
    });
    setImageUri(productToUpdate.imageUrl);
    console.log('[Manage Product Screen] product to update', productToUpdate);
    setIsLoading(false);
  };

  useLayoutEffect(() => {
    setIsLoading(true);
    productId ? getProductToUpdate() : setIsLoading(false);
  }, [productId]);

  const invalidCharsRegex = /[\/\?<>\\:\*\|"\.\)\(]/g;
  const imageName =
    productInputs.name.value.replace(invalidCharsRegex, '') +
    new Date().toString();
  // const imageName = productInputs.name.value + new Date().toString();
  // console.log('[Imagename]',imageName);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: productId ? 'Atualiza produto' : 'Adiciona Produto',
    });
  }, [navigation, productId]);

  function nameChangeHandler(value) {
    // console.log('Product name changed', productInputs);
    const newProduct = {
      ...productInputs,
      name: { value: value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function descriptionChangeHandler(value) {
    // console.log('Product description changed', value);
    const newProduct = {
      ...productInputs,
      description: { value: value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function priceChangeHandler(value) {
    console.log('Product Price changed');
    const formattedValue = value.replace(',', '.');
    const newProduct = {
      ...productInputs,
      price: { value: formattedValue, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function veganChangeHandler() {
    console.log('IsVegan changed');
    const newProduct = {
      ...productInputs,
      isVegan: { value: !productInputs.isVegan.value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function glutenFreeChangeHandler() {
    console.log('IsGluten changed');
    const newProduct = {
      ...productInputs,
      isGlutenFree: { value: !productInputs.isGlutenFree.value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function newChangeHandler() {
    console.log('IsNew changed');
    const newProduct = {
      ...productInputs,
      isNew: { value: !productInputs.isNew.value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  function isProcessedChangeHandler() {
    console.log('IsProcessed changed');
    const newProduct = {
      ...productInputs,
      isProcessed: { value: !productInputs.isProcessed.value, isValid: true },
    };
    setProductInputs(newProduct);
  }

  // function availableQuantityChangeHandler(value) {
  //   console.log('Available Quantity changed');
  //   const newProduct = {
  //     ...productInputs,
  //     availableQuantity: { value: value, isValid: true },
  //   };
  //   setProductInputs(newProduct);
  // }

  // function imageChangeHandler(value) {
  //   console.log('Image changed');
  //   const newProduct = { ...productInputs, image: value };
  //   setProduct(newProduct);
  // }

  //Add or Update
  const confirmButtonHandler = async () => {
    setIsSubmitting(true);
    console.log(
      '[Manage Products Screen] Add Button pressed',
      JSON.stringify(productInputs, null, 2)
    );

    // const productNameIsValid = true;
    // const productPriceIsValid = true;
    // const productImageUrlIsValid = true;

    const productNameIsValid = productInputs.name.value.trim().length > 0;
    const productPriceIsValid =
      !isNaN(productInputs.price.value) && productInputs.price.value >= 0;
    const productImageUrlIsValid = isValidUrl(productInputs.imageUrl.value);

    console.log(
      '[Manage Product Screen] fields valids',
      productNameIsValid,
      productPriceIsValid,
      productImageUrlIsValid
    );
    if (
      !productNameIsValid ||
      !productPriceIsValid ||
      !productImageUrlIsValid
    ) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setProductInputs({
        name: { value: productInputs.name.value, isValid: productNameIsValid },
        description: { value: productInputs.description.value, isValid: true },
        price: {
          value: productInputs.price.value,
          isValid: productPriceIsValid,
        },
        imageUrl: {
          value: productInputs.imageUrl.value,
          isValid: productImageUrlIsValid,
        },
        isNew: { value: productInputs.isNew.value, isValid: true },
        isVegan: { value: productInputs.isVegan.value, isValid: true },
        isGlutenFree: {
          value: productInputs.isGlutenFree.value,
          isValid: true,
        },
        isProcessed: {
          value: productInputs.isProcessed.value,
          isValid: true,
        },
      });
      setIsSubmitting(false);
      return;
    }

    const productToAddOrUpdate = {
      name: productInputs.name.value,
      description: productInputs.description.value,
      price: parseFloat(productInputs.price.value),
      imageUrl: productInputs.imageUrl.value,
      isNew: productInputs.isNew.value,
      isVegan: productInputs.isVegan.value,
      isGlutenFree: productInputs.isGlutenFree.value,
      isProcessed: productInputs.isProcessed.value,
    };

    if (productId) {
      try {
        await updateDocument('products', productId, {
          ...productToAddOrUpdate,
          updatedAt: new Date().toString(),
        });
        productsCtx.updateProduct(productId, productToAddOrUpdate);
        // navigation.emit('productUpdatedOrDeleted');
      } catch (e) {
        console.log('Ocorreu um erro inesperado', e);
        setError('Ocorreu um erro inesperado', e);
      }
    } else {
      try {
        const id = await addDocument('products', {
          ...productToAddOrUpdate,
          createdAt: new Date().toString(),
        });
        productsCtx.addProduct({ ...productToAddOrUpdate, id: id });
      } catch (e) {
        console.log('Ocorreu um erro inesperado', e);
        setError('Ocorreu um erro inesperado', e);
      }
    }
    setIsSubmitting(false);
    navigation.goBack();
  };

  const deleteProductHandler = async () => {
    setIsSubmitting(true);
    // setIsUpdating(false);
    console.log(
      '[Manage Products Screen] delete product handler function',
      productId
    );
    try {
      await deleteDocument('products', productId);
      // productsCtx.removeProduct(productId);
      navigation.goBack();
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
      setError('Ocorreu um erro inesperado', e);
      setIsSubmitting(false);
    }
  };

  // console.log(
  //   '[Manage Products Screen] product',
  //   JSON.stringify(productInputs, null, 2)
  // );

  function cancelButtonHandler() {
    navigation.goBack();
  }

  const pickImage = async () => {
    console.log('[Manage Products Screen] pick image started');
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: 'Images', // We can specify whether we need only Images or Videos
      allowsEditing: true,
      aspect: [2, 2],
      quality: 0.5, // 0 means compress for small size, 1 means compress for maximum quality
      selectionLimit: 1,
    });
    setImageUri(result.assets[0].uri);
    const imageUri = result.assets[0].uri;
    const response = await fetch(imageUri);
    const blob = await response.blob();
    const receiptRef = firebase.storage().ref().child(`products/${imageName}`);
    setIsLoading(true);
    receiptRef
      .put(blob)
      .then((response) => {
        receiptRef
          .getMetadata()
          .then((receiptMetadata) => {
            receiptRef
              .getDownloadURL()
              .then((url) => {
                setImageUrl(url);
                console.log(url);
                const newProduct = {
                  ...productInputs,
                  imageUrl: { value: url, isValid: true },
                };
                setProductInputs(newProduct);
                setIsLoading(false);
                return url;
              })
              .catch((error) => {
                console.log('Erro =>', error);
                Alert.alert(
                  'Erro ao recuperar o endereço da imagem do comprovante do banco de dados!',
                  error.message
                );
              });
          })
          .catch((error) => {
            Alert.alert(
              'Erro ao recuperar os metadados do comprovante de pagamento!',
              error.message
            );
          });
      })
      .catch((error) => {
        Alert.alert(
          'Erro ao armazenar a imagem do comprovante!',
          error.message
        );
      });
  };

  // console.log(
  //   '[Manage Products Screen]',
  //   JSON.stringify(productsCtx.products, null, 2)
  // );
  // console.log('[Manage Products Screen] imageUrl', imageUri);

  const formIsInvalid =
    !productInputs.name.isValid ||
    !productInputs.description.isValid ||
    !productInputs.price.isValid ||
    !productInputs.imageUrl.isValid;

  function errorHandler() {
    setError(null);
    navigation.goBack();
  }

  if (error && !isSubmitting) {
    return <ErrorOverlay message={error} onConfirm={errorHandler} />;
  }

  if (isSubmitting) {
    return <Spinner />;
  }

  return (
    <View style={styles.addNewProductScreenContainer}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{ flex: 1 }}
      >
        <Text style={styles.inputLabel}>Nome</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='Nome do produto'
            value={productInputs.name.value}
            isValid={productInputs.name.isValid}
            textInputConfig={{
              onChangeText: nameChangeHandler,
              autoCapitalize: 'sentences',
              multiline: true,
            }}
          />
        </View>
        <Text style={styles.inputLabel}>Descrição</Text>
        <View style={styles.fieldContainer}>
          <Input
            label='Descrição do produto (Opcional)'
            value={productInputs.description.value}
            isValid={productInputs.description.isValid}
            textInputConfig={{
              onChangeText: descriptionChangeHandler,
              placeholder: 'Descrição do produto (Opcional)',
            }}
          />
        </View>
        <Text style={styles.inputLabel}>Preço</Text>

        <View style={styles.fieldContainer}>
          <Input
            label='Preço'
            value={productInputs.price.value.toString()}
            isValid={productInputs.price.isValid}
            textInputConfig={{
              onChangeText: priceChangeHandler,
              keyboardType: 'numeric',
            }}
          />
        </View>
        <View style={styles.checkboxContainer}>
          <Checkbox
            style={styles.checkbox}
            value={productInputs.isNew.value}
            onValueChange={newChangeHandler}
            color={
              productInputs.isNew.value
                ? GlobalStyles.colors.secondary
                : undefined
            }
          />
          <Text style={styles.inputLabel}>Produto Novo</Text>
        </View>
        <View style={styles.checkboxContainer}>
          <Checkbox
            style={styles.checkbox}
            value={productInputs.isVegan.value}
            onValueChange={veganChangeHandler}
            color={
              productInputs.isVegan.value
                ? GlobalStyles.colors.secondary
                : undefined
            }
          />
          <Text style={styles.inputLabel}>Produto Vegano</Text>
        </View>
        <View style={styles.checkboxContainer}>
          <Checkbox
            style={styles.checkbox}
            value={productInputs.isGlutenFree.value}
            onValueChange={glutenFreeChangeHandler}
            color={
              productInputs.isGlutenFree.value
                ? GlobalStyles.colors.secondary
                : undefined
            }
          />
          <Text style={styles.inputLabel}>Produto Sem Glúten</Text>
        </View>
        <View style={styles.checkboxContainer}>
          <Checkbox
            style={styles.checkbox}
            value={productInputs.isProcessed.value}
            onValueChange={isProcessedChangeHandler}
            color={
              productInputs.isNew.value
                ? GlobalStyles.colors.secondary
                : undefined
            }
          />
          <Text style={styles.inputLabel}>Produto Processado</Text>
        </View>

        <Label1 style={{ textAlign: 'center' }}>Imagem (obrigatória)</Label1>
        <View style={styles.imageContainer}>
          <Pressable onPress={pickImage}>
            <Ionicons name='cloud-upload' size={24} color='black' />
          </Pressable>
          {imageUri ? (
            <Image
              source={{ uri: imageUri }}
              style={{ width: 100, height: 100 }}
            />
          ) : (
            <View style={styles.imagePlaceHolder}>
              <Image
                source={require('../assets/images/imageplaceholder100x100.jpg')}
                style={{ width: 100, height: 100 }}
              />
            </View>
          )}
          {/* <Text>{productInputs.imageUrl.value}</Text> */}
          {isLoading ? <Spinner /> : null}
        </View>
        {formIsInvalid && (
          <Text style={styles.errorText}>Campos inválidos !!!</Text>
        )}
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <PrimaryButton onPress={confirmButtonHandler}>
              {productId ? 'Atualizar' : 'Adicionar'}
            </PrimaryButton>
          </View>
          <View style={styles.buttonContainer}>
            <PrimaryButton onPress={cancelButtonHandler}>
              Cancelar
            </PrimaryButton>
          </View>
        </View>
        {productId && (
          <View style={styles.deleteIconContainer}>
            <Ionicons
              name='trash'
              size={36}
              color={GlobalStyles.colors.alert}
              onPress={deleteProductHandler}
            />
          </View>
        )}
      </KeyboardAvoidingView>
    </View>
  );
}

export const manageProductScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      {/* <Text>Faça ou atualize o seu pedidoalskjdfçlakjfçalsdkjfçalsdkfjçsdlfkj lkjasdlkfj </Text> */}
      <HeaderTitle title='Cadastra/Atualiza Produto' />
    </View>
  ),
  headerStyle: {
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
};

export default ManageProductsScreen;

const styles = StyleSheet.create({
  addNewProductScreenContainer: {
    flex: 1,
    marginTop: 15,
    // backgroundColor: GlobalStyles.colors.primary,
  },
  inputLabel: {
    color: GlobalStyles.colors.secondary,
    marginLeft: 20,
  },
  fieldContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 25,
    marginBottom: 5,
  },
  checkbox: {
    borderColor: GlobalStyles.colors.secondary,
    borderWidth: 1,
  },
  imagePlaceHolder: {
    width: 100,
    height: 100,
    // borderColor: GlobalStyles.colors.primary,
    // borderWidth: 2,
  },
  imageContainer: {
    // flex: 1,
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 20,
  },
  buttonContainer: {
    flex: 1,
  },
  deleteIconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 10,
  },
  errorText: {
    textAlign: 'center',
    color: GlobalStyles.colors.error,
    margin: 8,
  },
});
