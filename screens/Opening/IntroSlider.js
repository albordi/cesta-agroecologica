import { useState, useRef } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  Pressable,
} from 'react-native';
import OpeningScreen1 from './OpeningScreen1';
import { GlobalStyles } from '../../constants/styles';
import OpeningScreen2 from './OpeningScreen2';
import OpeningScreen3 from './OpeningScreen3';
import { Label4 } from '../../components/UI/Labels';

const { width, height } = Dimensions.get('window');

const IntroSlider = ({ navigation }) => {
  const scrollViewRef = useRef(null);
  const [currentPage, setCurrentPage] = useState(0);

  const handleScroll = (event) => {
    const offsetX = event.nativeEvent.contentOffset.x;
    const page = Math.round(
      offsetX / event.nativeEvent.layoutMeasurement.width
    );
    setCurrentPage(page);
  };

  const scrollToPage = (pageNumber) => {
    if (scrollViewRef.current) {
      scrollViewRef.current.scrollTo({
        x: pageNumber * Dimensions.get('window').width,
        animated: true,
      });
    }
  };

  return (
    <View style={styles.screenContainer}>
      <StatusBar hidden />
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView
          ref={scrollViewRef}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onScroll={handleScroll}
          scrollEventThrottle={16} // Adjust the throttle as needed
        >
          {/* <ScrollView
          style={{ flex: 1 }}
          horizontal
          scrollEventThrottle={16}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={true}
          onScroll={(event) => {
            setSliderPage(event);
          }}
        > */}
          <View style={{ width, height }}>
            <OpeningScreen1 currentPage={currentPage} scrollToPage={scrollToPage}/>
            <Pressable
              style={styles.enterButton}
              android_ripple={{ color: GlobalStyles.colors.buttonHoverEffect }}
              onPress={() => navigation.navigate('AuthScreen')}
            >
              <Text style={styles.buttonText}>Entre</Text>
            </Pressable>
          </View>
          <View style={{ width, height }}>
            <OpeningScreen2 currentPage={currentPage} scrollToPage={scrollToPage}/>
            <Pressable
              style={styles.enterButton}
              android_ripple={{ color: GlobalStyles.colors.buttonHoverEffect }}
              onPress={() => navigation.navigate('AuthScreen')}
            >
              <Text style={styles.buttonText}>Entre</Text>
            </Pressable>
          </View>
          <View style={{ width, height }}>
            <OpeningScreen3 currentPage={currentPage} scrollToPage={scrollToPage}/>
            <Pressable
              style={styles.enterButton}
              android_ripple={{ color: GlobalStyles.colors.buttonHoverEffect }}
              onPress={() => navigation.navigate('AuthScreen')}
            >
              <Text style={styles.buttonText}>Entre</Text>
            </Pressable>
          </View>
          {/* <View style={{ width, height }}>
            <Image
              source={require('../../assets/images/introslider/screen3.png')}
              style={styles.imageStyle}
            />
            <View style={styles.wrapper}>
              <Text style={styles.header}>Top Notch Artists</Text>
              <Text style={styles.paragraph}>... all in one place</Text>
            </View>
          </View> */}
        </ScrollView>
        <View style={styles.paginationWrapper}>
          {Array.from(Array(3).keys()).map((key, index) => (
            <View
              style={[
                styles.paginationDots,
                { opacity: currentPage === index ? 1 : 0.2 },
              ]}
              key={index}
            />
          ))}
        </View>
        {/* <Pressable onPress={() => scrollToPage(currentPage + 1)}>
          <Text style={styles.arrowText}>{'>'}</Text>
        </Pressable> */}
        {/* <Pressable
          style={styles.nextButton}
          onPress={() => scrollToPage(currentPage + 1)}
        >
          <Label4 style={styles.nextButtonText}>Próximo Passo</Label4>
        </Pressable> */}
        {/* <View style={styles.arrowContainer}>
          <Pressable onPress={() => scrollToPage(currentPage - 1)}>
            <Text style={styles.arrowText}>{'<'}</Text>
          </Pressable>

          <Text style={styles.pageIndicator}>{`${currentPage + 1}/${
            renderPages().length
          }`}</Text>

          <Pressable onPress={() => scrollToPage(currentPage + 1)}>
            <Text style={styles.arrowText}>{'>'}</Text>
          </Pressable>
        </View> */}
      </SafeAreaView>
    </View>
  );
};

export default IntroSlider;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.primary,
  },
  imageStyle: {
    // height: PixelRatio.getPixelSizeForLayoutSize(200),
    width: width,
    // height: '100%',
    alignSelf: 'center',
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  paragraph: {
    fontSize: 17,
  },
  enterButton: {
    position: 'absolute',
    bottom: 15,
    left: 0,
    right: 0,
  },
  buttonText: {
    fontFamily: 'Antropos',
    color: GlobalStyles.colors.secondary,
    textAlign: 'center',
    // marginBottom: 25,
  },
  paginationWrapper: {
    position: 'absolute',
    bottom: 50,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  paginationDots: {
    height: 10,
    width: 10,
    borderRadius: 10 / 2,
    backgroundColor: GlobalStyles.colors.secondary,
    marginLeft: 10,
  },
});
