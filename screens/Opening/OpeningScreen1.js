import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Pressable,
} from 'react-native';
import { GlobalStyles } from '../../constants/styles';
import GLOBALS from '../../Globals';
import { Label1, Label2, Label3, Label4 } from '../../components/UI/Labels';

function OpeningScreen1({scrollToPage, currentPage}) {
  return (
    <View style={styles.screenContainer}>
      <View style={styles.textSide}>
        <Image
          style={styles.splashImage}
          source={require('../../assets/images/splash.png')}
        />
        <View style={styles.textContainer}>
          <Label4>Oba!</Label4>
          <Label4 style={{ marginBottom: '5%' }}>
            Bem vinda(o) à nossa feira virtual de produtos locais e cheios de
            sabor
          </Label4>
          <View style={styles.paragraphContainer}>
            <View style={[styles.bullet, { backgroundColor: '#d3ac35' }]} />
            <Label3>
              Alimentos produzidos SEM uso de agrotóxicos e fertilizantes
              sintéticos;
            </Label3>
          </View>
          <View style={styles.paragraphContainer}>
            <View style={[styles.bullet, { backgroundColor: '#d3ac35' }]} />
            <Label3>Tudo fresquinho e colhido no dia;</Label3>
          </View>
          <View style={styles.paragraphContainer}>
            <View style={[styles.bullet, { backgroundColor: '#d3ac35' }]} />
            <Label3>Biodiversidade;</Label3>
          </View>
          <View style={styles.paragraphContainer}>
            <View style={[styles.bullet, { backgroundColor: '#d3ac35' }]} />
            <Label3>Mulheres na terra;</Label3>
          </View>
          <View style={styles.paragraphContainer}>
            <View style={[styles.bullet, { backgroundColor: '#d3ac35' }]} />
            <Label3>Produtos saudáveis e nutritivos;</Label3>
          </View>
          <View style={styles.paragraphContainer}>
            <View style={[styles.bullet, { backgroundColor: '#d3ac35' }]} />
            <Label3>
              Cuidado com a sua saúde, com o campo e com o meio ambiente;
            </Label3>
          </View>
        </View>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../assets/images/vasos-e-sementes.png')}
          />
        </View>
        <Pressable
          style={styles.nextButton}
          onPress={() => scrollToPage(currentPage + 1)}
        >
          <Label4 style={styles.nextButtonText}>Próximo Passo</Label4>
        </Pressable>
      </View>
      <View style={styles.colorSide}>
        <View style={styles.color1}></View>
        <View style={styles.color2}></View>
        <View style={styles.color3}></View>
      </View>
    </View>
  );
}

export default OpeningScreen1;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: GlobalStyles.colors.color1,
  },
  textSide: {
    flex: 4,
  },
  splashImage: {
    position: 'absolute',
    left: 15,
    top: 25,
  },
  textContainer: {
    marginLeft: 50,
    marginTop: 60,
    marginRight: 40,
    color: GlobalStyles.colors.primary,
  },
  image: {
    // margin: 25,
    marginLeft: 25,
    width: 180,
    height: 200,
    borderRadius: 10,
  },
  paragraphContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    // marginBottom: 10,
  },
  bullet: {
    marginTop: 5,
    width: 7,
    height: 7,
    borderRadius: 5,
    marginRight: 8,
  },
  colorSide: {
    flex: 1,
    flexDirection: 'row',
  },
  nextButton: {
    flex: 1,
    backgroundColor: '#d3ac35',
    position: 'absolute',
    bottom: 68,
    left: '18%',
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 10,
  },
  nextButtonText: {
    color: 'white',
    textAlign: 'center',
  },
  color1: {
    flex: 1,
    backgroundColor: '#d3ac35',
    width: 10,
  },
  color2: {
    flex: 2,
    backgroundColor: '#824034',
    width: 10,
  },
  color3: {
    flex: 1,
    backgroundColor: '#838a3a',
    width: 10,
  },
});
