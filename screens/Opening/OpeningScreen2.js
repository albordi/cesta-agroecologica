import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Pressable,
} from 'react-native';
import { GlobalStyles } from '../../constants/styles';
import GLOBALS from '../../Globals';
import { Label3,Label4 } from '../../components/UI/Labels';

function OpeningScreen2({scrollToPage, currentPage}) {
  return (
    <View style={styles.screenContainer}>
      <View style={styles.colorSide}>
        <View style={styles.color1}></View>
        <View style={styles.color2}></View>
        <View style={styles.color3}></View>
      </View>
      <View style={styles.textSide}>
        <View style={styles.textContainer}>
          <Label3 style={styles.lineSpace2}>
            Ao comprar e consumir alimentos da Cesta Agroecológica, você permite
            que as agricultoras e agricultores locais continuem vivendo na
            terra, produzindo comida de verdade e protegendo as florestas, as
            matas, a fauna, os rios e todo o ecossistema da nossa região. Você
            também apoia a saúde e bem estar dessas famílias e a transição
            agroecológica que está acontecendo em seu território
          </Label3>
        </View>
        <Pressable
          style={styles.nextButton}
          onPress={() => scrollToPage(currentPage + 1)}
        >
          <Label4 style={styles.nextButtonText}>Próximo Passo</Label4>
        </Pressable>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../assets/images/maos.jpg')}
          />
        </View>
      </View>
    </View>
  );
}

export default OpeningScreen2;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: GlobalStyles.colors.color2,
  },
  textSide: {
    flex: 4,
  },
  textContainer: {
    marginHorizontal: 40,
    marginTop: 80,
    color: GlobalStyles.colors.primary,
  },
  imageContainer: {
    // width: 300,
    // backgroundColor: 'black'
  },
  image: {
    // margin: 25,
    marginTop: -50,
    width: 300,
    height: 330,
    borderRadius: 10,
  },
  colorSide: {
    flex: 1,
    flexDirection: 'row',
  },
  nextButton: {
    // flex: 1,
    marginTop: 50,
    marginLeft: 20,
    backgroundColor: '#d3ac35',
    width: 240,
    height: 50,
    alignContent:'center',
    justifyContent: 'center',
    // position: 'absolute',
    // bottom: 68,
    // left: '18%',
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 10,
    zIndex: 5,
  },
  nextButtonText: {
    color: 'white',
    textAlign: 'center',
  },
  color1: {
    flex: 5,
    backgroundColor: '#658a3d',
    width: 10,
  },
  color2: {
    flex: 3,
    backgroundColor: '#fa9205',
    width: 10,
  },
  color3: {
    flex: 2,
    backgroundColor: '#8e2e2f',
    width: 10,
  },
  lineSpace2 : {
    lineHeight: 24
  }
});
