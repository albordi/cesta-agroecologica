import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Pressable,
} from 'react-native';
import { GlobalStyles } from '../../constants/styles';
import GLOBALS from '../../Globals';
import { Label3, Label4 } from '../../components/UI/Labels';
import { useNavigation } from '@react-navigation/native';

function OpeningScreen3() {
  const navigation = useNavigation();

  return (
    <View style={styles.screenContainer}>
      <View style={styles.textSide}>
        <Label4>FALTA POUCO!</Label4>
        <Image
          style={styles.splashImage}
          source={require('../../assets/images/splashright.png')}
        />
        <View style={styles.textContainer}>
          <Label3 style={{ lineHeight: 24 }}>
            Você está prestes a entrar nessa rede de consumo consciente. Faça
            seu cadastro ou entre no aplicativo !
          </Label3>
        </View>
        <Pressable
          style={styles.nextButton}
          onPress={() => navigation.navigate('AuthScreen')}
        >
          <Label4 style={styles.nextButtonText}>Clique Aqui</Label4>
        </Pressable>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../assets/images/roda.jpg')}
          />
        </View> 
      </View>
      <View style={styles.colorSide}>
        <View style={styles.color1}></View>
        <View style={styles.color2}></View>
        <View style={styles.color3}></View>
      </View>
    </View>
  );
}

export default OpeningScreen3;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  splashImage: {
    position: 'absolute',
    right: 105,
    top: -30,
  },
  textSide: {
    flex: 4,
    marginTop: '15%',
    marginLeft: 30,
  },
  textContainer: {
    marginHorizontal: 40,
    marginTop: 40,
    color: GlobalStyles.colors.primary,
  },
  imageContainer: {
    // width: 300,
  },
  image: {
    margin: 25,
    width: 250,
    height: 250,
    borderRadius: 10,
  },
  colorSide: {
    flex: 1,
    flexDirection: 'row',
  },
  nextButton: {
    // flex: 1,
    marginTop: 50,
    marginLeft: 20,
    backgroundColor: '#d3ac35',
    width: 240,
    height: 50,
    alignContent: 'center',
    justifyContent: 'center',
    // position: 'absolute',
    // bottom: 68,
    // left: '18%',
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 10,
    zIndex: 5,
  },
  nextButtonText: {
    color: 'white',
    textAlign: 'center',
  },
  color1: {
    flex: 1,
    backgroundColor: '#d3ac35',
    width: 10,
  },
  color2: {
    flex: 2,
    backgroundColor: '#824034',
    width: 10,
  },
  color3: {
    flex: 1,
    backgroundColor: '#838a3a',
    width: 10,
  },
});
