import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Pressable,
} from 'react-native';
import { GlobalStyles } from '../../constants/styles';
import GLOBALS from '../../Globals';

function OpeningScreen4({ onDone, navigation }) {
  return (
    <ImageBackground
      style={styles.screenContainer}
      source={require('../../assets/images/screen4.png')}
      resizeMode='stretch'
    >
      <View style={styles.textContainer}>
        <Text style={styles.title}>Falta Pouco!</Text>
        <Text style={styles.subtitle}>
          Você está prestes a entrar nesse rede de consumo consciente.{' '}
        </Text>
        <Text style={styles.subtitle}>Faça o seu cadastro ou faça login.</Text>
        <Pressable
          android_ripple={{ color: GlobalStyles.colors.buttonHoverEffect }}
          onPress={onDone}
        >
          <Text style={styles.buttonText}>Entre...</Text>
        </Pressable>
      </View>

      {/* <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={require('../../assets/images/image1.png')}
        />
      </View> */}
    </ImageBackground>
  );
}

export default OpeningScreen4;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: GlobalStyles.colors.primary,
  },
  textContainer: {
    flex: 1,
    // alignItems: 'flex-start',
    // justifyContent: 'flex-end',
    // padding: 25,
  },
  title: {
    fontFamily: 'Antropos',
    fontSize: 25,
    textAlign: 'left',
    color: GlobalStyles.colors.secondary,
    marginTop: 120,
    marginLeft: 40,
  },
  subtitle: {
    // fontFamily: 'Antropos',
    fontSize: 19,
    // textAlign: 'center',
    marginTop: 30,
    marginLeft: 40,
    marginRight: 120,
    color: GlobalStyles.colors.secondary,
  },
  // textItems: {
  //   marginTop: 50,
  //   marginLeft: 40,
  //   marginRight: 125,
  // },
  // textItem: {
  //   fontFamily: 'Cambria',
  //   fontSize: 15,
  //   color: GlobalStyles.colors.secondary,
  // },
  buttonText: {
    // backgroundColor: 'red',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    marginLeft: 40,
    color: GlobalStyles.colors.secondary,
  },
});
