import { useEffect, useState } from 'react';
import { View, StyleSheet, Modal, Platform } from 'react-native';
import { Label3, Label4, Label5 } from '../../components/UI/Labels';
import PrimaryButton from '../../components/PrimaryButton';
import ProductListSelection from '../../components/ProductListSelection';
import { GlobalStyles } from '../../constants/styles';

function ExtraProductsSelection({
  products,
  onChangeQuantity,
  showModal,
  setShowModal,
}) {
  // console.log('[Extra Products Selection Modal]', showModal);

  return (
    <View style={styles.modalContainer}>
      <Modal
        animationType='slide'
        transparent={true}
        visible={showModal}
        onRequestClose={() => {
          setShowModal(null);
        }}
      >
        <View style={styles.modalContent}>
          <View style={styles.modalView}>
            <Label5
              style={{
                textAlign: 'center',
                color: GlobalStyles.colors.secondary,
                marginLeft: 10,
              }}
            >
              Quer rechear ainda mais sua cesta com outros produtos?
            </Label5>
            <Label3 style={{ textAlign: 'center', marginHorizontal: 10 }}>
              Aproveite! Você tem acesso a preços especiais e exclusivos para
              acrescentar mais delícias à sua mesa
            </Label3>
            <ProductListSelection
              // products={products}
              products={products.sort((a, b) =>
                a.name.toLowerCase().localeCompare(b.name.toLowerCase())
              )}
              onChangeQuantity={onChangeQuantity}
            />
            <View style={styles.buttonsContainer}>
              <View style={styles.buttonContainer}>
                <PrimaryButton onPress={() => setShowModal(false)}>
                  Continuar
                </PrimaryButton>
              </View>
              <View style={styles.buttonContainer}>
                <PrimaryButton onPress={() => setShowModal(false)}>
                  Fechar
                </PrimaryButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

export default ExtraProductsSelection;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
  },
  modalContent: {
    // flex: 1,
    height: '80%',
    width: '100%',
    backgroundColor: GlobalStyles.colors.backGroundColor,
    borderTopRightRadius: 18,
    borderTopLeftRadius: 18,
    position: 'absolute',
    bottom: 0,
    borderColor: GlobalStyles.colors.secondary,
    borderWidth: 3,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  modalView: {
    marginHorizontal: 10,
    height: '70%',
  },
  basketImage: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  confirmButtonContainer: {
    marginHorizontal: 15,
    marginBottom: 10,
  },
  buttonContainer: {
    flex: 1,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 20,
  },
});
