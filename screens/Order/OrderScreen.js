import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, Alert, ScrollView, Platform } from 'react-native';
import { Label3 } from '../../components/UI/Labels';
import { ConsumerGroupContext } from '../../store/context/consumergroupcontext';
import { DeliveryContext } from '../../store/context/deliverycontext';
import { OrderContext } from '../../store/context/ordercontext';
import { UserContext } from '../../store/context/usercontext';
import { GlobalStyles } from '../../constants/styles';
import Spinner from '../../components/Spinner';
import ErrorOverlay from '../../components/ErrorOverlay';
import ProductListWithCheck from '../../components/ProductListWithCheck';
import PrimaryButton from '../../components/PrimaryButton';
import BasketButton from '../../components/BasketButton';
import Divider from '../../components/Divider';
import ConfirmOrderModal from '../../components/ConfirmOrderModal';
import { validateProductsAmount } from '../../util/validation';
import ExtraProductsSelection from './ExtraProductsSelection';
// import { removeChecked } from '../../util/misc';
import HeaderTitle from '../../components/HeaderTitle';
import Popup from '../../components/UI/Popup';
import GLOBALS from '../../Globals';
import { Ionicons } from '@expo/vector-icons';
import OrderConfirmedModal from '../../components/OrderConfirmedModal';
import ShowProductsModal from './ShowProductsModal';

function OrderScreen({ navigation, route }) {
  // console.log('[Order Screen] started.');

  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [showModal, setShowModal] = useState(null);
  // const [currentDelivery, setCurrentDelivery] = useState();
  const [littleBasket, setLittleBasket] = useState([]);
  const [normalBasket, setNormalBasket] = useState([]);
  const [extraProducts, setExtraProducts] = useState([]);
  const [deliveryMessage, setDeliveryMessage] = useState(null);
  const [delivery, setDelivery] = useState({});
  const [order, setOrder] = useState();
  const [user, setUser] = useState({});
  const [showOrderConfirmationModal, setShowOrderConfirmationModal] =
    useState(false);
  const [showOrderConfirmedModal, setShowOrderConfirmedModal] = useState(false);
  const [showExtraProductsSelection, setShowExtraProductsSelection] =
    useState(false);

  // console.log('[Order Screen] route', route);
  const deliveryCtx = useContext(DeliveryContext);
  const consumerGroupCtx = useContext(ConsumerGroupContext);
  const orderCtx = useContext(OrderContext);
  const userCtx = useContext(UserContext);

  const deliveryId = route.params.deliveryId;
  const userId = route.params.userId;
  let orderToUpdate = route.params.order ? route.params.order : null;

  // console.log(
  //   '[Order Screen] started. Consumer Group Information',
  //   JSON.stringify(consumerGroupCtx.consumerGroup.quantityLittleBasket, null, 2)
  // );
  // console.log(
  //   '[Order Screen] started. delivery',
  //   JSON.stringify(delivery, null, 2)
  // );
  // console.log('[Order Screen] order', JSON.stringify(order, null, 2));
  // console.log('[Order Screen] user', JSON.stringify(user, null, 2));
  // console.log(
  //   '[Order Screen] order to update',
  //   JSON.stringify(orderToUpdate, null, 2)
  // );
  // console.log(
  //   '[Order Screen] little basket',
  //   JSON.stringify(littleBasket, null, 2)
  // );
  // console.log(
  //   '[Order Screen] extra products basket',
  //   JSON.stringify(extraProducts, null, 2)
  // );
  // console.log(
  //   '[Order Screen] order to update',
  //   JSON.stringify(orderToUpdate, null, 2)
  // );

  const startOrder = async () => {
    const user = await userCtx.getUserData(userId);
    setUser(user);
    if (orderToUpdate) {
      console.log('[Order Screen] UPDATE ORDER');
      orderAux = route.params.order;
      if (orderToUpdate.donationBasketProducts) {
        // console.log('[Order Screen] setting donation basket');
        orderAux.donationBasket = true;
        delete orderAux.donationBasketProducts;
      }
      if (orderToUpdate.specialBasketProducts) {
        // console.log('[Order Screen] setting donation basket');
        orderAux.specialBasket = true;
        delete orderAux.specialBasketProducts;
      }
    } else {
      console.log('[Order Screen] NEW ORDER');
      //new Order
      orderAux = {
        userId: userId,
        userName: user.name,
        deliveryId: deliveryId,
        specialBasket: false,
        donationBasket: false,
      };
    }
    setOrder(orderAux);
    setIsLoading(false);
  };

  useEffect(() => {
    setIsLoading(true);
    let orderAux = {};
    // console.log('[Order Screen] useEffect started.', order);
    // if (order === undefined) {
    //   orderAux = fetchUserOrder();
    // } //Only if this screen is accessed by individual user
    createBasketProducts();
    startOrder();
    // console.log('[Order Screen] orderAux', JSON.stringify(orderAux, null, 2));
  }, []);

  const createBasketProducts = () => {
    // console.log(deliveryCtx.deliveries);
    const deliveryAux = deliveryCtx.deliveries.find(
      (delivery) => delivery.id === deliveryId
    );
    setDelivery(deliveryAux);

    const baseProductsAux = [];
    // console.log('Base Basket Products',deliveryAux.baseBasketProducts);
    // console.log(
    //   'Delivery Products',
    //   JSON.stringify(deliveryCtx.deliveries, null, 2)
    // );

    deliveryAux.baseBasketProducts.map((productId) => {
      const product = deliveryAux.products.find(
        (item) => item.id === productId
      );
      const productAux = {
        id: product.id,
        name: product.name,
        imageUrl: product.imageUrl,
        description: product.description,
        price: product.price,
        isNew: product.isNew,
        isVegan: product.isVegan,
        isGlutenFree: product.isGlutenFree,
        isProcessed: product.isProcessed,
        checked: false,
      };

      baseProductsAux.push({ ...productAux });
    });
    let littleBasketProducts = baseProductsAux.map((product) => ({
      ...product,
    }));
    // console.log('====', JSON.stringify(littleBasketProducts, null, 2));
    littleBasketProducts = littleBasketProducts.sort((a, b) =>
      a.name.localeCompare(b.name)
    );

    // console.log('[Order Screen] little basket products',JSON.stringify(littleBasketProducts, null, 2));
    let normalBasketProducts = baseProductsAux.map((product) => ({
      ...product,
    }));
    normalBasketProducts = normalBasketProducts.sort((a, b) =>
      a.name.localeCompare(b.name)
    );

    const productsAux = [];
    deliveryAux.extraProducts.map((productId) => {
      const product = deliveryAux.products.find(
        (item) => item.id === productId
      );
      const productAux = {
        id: product.id,
        name: product.name,
        description: product.description,
        imageUrl: product.imageUrl,
        price: product.price,
        isNew: product.isNew,
        isVegan: product.isVegan,
        isGlutenFree: product.isGlutenFree,
        isProcessed: product.isProcessed,
        quantity: 0,
      };
      productsAux.push({ ...productAux });
    });

    const extraProducts = productsAux.map((product) => ({ ...product }));

    if (orderToUpdate) {
      if (orderToUpdate.littleBasket) {
        littleBasketProducts.map((product, index) => {
          const productFound = orderToUpdate.littleBasket.find(
            (productChecked) => productChecked.id === product.id
          );
          if (productFound) {
            littleBasketProducts[index].checked = true;
          }
        });
      }
      if (orderToUpdate.normalBasket) {
        normalBasketProducts.map((product, index) => {
          const productFound = orderToUpdate.normalBasket.find(
            (productChecked) => productChecked.id === product.id
          );
          if (productFound) {
            normalBasketProducts[index].checked = true;
          }
        });
      }
      if (orderToUpdate.extraProducts) {
        extraProducts.map((product, index) => {
          const productFound = orderToUpdate.extraProducts.find(
            (productChecked) => productChecked.id === product.id
          );
          if (productFound) {
            extraProducts[index].checked = true;
            extraProducts[index].quantity = productFound.quantity;
          }
        });
      }
    }

    setLittleBasket(littleBasketProducts);
    setNormalBasket(normalBasketProducts);
    setExtraProducts(extraProducts);
    setDeliveryMessage(deliveryAux.message);
  };

  const setAndCalltModal = (basketType) => {
    // console.log('[Order Screen line 127] setAndCallModal function', basketType);
    // console.log(
    //   '[Order Screen] little basket products',
    //   JSON.stringify(littleBasket, null, 2)
    // );
    // console.log(
    //   '[Order Screen] normal basket products',
    //   JSON.stringify(normalBasket, null, 2)
    // );
    //     console.log(
    //   '[Order Screen] extra products',
    //   JSON.stringify(extraProducts, null, 2)
    // );
    // console.log(
    //   '[Order Screen] ===================================',
    //   JSON.stringify(
    //     consumerGroupCtx.consumerGroup.quantityLittleBasket,
    //     null,
    //     2
    //   )
    // );

    if (basketType === GLOBALS.BASKET.TYPE.LITTLE) {
      console.log('Setting little basket...');
      setShowModal({
        basket: GLOBALS.BASKET.TYPE.LITTLE,
        title: 'Cesta P - Pequena',
        productAmount: consumerGroupCtx.consumerGroup.quantityLittleBasket,
        elo: consumerGroupCtx.consumerGroup.quantityLittleBasket + 1,
        subTitle:
          'Monte sua cesta! Escolha' +
          consumerGroupCtx.consumerGroup.quantityLittleBasket -
          1 +
          'itens diferentes. Agora, se você é um(a) cliente ELO, escolha' +
          consumerGroupCtx.consumerGroup.quantityLittleBasket +
          'itens. Boa feira!',
        products: littleBasket,
        price: consumerGroupCtx.consumerGroup.littleBasketPrice,
      });
    }
    if (basketType === GLOBALS.BASKET.TYPE.NORMAL) {
      console.log('Setting normal basket...');
      setShowModal({
        basket: GLOBALS.BASKET.TYPE.NORMAL,
        title: 'Cesta N - Normal',
        productAmount: consumerGroupCtx.consumerGroup.quantityNormalBasket,
        elo: consumerGroupCtx.consumerGroup.quantityNormalBasket + 1,
        subTitle:
          'Monte sua cesta! Escolha' +
          consumerGroupCtx.consumerGroup.quantityNormalBasket -
          1 +
          'itens diferentes. Agora, se você é um(a) cliente ELO, escolha' +
          consumerGroupCtx.consumerGroup.quantityNormalBasket +
          'itens. Boa feira!',
        products: normalBasket,
      });
    }
    if (basketType === GLOBALS.BASKET.TYPE.EXTRAPRODUCTS) {
      console.log('Setting extra products basket...');
      setShowModal({
        basket: GLOBALS.BASKET.TYPE.EXTRAPRODUCTS,
        title: 'Produtos Extras',
        subTitle: 'Selecione quantos produtos quiser',
        products: extraProducts,
      });
    }
  };

  function setSpecBasket() {
    console.log('SetSpecBasket');
    // setOrder({ ...order, specialBasket: !order.specialBasket });

    //========================
    const deliveryAux = { ...delivery };

    // The code below verify if the donationBasket is being checked and verify if there are products available.
    if (!order.specialBasket) {
      for (const productId of delivery.specialBasketProducts) {
        const product = deliveryAux.products.find((p) => p.id === productId);
        if (product) {
          // Verifica se a quantidade disponível é suficiente
          if (product.availableProducts <= product.purchasedProducts) {
            Alert.alert(
              'Cesta Especial ! Quantidade de produtos insuficiente',
              `Um produto, que faz parte da cesta doação já excedeu a quantidade disponível: ${product.name}.`
            );
            return; // Sai completamente da função
          }
          // Incrementa purchasedProducts
          if (product.purchasedProducts) {
            product.purchasedProducts++;
          } else {
            product.purchasedProducts = 1;
          }
        } else {
          console.log('Produto não encontrado.');
        }
      }
    } else {
      // Cliente está desabilitando a cesta doação. É necessário devolver os produtos ao delivery.
      // Client is unchecking the special bascket. It is necessary to add the products to the products available
      for (const productId of delivery.specialBasketProducts) {
        const product = deliveryAux.products.find((p) => p.id === productId);
        if (product) {
          if (product.purchasedProducts) {
            product.purchasedProducts--;
          }
        } else {
          console.log('Produto não encontrado.');
        }
      }
    }

    // Atualiza delivery e ordem
    setDelivery(deliveryAux);
    // console.log('Order', JSON.stringify(order, null, 2));
    console.log('Delivery', JSON.stringify(deliveryAux, null, 2));
    setOrder({ ...order, specialBasket: !order.specialBasket });
  }

  function setDonBasket() {
    const deliveryAux = { ...delivery };

    // The code below verify if the donationBasket is being checked and verify if there are products available.
    if (!order.donationBasket) {
      for (const productId of delivery.donationBasketProducts) {
        const product = deliveryAux.products.find((p) => p.id === productId);
        if (product) {
          // Verifica se a quantidade disponível é suficiente
          if (product.availableProducts <= product.purchasedProducts) {
            Alert.alert(
              'Quantidade de produtos insuficiente',
              `Um produto, que faz parte da cesta doação já excedeu a quantidade disponível: ${product.name}.`
            );
            return; // Sai completamente da função
          }
          // Incrementa purchasedProducts
          if (product.purchasedProducts) {
            product.purchasedProducts++;
          } else {
            product.purchasedProducts = 1;
          }
        } else {
          console.log('Produto não encontrado.');
        }
      }
    } else {
      // Cliente está desabilitando a cesta doação. É necessário devolver os produtos ao delivery.
      for (const productId of delivery.donationBasketProducts) {
        const product = deliveryAux.products.find((p) => p.id === productId);
        if (product) {
          if (product.purchasedProducts) {
            product.purchasedProducts--;
          }
        } else {
          console.log('Produto não encontrado.');
        }
      }
    }

    // Atualiza delivery e ordem
    setDelivery(deliveryAux);
    // console.log('Order', JSON.stringify(order, null, 2));
    console.log('Delivery', JSON.stringify(deliveryAux, null, 2));
    setOrder({ ...order, donationBasket: !order.donationBasket });
  }

  function errorHandler() {
    setError(null);
    navigation.goBack();
  }

  //Update the delivery to guarantee the purchased product is available. If the product is unavailable the function return false
  const updateDeliveryWithPurchasedProduct = (productId, amount) => {
    console.log(
      '[Order Screen] updatedeliverywithpurchased product',
      productId
    );
    const deliveryAux = { ...delivery };
    const product = deliveryAux.products.find((p) => p.id === productId);

    // Verifica se o produto foi encontrado
    console.log('Product:', product);
    if (product) {
      //Verify if the amount is available to buy
      if (product.availableProducts <= product.purchasedProducts) {
        Alert.alert(
          'Quantidade de produtos insuficiente',
          'O número limite de produtos foi atingido.'
        );
        return false;
      }
      // Se o campo 'purchasedProducts' já existir, adiciona o valor de 'amount' a ele
      if (product.purchasedProducts) {
        product.purchasedProducts += amount;
      } else {
        // Se o campo 'purchasedProducts' não existir, cria e atribui o valor de 'amount'
        product.purchasedProducts = amount;
      }
    } else {
      console.log('Produto não encontrado.');
    }
    setDelivery(deliveryAux);
    return true;
  };

  const checkHandler = (productId) => {
    const isLittleBasket = showModal.basket === GLOBALS.BASKET.TYPE.LITTLE;
    const basketType = isLittleBasket ? 'little' : 'normal';
    const basket = isLittleBasket ? littleBasket : normalBasket;
    const setBasket = isLittleBasket ? setLittleBasket : setNormalBasket;
    const maxProductsAllowed = isLittleBasket
      ? consumerGroupCtx.consumerGroup.quantityLittleBasket
      : consumerGroupCtx.consumerGroup.quantityNormalBasket;
  
    console.log(`[Order Screen] ${basketType} basket`);
  
    const index = basket.findIndex((product) => product.id === productId);
    const basketAux = [...basket];
  
    // Verify if the product is already checked and update delivery
    if (basketAux[index].checked) {
      updateDeliveryWithPurchasedProduct(productId, -1);
    } else {
      if (!updateDeliveryWithPurchasedProduct(productId, 1)) {
        console.log('Sair');
        return;
      }
    }
  
    basketAux[index].checked = !basketAux[index].checked;
  
    // Calculate the total number of selected products
    const productsAmount = basketAux.reduce(
      (count, product) => (product.checked ? count + 1 : count),
      0
    );
  
    // Check if the number of products exceeds the allowed limit
    const maxAllowed = userCtx.user.elo ? maxProductsAllowed + 1 : maxProductsAllowed;
    if (productsAmount > maxAllowed) {
      Alert.alert(
        `Número de produtos da cesta ${basketType} dever ser de ${maxProductsAllowed}`,
        `caso seja consumidor elo ${maxAllowed}`
      );
      basketAux[index].checked = !basketAux[index].checked; // Undo the change
    } else {
      basketAux.productsAmount = productsAmount;
      setBasket(basketAux);
    }
  };  

  const showExtraProducts = () => {
    console.log('[Order Screen] showExtraProducts function');
    setShowModal(null);
    setShowExtraProductsSelection(true);
    // setAndCalltModal(GLOBALS.BASKET.TYPE.EXTRAPRODUCTS);
  };

  const changeExtraProductsQuantity = (productId, quantity) => {
    console.log('[Order Screen] changeExtraProductQuantity');
    const extraProductsAux = [...extraProducts];
    const extraProductToUpdate = extraProductsAux.find(
      (extraProduct) => extraProduct.id === productId
    );
    if (!updateDeliveryWithPurchasedProduct(productId, quantity)) {
      return;
    }

    if (extraProductToUpdate) {
      extraProductToUpdate.quantity = extraProductToUpdate.quantity + quantity;
      if (extraProductToUpdate.quantity < 0) {
        extraProductToUpdate.quantity = 0;
      }
    } else {
      console.log('[Order Screen] extra product não encontrado'); // Handle the case when the object is not found
    }
    setExtraProducts(extraProductsAux);
  };

  const calcTotalOrder = (orderAux) => {
    let totalOrderPrice = 0;
    console.log(
      '[Order Screen] Calc Total Order',
      JSON.stringify(orderAux, null, 2)
    );
    if (orderAux.littleBasket) {
      orderAux.littleBasketPrice = parseFloat(
        consumerGroupCtx.consumerGroup.littleBasketPrice
      );
      totalOrderPrice = totalOrderPrice + orderAux.littleBasketPrice;
      console.log(totalOrderPrice);
    }
    if (orderAux.normalBasket) {
      orderAux.normalBasketPrice = parseFloat(
        consumerGroupCtx.consumerGroup.normalBasketPrice
      );
      totalOrderPrice = totalOrderPrice + orderAux.normalBasketPrice;
    }
    if (orderAux.specialBasketProducts) {
      orderAux.specialBasketPrice = parseFloat(
        consumerGroupCtx.consumerGroup.specialBasketPrice
      );
      totalOrderPrice =
        totalOrderPrice +
        parseFloat(consumerGroupCtx.consumerGroup.specialBasketPrice);
    }
    if (orderAux.donationBasketProducts) {
      console.log(
        '[Order Screen] calc total order donation basket',
        consumerGroupCtx.consumerGroup.donationBasketPrice
      );
      orderAux.donationBasketPrice = parseFloat(
        consumerGroupCtx.consumerGroup.donationBasketPrice
      );
      totalOrderPrice =
        totalOrderPrice +
        parseFloat(consumerGroupCtx.consumerGroup.donationBasketPrice);
    }
    if (orderAux.extraProducts) {
      let extraProductsTotalPrice = 0;
      console.log('Extra Products Total Price', extraProductsTotalPrice);
      orderAux.extraProducts.map((extraProduct) => {
        extraProductsTotalPrice =
          extraProductsTotalPrice + extraProduct.price * extraProduct.quantity;
        console.log('Extra Products Total Price', extraProductsTotalPrice);
      });
      orderAux.extraProductsTotalPrice = extraProductsTotalPrice;
      totalOrderPrice = totalOrderPrice + extraProductsTotalPrice;
    }
    console.log('Total Order Price', totalOrderPrice);
    orderAux.totalOrderPrice = totalOrderPrice;
    return orderAux;
  };

  const prepareOrderToStore = () => {
    let orderAux = {
      ...order,
    };

    // console.log(
    //   '[Order Screen] prepare to store Order Aux',
    //   JSON.stringify(orderAux, null, 2)
    // );
    //Filtra a cesta com os produtos selecionados e remove o campo checked.
    let littleBasketAux = littleBasket
      .filter((product) => product.checked)
      .map(
        ({
          checked,
          isNew,
          isVegan,
          isGlutenFree,
          isProcessed,
          price,
          imageUrl,
          description,
          ...rest
        }) => rest
      );
    // console.log('================', littleBasketAux);
    if (littleBasketAux.length > 0) {
      let errorMessage = validateProductsAmount(
        GLOBALS.BASKET.TYPE.LITTLE,
        littleBasket,
        user.elo,
        consumerGroupCtx.consumerGroup.quantityLittleBasket - 1
      );
      if (errorMessage) {
        Alert.alert('Atenção', errorMessage);
        return;
      }
      // console.log('====', delivery.productInAbundance);
      orderAux = {
        ...orderAux,
        productInAbundance: delivery.productInAbundance,
        littleBasket: littleBasketAux,
      };
    } else {
      delete orderAux.littleBasket;
      delete orderAux.littleBasketPrice;
    }

    // console.log(
    //   '[Order Screen] prepare to store Order Aux after',
    //   JSON.stringify(orderAux, null, 2)
    // );
    let normalBasketAux = normalBasket
      .filter((product) => product.checked)
      .map(
        ({
          checked,
          isNew,
          isVegan,
          isGlutenFree,
          isProcessed,
          price,
          imageUrl,
          description,
          ...rest
        }) => rest
      );
    if (normalBasketAux.length > 0) {
      errorMessage = validateProductsAmount(
        GLOBALS.BASKET.TYPE.NORMAL,
        normalBasket,
        user.elo,
        consumerGroupCtx.consumerGroup.quantityNormalBasket - 1
      );
      if (errorMessage) {
        Alert.alert(errorMessage);
        return;
      }
      orderAux = {
        ...orderAux,
        normalBasket: normalBasketAux,
        productInAbundance: delivery.productInAbundance,
      };
    } else {
      delete orderAux.normalBasket;
      delete orderAux.normalBasketPrice;
    }
    let extraProductsAux = extraProducts
      .filter((product) => product.quantity > 0)
      .map(
        ({ checked, isNew, isVegan, isGlutenFree, isProcessed, ...rest }) =>
          rest
      );
    if (extraProductsAux.length > 0) {
      orderAux = { ...orderAux, extraProducts: extraProductsAux };
    } else {
      // delete orderAux.specialBasket;
      delete orderAux.extraProductsTotalPrice;
    }

    //Store the special basket products to the order
    if (order.specialBasket) {
      specialBasketProductsAux = [];
      // console.log('[OderScreen] special basket selected');
      // console.log(
      //   '[]',
      //   JSON.stringify(delivery.specialBasketProducts, null, 2)
      // );
      delivery.specialBasketProducts.map((specialProductId) => {
        const productAux = delivery.products.find(
          (item) => item.id === specialProductId
        );
        const specialProductAux = {
          id: productAux.id,
          name: productAux.name,
        };
        // let specialProductsAux = delivery.products
        //   .filter((product) => product.id == specialProductId)
        //   .map(({ checked, isNew, isVegan, isGlutenFree, ...rest }) => rest);
        specialBasketProductsAux.push(specialProductAux);
      });
      // console.log(
      //   '[OderScreen] special basket products',
      //   JSON.stringify(specialProductsAux, null, 2)
      // );
      orderAux = {
        ...orderAux,
        specialBasketProducts: specialBasketProductsAux,
      };
      delete orderAux.specialBasket;
    }

    //Store the donation basket products to the order
    if (order.donationBasket) {
      console.log('[OderScreen] donation basket selected');
      donationProductsAux = [];
      delivery.donationBasketProducts.map((donationProductId) => {
        const productAux = delivery.products.find(
          (item) => item.id === donationProductId
        );
        const donationProductAux = {
          id: productAux.id,
          name: productAux.name,
        };
        // let specialProductsAux = delivery.products
        //   .filter((product) => product.id == specialProductId)
        //   .map(({ checked, isNew, isVegan, isGlutenFree, ...rest }) => rest);
        donationProductsAux.push(donationProductAux);
      });
      // console.log(
      //   '[OderScreen] special basket products',
      //   JSON.stringify(specialProductsAux, null, 2)
      // );
      orderAux = { ...orderAux, donationBasketProducts: donationProductsAux };
      delete orderAux.donationBasket;
    }

    // Verificar se existe alguma cesta. Senão remover o pedido.
    if (
      !orderAux.littleBasket &&
      !orderAux.normalBasket &&
      !orderAux.extraProducts &&
      !orderAux.specialBasketProducts &&
      !orderAux.donationBasketProducts
    ) {
      console.log('[Order Screen] Products not selected');
      Alert.alert('Nehuma cesta ou produto foi selecionado');
      //apagar o pedido se ele já existe.
      if (orderToUpdate) {
        console.log('Apagar a ordem.');
        // removeOrder();
      }
      return;
    }

    // console.log('[Order Screen] Order', JSON.stringify(orderAux, null, 2));
    orderAux = calcTotalOrder(orderAux);
    // console.log(
    //   '[Order Screen] prepare to store Order Aux (before store)',
    //   JSON.stringify(orderAux, null, 2)
    // );
    delete orderAux.donationBasket;
    delete orderAux.specialBasket;
    setOrder(orderAux);
    setShowOrderConfirmationModal(true);
  };

  const onHandleOrderSubmit = (payment, deliveryLocal) => {
    const orderAux = order;
    if (deliveryLocal === null) {
      Alert.alert(
        'Local de entrega!',
        'É obrigatório definir o local de entrega.'
      );
      return;
    }

    if (payment.option === null) {
      Alert.alert('Pagamento!', 'É obrigatório definir a forma de pagamento.');
      return;
    }

    orderAux.payment = payment;
    orderAux.deliveryLocal = deliveryLocal;
    if (orderToUpdate) {
      orderCtx.updateOrder(orderToUpdate.id, orderAux, deliveryId, delivery);
    } else {
      // console.log('[Order Screen] add order', orderAux);
      // orderCtx.addOrder(orderAux, deliveryId, delivery);
      orderCtx.createOrderAndUpdateDelivery(orderAux, deliveryId, delivery);
    }
    // Alert.alert(
    //   'Pedido efetuado com sucesso!',
    //   'Agora é só aguardar o dia da entrega.'
    // );
    console.log('[Order Screen] set confirmed Modal to true');
    console.log(showOrderConfirmedModal);
    setShowOrderConfirmedModal(true);
    // console.log(
    //   '[Order Screen] Order to Add or Update',
    //   JSON.stringify(orderAux, null, 2)
    // );
    // navigation.goBack();
    // navigation.navigate('UsersOrdersScreen', {
    //   deliveryId: deliveryCtx.nextDelivery.id,
    // });
    // Subtrair os produtos dessa cesta do total.
    // navigation.navigate('Fazer Pedido');
  };

  const returnToPreviousScreen = () => {
    console.log('Return to previous screen');
    navigation.goBack();
  };

  // console.log('isLoading', isLoading);
  if (isLoading) {
    // console.log('[OrderScreen] isLoading...');
    return <Spinner />;
  }

  const productCount = (basketType) => {
    let products = [];
    if (basketType === GLOBALS.BASKET.TYPE.LITTLE) {
      products = littleBasket.filter((product) => product.checked);
    }
    if (basketType === GLOBALS.BASKET.TYPE.NORMAL) {
      products = normalBasket.filter((product) => product.checked);
    }
    if (basketType === GLOBALS.BASKET.TYPE.EXTRAPRODUCTS) {
      products = extraProducts.filter((product) => product.checked);
    }
    return products.length;
  };

  const closeConfirmationModal = () => {
    setShowOrderConfirmationModal(false);
  };

  const updateProductsAvailabilityonRemoveOrder = (productIds) => {
    console.warn('Inicio da removação do pedido');
    const deliveryAux = { ...delivery };
    // console.log(JSON.stringify(productIds, null, 2));
    productIds.map((productId) => {
      const product = deliveryAux.products.find(
        (product) => product.id === productId
      );
      if (product) {
        product.purchasedProducts--;
      } else {
        console.error(`Product with ID ${product} not found in delivery.`);
      }
    });
    // console.log(JSON.stringify(deliveryAux, null, 2));
    setDelivery(deliveryAux);
  };

  const removeOrder = () => {
    Alert.alert(
      'Deleção',
      'Você tem certeza que deseja apagar esse pedido?',
      [
        {
          text: 'NÃO',
          style: 'cancel',
        },
        {
          text: 'SIM',
          onPress: () => {
            //Restore the order products to the delivery.
            order.littleBasket &&
              updateProductsAvailabilityonRemoveOrder(
                order.littleBasket.map((item) => item.id)
              );

            order.normalBasket &&
              updateProductsAvailabilityonRemoveOrder(
                order.normalBasket.map((item) => item.id)
              );
            order.donationBasket &&
              updateProductsAvailabilityonRemoveOrder(
                delivery.donationBasketProducts.map((item) => item)
              );
            order.specialBasket &&
              updateProductsAvailabilityonRemoveOrder(
                delivery.specialBasketProducts.map((item) => item)
              );

            // updateProductsAvailabilityonRemoveOrder(littleBasketIds);
            orderCtx.removeOrder(order.id, delivery);
            navigation.navigate('Entregas');
          },
        },
      ],
      { cancelable: false }
    );
  };

  // console.log(
  //   '[Order Screen] started. little basket',
  //   JSON.stringify(littleBasket, null, 2)
  // );
  // console.log(
  //   '[Order Screen] started. normal basket',
  //   JSON.stringify(normalBasket, null, 2)
  // );
  // console.log(
  //   '[Order Screen] started. extraProducts',
  //   JSON.stringify(extraProducts, null, 2)
  // );
  // console.log(
  //   '[Order Screen] started. special basket',
  //   JSON.stringify(specialBasket, null, 2)
  // );
  // console.log(
  //   '[Order Screen] started. donation basket',
  //   JSON.stringify(donationBasket, null, 2)
  // );

  return (
    <View style={styles.screenContainer}>
      {/* <View style={styles.generalMessageContainer}>
        <Label1 style={styles.message}>
          {consumerGroupCtx.consumerGroup.message}
        </Label1>
      </View>
      <View style={styles.generalMessageContainer}>
        <Label1 style={styles.message}>{deliveryMessage}</Label1>
      </View> */}
      {/* <HeaderTitle style={{ textAlign: 'center' }} title='Faça o seu pedido' /> */}
      {orderToUpdate && (
        <Label3 style={GlobalStyles.centerText}>
          Você já tem um pedido cadastrado. Você pode editar o seu pedido ou
          cancelar no botão abaixo
        </Label3>
      )}
      <ScrollView style={styles.scrollViewContainer}>
        <BasketButton
          buttonTitle='Cesta P - Pequena'
          style={{
            backgroundColor: GlobalStyles.colors.littleBasketBackground,
          }}
          image={require('../../assets/images/littlebasket.png')}
          // basketDetails='4 itens no total (3 da sua escolha + item em abundância na roça)'
          basketDetails={
            consumerGroupCtx.consumerGroup.quantityLittleBasket +
            ' itens no total (' +
            (consumerGroupCtx.consumerGroup.quantityLittleBasket - 1) +
            ' da sua escolha + item em abundância na roça)'
          }
          basketPrice={consumerGroupCtx.consumerGroup.littleBasketPrice}
          onPress={() => setAndCalltModal(GLOBALS.BASKET.TYPE.LITTLE)}
          pressed={productCount(GLOBALS.BASKET.TYPE.LITTLE) > 0}
        />
        <BasketButton
          buttonTitle='Cesta N - Normal'
          image={require('../../assets/images/normalbasket.png')}
          style={{
            backgroundColor: GlobalStyles.colors.normalBasketBackground,
          }}
          basketDetails={
            consumerGroupCtx.consumerGroup.quantityNormalBasket +
            ' itens no total (' +
            (consumerGroupCtx.consumerGroup.quantityNormalBasket - 1) +
            ' da sua escolha + item em abundância na roça)'
          }
          basketPrice={consumerGroupCtx.consumerGroup.normalBasketPrice}
          onPress={() => setAndCalltModal(GLOBALS.BASKET.TYPE.NORMAL)}
          pressed={productCount(GLOBALS.BASKET.TYPE.NORMAL) > 0}
        />
        {delivery.donationBasketProducts.length > 0 && (
          <BasketButton
            buttonTitle='Cesta D - Doação'
            image={require('../../assets/images/donationbasket.png')}
            style={{
              backgroundColor: GlobalStyles.colors.donationBasketBackground,
            }}
            // basketDetails='7 itens escolhidos por nós, que serão doados à famílias carentes' + {deliveryCtx.nextDelivery.donationBasketMessage}
            basketDetails={deliveryCtx.nextDelivery.donationBasketMessage}
            basketPrice={consumerGroupCtx.consumerGroup.donationBasketPrice}
            onPress={() => setDonBasket()}
            // donationBasket={order.donationBasket}
            pressed={order.donationBasket}
          />
        )}
        {delivery.specialBasketProducts.length > 0 && (
          <BasketButton
            buttonTitle='Cesta Especial'
            image={require('../../assets/images/specialbasket.png')}
            style={{
              backgroundColor: GlobalStyles.colors.specialBasketBackground,
            }}
            basketDetails={deliveryCtx.nextDelivery.specialBasketMessage}
            basketPrice={consumerGroupCtx.consumerGroup.specialBasketPrice}
            onPress={() => setSpecBasket()}
            // specialBasket={order.specialBasket ? order.specialBasket : false}
            pressed={order.specialBasket ? order.specialBasket : false}
          />
        )}

        {showModal !== null
          ? ShowProductsModal(
              user,
              delivery,
              showModal,
              setShowModal,
              checkHandler,
              showExtraProducts
            )
          : null}
        {showExtraProductsSelection && (
          <ExtraProductsSelection
            products={extraProducts}
            onChangeQuantity={changeExtraProductsQuantity}
            showModal={showExtraProductsSelection}
            setShowModal={setShowExtraProductsSelection}
          />
        )}
      </ScrollView>
      <View style={styles.confirmButtonContainer}>
        <PrimaryButton onPress={() => prepareOrderToStore()}>
          Próximo Passo
        </PrimaryButton>
        {orderToUpdate && (
          <PrimaryButton onPress={() => removeOrder()}>
            Cancelar Pedido?
          </PrimaryButton>
        )}
      </View>
      {showOrderConfirmationModal && (
        <View>
          <ConfirmOrderModal
            confirmOrder={onHandleOrderSubmit}
            cancelOrder={closeConfirmationModal}
            order={order}
          />
        </View>
      )}
      {showOrderConfirmedModal && (
        <View>
          <OrderConfirmedModal
            showModal={true}
            onPress={returnToPreviousScreen}
          />
        </View>
      )}
    </View>
  );
}

export const orderScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      {/* <Text>Faça ou atualize o seu pedidoalskjdfçlakjfçalsdkjfçalsdkfjçsdlfkj lkjasdlkfj </Text> */}
      <HeaderTitle title='Faça ou Atualize o seu pedido' />
    </View>
  ),
  headerStyle: {
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
};

export default OrderScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    paddingVertical: 10,
    zIndex: -1,
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  generalMessageContainer: {
    backgroundColor: GlobalStyles.colors.primary,
    borderRadius: 5,
    marginBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 3,
    marginHorizontal: 10,
  },
  message: { textAlign: 'center', padding: 5 },
  scrollViewContainer: {
    marginHorizontal: 15,
    flexGrow: 1,
    height: '50%',
  },
  details: {
    margin: 10,
  },
  priceContainer: {
    flex: 1,
  },
  price: {
    fontSize: 18,
    marginHorizontal: 5,
    textAlign: 'right',
    color: GlobalStyles.colors.secondary,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    // flex: 1,
    height: '90%',
    width: '100%',
    backgroundColor: GlobalStyles.colors.backGroundColor,
    borderTopRightRadius: 18,
    borderTopLeftRadius: 18,
    position: 'absolute',
    bottom: 0,
    borderColor: GlobalStyles.colors.secondary,
    borderWidth: 3,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  modalView: {
    justifyContent: 'space-between',
    // alignItems: 'center',
    marginHorizontal: 10,
    height: '55%',
    backGroundColor: 'blue',
  },
  // productsListContainer: {
  //   height: '90%',

  // },
  basketImage: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  confirmButtonContainer: {
    marginHorizontal: 15,
  },
  buttonContainer: {
    flex: 1,
  },
  buttonsContainer: {
    // flex: 1,
    flexDirection: 'row',
    // justifyContent: 'space-around',
    // alignContent: 'flex-end',

    marginHorizontal: 20,
    backgroundColor: 'red',
    // backGroundColor: 'red',
  },
});
