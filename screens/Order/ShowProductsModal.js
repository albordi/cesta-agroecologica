import { View, Modal, StyleSheet, Platform } from 'react-native';
import {
  Label1,
  Label2,
  Label3,
  Label4,
  Label5,
} from '../../components/UI/Labels';
import ProductListWithCheck from '../../components/ProductListWithCheck';
import { GlobalStyles } from '../../constants/styles';
import Popup from '../../components/UI/Popup';
import PrimaryButton from '../../components/PrimaryButton';

const ShowProductsModal = (
  user,
  delivery,
  showModal,
  setShowModal,
  checkHandler,
  showExtraProducts
) => {
  // console.log(
  //   '[Show Products Modal Component] user',
  //   JSON.stringify(user, null, 2)
  // );

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={showModal !== null}
      onRequestClose={() => {
        setShowModal(null);
      }}
    >
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          <View style={styles.headerContainer}>
            {user.elo ? <Label3>Elo</Label3> : null}
            <Label5
              style={{
                textAlign: 'center',
                color: '#856527',
                marginLeft: 10,
              }}
            >
              {showModal.title} R${showModal.price}
            </Label5>
            <Label3 style={[GlobalStyles.boldText, { marginLeft: 10 }]}>
              Monte sua cesta!
            </Label3>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 10,
              }}
            >
              <Label3 style={GlobalStyles.mb10}>
                Escolha {showModal.productAmount - 1} itens diferentes. Agora se
                você é cliente ELO
                <Popup message='Ser um Cliente Elo é se comprometer a fazer o pedido toda semana e ter a vantagem de escolher um item a mais sem sua cesta pagando o mesmo valor de quem não é Cliente Elo' />
                , escolha {showModal.productAmount} itens. Boa feira !
              </Label3>
            </View>
          </View>
          <View style={styles.productsContainer}>
            <Label1 style={GlobalStyles.centerText}>
              Brinde da semana {delivery.productInAbundance.name.toUpperCase()}
            </Label1>
            <ProductListWithCheck
              products={showModal.products}
              checkHandler={checkHandler}
            />
          </View>
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={() => showExtraProducts()}>
                Continuar
              </PrimaryButton>
            </View>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={() => setShowModal(null)}>
                Fechar
              </PrimaryButton>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default ShowProductsModal;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    flex: 1,
    justifyContent: 'space-between',
    // alignItems: 'flex-end',
    height: '95%',
    width: '100%',
    backgroundColor: GlobalStyles.colors.backGroundColor,
    borderTopRightRadius: 18,
    borderTopLeftRadius: 18,
    position: 'absolute',
    bottom: 0,
    borderColor: GlobalStyles.colors.secondary,
    borderWidth: 1,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  productsContainer: {
    height: '65%',
  },
  buttonContainer: {
    flex: 1,
  },
  buttonsContainer: {
    // flex: 1,
    flexDirection: 'row',
    // alignSelf: 'flex-end',
    // justifyContent: 'space-around',
    // alignContent: 'flex-end',
    marginTop: 10,
    marginHorizontal: 20,
  },
});
