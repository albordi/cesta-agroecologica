import React, { useState, useCallback, useContext } from 'react';
import { View, StyleSheet, FlatList, RefreshControl } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { format } from 'date-fns';

import { Label3 } from '../../components/UI/Labels';
import { DeliveryContext } from '../../store/context/deliverycontext';
import Spinner from '../../components/Spinner';
import { OrderContext } from '../../store/context/ordercontext';
import { UserContext } from '../../store/context/usercontext';
import GLOBALS from '../../Globals';
import OrderButton from '../../components/OrderButton';
import HeaderTitle from '../../components/HeaderTitle';
import { GlobalStyles } from '../../constants/styles';

function UsersOrdersScreen({ navigation, route }) {
  console.log('[Users Orders Screen] started.');
  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [usersOrders, setUsersOrders] = useState();

  const deliveryCtx = useContext(DeliveryContext);
  const orderCtx = useContext(OrderContext);
  const usersCtx = useContext(UserContext);

  const deliveryId = route.params.deliveryId;
  // console.log(
  //   '[Users Orders Screen] next delivery',
  //   JSON.stringify(deliveryCtx.delivery)
  // );
  // console.log(
  //   '[Users Orders Screen] usersOrders',
  //   JSON.stringify(usersOrders, null, 2)
  // );

  const fetchUsersAndOrders = async () => {
    setIsLoading(true);
    console.log('[Users Orders Screen] isLoading');
    const users = await usersCtx.fetchUsers();
    const orders = await orderCtx.getOrders(deliveryId);

    const usersOrdersAux = [];
    // console.log(
    //   '[Users Orders Screen] orders',
    //   JSON.stringify(orderCtx.orders, null, 2)
    // );

    // usersCtx.users.map((user) => {
    users.map((user) => {
      let userOrder = {
        userName: user.name,
        userId: user.id,
      };

      const orderAux = orders.find((item) => item.userId === user.id);
      if (orderAux) {
        userOrder = { ...userOrder, order: orderAux };
        // // userOrder.orderId = orderAux.id;
        // userOrder.order.litleBasket = orderAux.litleBasket ? true : false;
        // userOrder.order.normalBasket = orderAux.normalBasket ? true : false;
        // userOrder.order.specialBasket = orderAux.specialBasket ? true : false;
        // userOrder.order.extraProducts = orderAux.extraProducts ? true : false;
      }
      usersOrdersAux.push(userOrder);
      // console.log('[Users Orders Screen] userOrder', JSON.stringify(userOrder, null, 2));

      // console.log('[Users Orders Screen] ', JSON.stringify(user, null, 2));
    });
    setUsersOrders(usersOrdersAux);
    setIsLoading(false);
    console.log('[Users Orders Screen] FINISH isLoading');
  };

  // useEffect(() => {
  //   fetchUsersAndOrders();
  // }, []);

  useFocusEffect(
    useCallback(() => {
      fetchUsersAndOrders();
    }, [])
  );

  // console.log('[UsersOrdersScreen] deliveryId', deliveryId);
  // console.log('[UsersOrdersScreen] userId', usersCtx.user.id);
  // console.log('[UsersOrdersScreen] reoute', route.params.deliveryId);

  const createOrUpdateOrder = (userId, order) => {
    console.log('[UsersOrdersScreen] Create or Update Order');
    // console.log('[UsersOrdersScreen] userId - orderId', userId, order);
    //Verificar se existe um pedido para esse consumidor.
    // console.log(
    //   'Message:',
    //   JSON.stringify(deliveryCtx.delivery.message, null, 2)
    // );
    // orderId ? (action = 'update') : (action = 'create');

    navigation.navigate('OrderScreen', {
      order: order,
      deliveryId: deliveryId,
      userId: userId,
    });
  };

  function renderUser(itemData) {
    // console.log('[Users Orders Screen] users.', usersCtx.users);
    const userOrder = itemData.item;
    // console.log('Item data',JSON.stringify(userOrder, null, 2));
    // const userOrder = orderCtx.fetchUserOrder(user.id, deliveryId);
    // console.log(
    //   '[Users Orders Screen] order.',
    //   JSON.stringify(userOrder, null, 2)
    // );

    return (
      <View>
        <OrderButton userOrder={userOrder} onClick={createOrUpdateOrder} />
      </View>
    );
  }

  if (isLoading) {
    console.log('isLoading...');
    return <Spinner />;
  }

  return (
    <View style={styles.screenContainer}>
      <Label3 style={{ textAlign: 'center' }}>
        Data da Entrega:
        {format(
          new Date(deliveryCtx.nextDelivery.date),
          GLOBALS.FORMAT.DEFAULT_DATE
        )}
      </Label3>

      <Label3 style={{ textAlign: 'center' }}>
        {deliveryCtx.nextDelivery.message}
      </Label3>
      <FlatList
        style={styles.flatListContainer}
        // data={usersCtx.users}
        data={usersOrders}
        renderItem={renderUser}
        keyExtractor={(item) => item.userId}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => {
              setIsLoading(true);
              fetchUsersAndOrders();
              setIsLoading(false);
            }}
          />
        }
      />
    </View>
  );
}

export const usersOrdersScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Pedidos' />
    </View>
  ),
  // headerBackImage: () => <BackArrow />,
  // headerBackTitleVisible: false,
  // headerStyle: {
  //   backgroundColor: 'transparent',
  //   elevation: 0,
  //   shadowOpacity: 0,
  //   borderBottomWidth: 0,
  // },
};

export default UsersOrdersScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.backGroundColor,
    paddingTop: 10,
  },
});
