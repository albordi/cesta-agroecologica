import { useState, useCallback } from 'react';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  Pressable,
  Image,
  RefreshControl,
  StyleSheet,
  Platform,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import { getCollection } from '../util/firebase';
import Spinner from '../components/Spinner';
import { GlobalStyles } from '../constants/styles';
import { Label2 } from '../components/UI/Labels';
import HeaderTitle from '../components/HeaderTitle';

function ProductsScreen({ navigation }) {
  console.log('[Products Screen] started');
  console.log('[Products Screen] products', products);

  const [isFetchingProducts, setIsFetchingProducts] = useState(true);
  const [error, setError] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [products, setProducts] = useState([]);

  async function getProducts() {
    setIsFetchingProducts(true);
    try {
      let prods = await getCollection('products');
      // prods.sort((a, b) => {
      //   const nameA = a.name.toLowerCase(); // Convert names to lowercase for case-insensitive sorting
      //   const nameB = b.name.toLowerCase();
      //   if (nameA < nameB) return -1;
      //   if (nameA > nameB) return 1;
      //   return 0; // Names are equal
      // });

      prods.sort((a, b) =>
        a.name.toLowerCase().localeCompare(b.name.toLowerCase())
      );

      // prods = prods.sort((a, b) => b.name.toLowerCase() < a.name.toLowerCase())
      // prods = prods.sort((a, b) => a.name.localeCompare(b.name));

      setProducts(prods);
    } catch (error) {
      setError('Houve um erro ao carregar os produtos !!!');
    }
    setIsFetchingProducts(false);
  }

  useFocusEffect(
    useCallback(() => {
      getProducts();
    }, [])
  );

  function onAddProductHandle() {
    // console.log('[Create Delivery Screen] Add new product');
    navigation.navigate('ManageProductsScreen');
  }

  const onUpdateProductHandle = (productId) => {
    // console.log('[Manage Delivery Screen] update product pressed', productId);

    navigation.navigate('ManageProductsScreen', {
      productId: productId,
    });
  };

  function renderProduct(product) {
    // console.log(
    //   '[Product Screen component] item data',
    //   JSON.stringify(product, null, 2)
    // );
    return (
      <Pressable onPress={() => onUpdateProductHandle(product.item.id)}>
        <View style={styles.productContainer}>
          {/* <Text>Product Container</Text> */}
          <Image style={styles.image} source={{ uri: product.item.imageUrl }} />
          <View style={styles.productContent}>
            <Text style={styles.productName}>
              {product.item.name.toUpperCase()}
            </Text>
            {product.item.isNew && (
              <Image
                source={require('../assets/images/icons/novoproduto.png')}
                style={styles.icon}
              />
            )}
            {product.item.isVegan && (
              <Image
                source={require('../assets/images/icons/vegan.png')}
                style={styles.icon}
              />
            )}
            {product.item.isGlutenFree && (
              <Image
                source={require('../assets/images/icons/semgluten.png')}
                style={styles.icon}
              />
            )}
            {product.item.isProcessed && (
              <Image
                source={require('../assets/images/icons/produtoprocessado.png')}
                style={styles.icon}
              />
            )}
          </View>
          <View style={styles.productPriceContainer}>
            <Text style={styles.productPrice}>
              {product.item.price.toFixed(2)}
            </Text>
          </View>
        </View>
      </Pressable>
    );
  }

  return (
    <View>
      <View style={styles.iconContainer}>
        <Pressable onPress={onAddProductHandle}>
          <Ionicons
            name='add-circle'
            size={24}
            color={GlobalStyles.colors.secondary}
          />
        </Pressable>
      </View>
      {products.length === 0 ? (
        <Label2 style={GlobalStyles.ml20}>
          Não existem produtos cadastrados
        </Label2>
      ) : (
        <Label2 style={GlobalStyles.centerText}>
          Clique sobre o produto para alterar
        </Label2>
      )}
      {isFetchingProducts ? (
        <Spinner />
      ) : (
        // <ScrollView
        //   style={styles.productsContainer}
        //   refreshControl={
        //     <RefreshControl
        //       refreshing={refreshing}
        //       onRefresh={() => {
        //         setIsFetchingProducts(true);
        //         getProducts();
        //         setIsFetchingProducts(false);
        //       }}
        //     />
        //   }
        // >
        <FlatList
          style={styles.flatListContainer}
          // data={products}
          data={products}
          renderItem={renderProduct}
          keyExtractor={(item) => item.id}
        />
        // </ScrollView>
      )}
    </View>
  );
}

export const productScreenOptions = {
  headerTitle: () => (
    <View>
      <HeaderTitle title='Gerenciar Produtos' />
    </View>
  ),
};

export default ProductsScreen;

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: 'grey',
  // },
  productContainer: {
    flexDirection: 'row',
    // flexWrap: 'wrap',
    alignItems: 'center',
    marginHorizontal: 20,
    marginVertical: 2,
    paddingVertical: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  productContent: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    // backgroundColor: 'red',
    marginLeft: 5,
  },
  productName: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    // width: '45%',
  },
  productPriceContainer: {
    // flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 10,
    // backgroundColor: 'green',
  },
  // iconContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'flex-end',
  //   marginHorizontal: 20,
  //   zIndex: -1,
  // },

  flatListContainer: {
    flexGrow: 1,
    marginBottom: 50,
  },
  image: {
    width: 60,
    height: '100%',
    marginLeft: 2,
    borderRadius: 5,
  },
  icon: {
    // backgroundColor: 'red',
    width: 30,
    height: 30,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // width: '100%', // Make sure it takes up space
  },
});
