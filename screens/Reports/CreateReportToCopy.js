import * as Clipboard from 'expo-clipboard';

export const createReportToCopy = async (usersOrders) => {
  console.log('[Order Report Screen] create report to copy');
  let reportToCopy = '';
  usersOrders.map((userOrder) => {
    reportToCopy +=
      userOrder.user.name +
      '\n' +
      userOrder.user.address.city +
      '\n' +
      userOrder.user.address.street +
      ',';
    userOrder.user.address.streetNumber +
      '\n' +
      userOrder.user.address.neighborhood +
      '\n' +
      userOrder.user.phone +
      '\n' +
      'Forma de pagamento: ' +
      userOrder.order.payment.option +
      '\n' +
      'Valor total: ' +
      userOrder.order.totalOrderPrice +
      '\n';

    if (userOrder.order.normalBasket) {
      reportToCopy += +'\n' + 'Cesta Normal' + '\n';
      reportToCopy +=
        'Brinde:' + userOrder.order.productInAbundance.name + '\n';
      userOrder.order.normalBasket.map((product) => {
        reportToCopy += product.name + '\n';
      });
    }

    if (userOrder.order.littleBasket) {
      reportToCopy += '\n' + 'Cesta Pequena' + '\n';
      reportToCopy +=
        'Brinde:' + userOrder.order.productInAbundance.name + '\n';
      userOrder.order.littleBasket.map((product) => {
        reportToCopy += product.name + '\n';
      });
    }

    if (userOrder.order.extraProducts) {
      reportToCopy += '\n' + 'Produtos Extras' + '\n';
      userOrder.order.extraProducts.map((product) => {
        reportToCopy += product.quantity + product.name + '\n';
      });
    }

    if (userOrder.order.specialBasketProducts) {
      reportToCopy += '\n' + 'Cesta Especial' + '\n';
      userOrder.order.specialBasketProducts.map((product) => {
        reportToCopy += product.name + '\n';
      });
    }

    if (userOrder.order.donationBasketProducts) {
      reportToCopy += '\n' + 'Cesta Doação' + '\n';
      userOrder.order.donationBasketProducts.map((product) => {
        reportToCopy += product.name + '\n';
      });
    }

    reportToCopy += '-------------------------\n';
  });
  console.log(reportToCopy);

  await Clipboard.setStringAsync(reportToCopy);
};
