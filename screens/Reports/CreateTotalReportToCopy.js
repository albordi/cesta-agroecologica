import * as Clipboard from 'expo-clipboard';

export const createTotalReportToCopy = async (products) => {
  console.log('[Total Report Screen] create report to copy');
  let reportToCopy = '';
  products.map((product) => {
    reportToCopy += product.name;
    for (let i = product.name.length; i < 50; i++){
      reportToCopy += '_';
    }
    reportToCopy +=  product.quantity + '\n';

    // reportToCopy += '-------------------------\n';
  });
  console.log(reportToCopy);

  await Clipboard.setStringAsync(reportToCopy);
};
