import { useState, useContext, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, FlatList, Platform } from 'react-native';
import { DeliveryContext } from '../../store/context/deliverycontext';
import { OrderContext } from '../../store/context/ordercontext';
import { UserContext } from '../../store/context/usercontext';
import Spinner from '../../components/Spinner';
import { Label1, Label2, Label3, Label4 } from '../../components/UI/Labels';
import HeaderTitle from '../../components/HeaderTitle';
import { GlobalStyles } from '../../constants/styles';
import { createReportToCopy } from './CreateReportToCopy';
import { Ionicons } from '@expo/vector-icons';

const OrderReportScreen = ({ route }) => {
  console.log('[Order Report Screen] started.');
  const [isLoading, setIsLoading] = useState(true);
  const [usersOrders, setUsersOrders] = useState([]);

  // console.log(
  //   '[Order Report Screen] usersOrders.',
  //   JSON.stringify(usersOrders, null, 2)
  // );

  // console.log('[Order Report] report to copy', reportToCopy.current);

  const deliveryCtx = useContext(DeliveryContext);
  const orderCtx = useContext(OrderContext);
  const usersCtx = useContext(UserContext);

  const deliveryId = route.params.deliveryId;

  const fetchUsersAndOrders = async () => {
    setIsLoading(true);
    const orders = await orderCtx.getOrders(deliveryId);
    let usersOrdersAux = [];

    await Promise.all(
      orders.map(async (order) => {
        const user = await usersCtx.getUserData(order.userId);
        usersOrdersAux.push({ user, order });
      })
    );

    usersOrdersAux.sort((a, b) => {
      if (
        a.user.address.city.toLowerCase() < b.user.address.city.toLowerCase()
      ) {
        return -1;
      }
      if (
        a.user.address.city.toLowerCase() > b.user.address.city.toLowerCase()
      ) {
        return 1;
      }
      return 0;
    });
    setUsersOrders(usersOrdersAux);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchUsersAndOrders();
  }, []);

  const orderReport = (userOrder) => {
    if (userOrder.order) {
      // console.log('[Order Report]', JSON.stringify(userOrder.order, null, 2));
      return (
        <View>
          {userOrder.order.normalBasket ? (
            <View>
              <Label2>Cesta Normal</Label2>
              <Text style={{ fontWeight: 'bold' }}>
                Brinde: {userOrder.order.productInAbundance.name}
              </Text>
              {userOrder.order.normalBasket.map((product, index) => {
                return <Text key={index}>{product.name}</Text>;
              })}
            </View>
          ) : null}

          {userOrder.order.littleBasket ? (
            <View>
              <Label2>Cesta Pequena</Label2>
              <Text style={{ fontWeight: 'bold' }}>
                {userOrder.order.productInAbundance.name}
              </Text>
              {userOrder.order.littleBasket.map((product, index) => {
                return <Text key={index}>{product.name}</Text>;
              })}
            </View>
          ) : null}

          {userOrder.order.extraProducts ? (
            <View>
              <Label2>Produtos extras</Label2>
              {userOrder.order.extraProducts.map((product, index) => {
                return (
                  <Text key={index}>
                    {product.quantity} {product.name}
                  </Text>
                );
              })}
            </View>
          ) : null}
          {userOrder.order.specialBasketProducts ? (
            <View>
              <Label2>Cesta Especial</Label2>
              {userOrder.order.specialBasketProducts.map((product) => {
                return <Text>{product.name}</Text>;
              })}
            </View>
          ) : null}
          {userOrder.order.donationBasketProducts ? (
            <View>
              <Label2>Cesta Doação</Label2>
              {userOrder.order.donationBasketProducts.map((product) => {
                return <Text>{product.name}</Text>;
              })}
            </View>
          ) : null}
        </View>
      );
    }
  };

  const renderItem = (itemData) => {
    const userOrder = itemData.item;
    return (
      <View key={userOrder.user.email}>
        <View style={styles.cityContainer}>
          <Label4>{userOrder.user.address.city}</Label4>
        </View>
        <View style={styles.userCard}>
          <Label1>{userOrder.user.name}</Label1>
          {/* <Label2>{userOrder.order.deliveryLocal}</Label2> */}
          <View style={styles.addressContainer}>
            {/* <Label2>Endereço</Label2> */}
            <Label2>
              {userOrder.user.address.street},
              {userOrder.user.address.streetNumber} -{' '}
              {userOrder.user.address.city}
            </Label2>
            <Label2>{userOrder.user.address.neighborhood}</Label2>
            <Label2>{userOrder.user.phone}</Label2>

          </View>
          <Label2>Forma de pagamento {userOrder.order.payment.option}</Label2>
          <Label2>Valor total: {userOrder.order.totalOrderPrice}</Label2>
          {/* {orderReport(userOrder)} */}
          {userOrder ? orderReport(userOrder) : null}
        </View>
      </View>
    );
  };

  if (isLoading) {
    console.log('isLoading...');
    return <Spinner />;
  }

  return (
    <View style={styles.screenContainer}>
      <Label3 style={{ textAlign: 'center' }}>Ordenado por cidade</Label3>
      <View style={styles.iconContainer}>
        <Label2>copiar </Label2>
        <Ionicons
          style={styles.updateIcon}
          name='copy'
          size={24}
          color={GlobalStyles.colors.secondary}
          onPress={() => createReportToCopy(usersOrders)}
        />
      </View>
      <FlatList
        style={styles.flatListContainer}
        // data={usersOrders}
        data={usersOrders.sort(
          (a, b) =>
            b.user.address.city.toLowerCase() <
            b.user.address.city.toLowerCase()
        )}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />

      {/* <Label1 style={{textAlign: 'center'}}>Não existem pedidos para essa entrega</Label1> */}
    </View>
  );
};

export const orderReportScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Relatório de Pedidos' />
    </View>
  ),
};

export default OrderReportScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  iconContainer: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginRight: 10,
  },
  cityContainer: {
    marginLeft: 20,
  },
  userCard: {
    backgroundColor: 'white',
    marginHorizontal: 15,
    marginVertical: 5,
    paddingHorizontal: 15,
    minHeight: 50,
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  addressContainer: {
    backgroundColor: '#c7c9b5',
    borderRadius: 3,
  },
});
