import { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, FlatList, Platform } from 'react-native';
import { DeliveryContext } from '../../store/context/deliverycontext';
import { OrderContext } from '../../store/context/ordercontext';
import Spinner from '../../components/Spinner';
import { Label1, Label2, Label3, Label4 } from '../../components/UI/Labels';
import HeaderTitle from '../../components/HeaderTitle';
import { GlobalStyles } from '../../constants/styles';
import { Ionicons } from '@expo/vector-icons';
import { format } from 'date-fns';
import GLOBALS from '../../Globals';
import { createTotalReportToCopy } from './CreateTotalReportToCopy';

const TotalProductsByDeliveryReportScreen = ({ route }) => {
  console.log('[TotalProductsByDelivery Report Screen] started.');
  const [isLoading, setIsLoading] = useState(true);
  const [products, setProducts] = useState([]);

  const orderCtx = useContext(OrderContext);
  const deliveryId = route.params.deliveryId;
  const deliveryCtx = useContext(DeliveryContext);

  console.log('===============> ', deliveryCtx.nextDelivery);

  const fetchOrders = async () => {
    setIsLoading(true);
    let productsAux = [];

    const addOrUpdateProduct = (productName, quantity) => {
      const existingProduct = productsAux.find((p) => p.name === productName);
      if (existingProduct) {
        existingProduct.quantity += quantity;
      } else {
        productsAux.push({ name: productName, quantity: quantity });
      }
    };

    const orders = await orderCtx.getOrders(deliveryId);
    console.log('[]', JSON.stringify(orders, null, 2));
    orders.map((order) => {
      if (order.normalBasket) {
        order.normalBasket.map((product) => {
          addOrUpdateProduct(product.name, 1);
        });
      }
      if (order.littleBasket) {
        order.littleBasket.map((product) => {
          addOrUpdateProduct(product.name, 1);
        });
      }
      if (order.donationBasketProducts) {
        order.donationBasketProducts.map((product) => {
          addOrUpdateProduct(product.name, 1);
        });
      }
      if (order.specialBasket) {
        order.donationBasketProducts.map((product) => {
          addOrUpdateProduct(product.name, 1);
        });
      }
      if (order.extraProducts) {
        order.extraProducts.map((product) => {
          addOrUpdateProduct(product.name, product.quantity);
        });
      }
    });

    // console.log('===',JSON.stringify( productsAux, null, 2));

    setProducts(productsAux);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchOrders();
  }, []);

  if (isLoading) {
    console.log('isLoading...');
    return <Spinner />;
  }

  const renderItem = (itemData) => {
    // console.log(itemData);

    return (
      <View key={itemData.index} style={styles.productCard}>
        <Label3>{itemData.item.name}</Label3>
        <Label3>{itemData.item.quantity}</Label3>
      </View>
    );
  };

  return (
    <View style={styles.screenContainer}>
      <Label1 style={styles.title}>
        Entrega do dia{' '}
        {format(
          new Date(deliveryCtx.nextDelivery.date),
          GLOBALS.FORMAT.DEFAULT_DATE
        )}
      </Label1>
      <View style={styles.iconContainer}>
        <Label2>copiar </Label2>
        <Ionicons
          style={styles.updateIcon}
          name='copy'
          size={24}
          color={GlobalStyles.colors.secondary}
          onPress={() => createTotalReportToCopy(products)}
        />
      </View>
      <FlatList
        style={styles.flatListContainer}
        data={products.sort((a, b) =>
          a.name.toLowerCase().localeCompare(b.name.toLowerCase())
        )}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

export const TotalProductsByDeliveryReportScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Relatório Totalizado' />
    </View>
  ),
};

export default TotalProductsByDeliveryReportScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  title: {
    textAlign: 'center',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginRight: 10,
  },
  flatListContainer: {
    marginVertical: 5,
  },
  productCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    marginHorizontal: 15,
    marginVertical: 2,
    paddingHorizontal: 15,
    // minHeight: 50,
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
});
