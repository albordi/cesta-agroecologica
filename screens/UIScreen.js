import React from 'react';
import { StyleSheet, View, Text, Share, Button } from 'react-native';

import { GlobalStyles } from '../constants/styles';
import HeaderTitle from '../components/HeaderTitle';
import {
  Label1,
  Label2,
  Label3,
  Label4,
  Label5,
  Label6,
} from '../components/UI/Labels';
import { ScrollView } from 'react-native-gesture-handler';

function UIScreen() {
  console.log('[Fonts Screen Started]');
  

  return (
    <ScrollView style={styles.screenContainer}>
      <Label1>Label 1</Label1>
      <Label2>Label 2</Label2>
      <Label3>Label 3</Label3>
      <Label4>Label 4</Label4>
      <Label5>Label 5</Label5>
      <Label6>Label 6</Label6>

      <View style={styles.primaryColor}>
        <Text>Primary Color</Text>
      </View>

    </ScrollView>
  );
}

export const aboutScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='UI Screen' />
    </View>
  ),
};

export default UIScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.primary,
    // padding: 10,
  },
  primaryColor :{
    color: GlobalStyles.colors.primary,
    backgroundColor: GlobalStyles.colors.primary,
    width: 100,
    height: 100,
  }
});
