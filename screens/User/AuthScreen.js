import { useState, useContext, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Alert,
  Platform,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Input from '../../components/Input';
import PrimaryButton from '../../components/PrimaryButton';
import { GlobalStyles } from '../../constants/styles';
import { AuthContext } from '../../store/context/authcontext';
import GLOBALS from '../../Globals';
import Spinner from '../../components/Spinner';
import { isValidEmail } from '../../util/validation';
import { TextInput } from 'react-native-paper';
import { Label3 } from '../../components/UI/Labels';
import { ScrollView } from 'react-native-gesture-handler';
const appJson = require('../../app.json');

function AuthScreen({ navigation }) {
  console.log('[Authscreen] starting...');
  const [user, setUser] = useState({
    // email: { value: 'cesta.agroecologica@gmail.com', isValid: true },
    // password: { value: 'ifsp@1234', isValid: true },
    email: { value: '', isValid: true },
    password: { value: '', isValid: true },
  });
  const [isAuthenticating, setIsAuthenticating] = useState(false);
  const [error, setError] = useState(null);
  const [showPassword, setShowPassword] = useState(false);

  const authCtx = useContext(AuthContext);
  // console.log(
  //   '[Authscreen rendered] AuthCTX',
  //   JSON.stringify(authCtx.isAuthenticated, null, 2)
  // );

  // useEffect(() => {
  //   // console.log(
  //   //   '[Authscreen] useEffect AuthCTX',
  //   //   JSON.stringify(authCtx, null, 2)
  //   // );
  // }, [authCtx]);

  function emailChangeHandler(value) {
    // console.log('Email changed', value);
    const newUser = { ...user, email: { value: value, isValid: true } };
    setUser(newUser);
  }

  function passwordChangeHandler(value) {
    // console.log('Password changed');
    const newUser = { ...user, password: { value: value, isValid: true } };
    setUser(newUser);
  }

  // console.log('user', JSON.stringify(user, null, 2));

  const signinHandler = async () => {
    console.log('[AuthScreen] Setting authenticating to true...');
    setIsAuthenticating(true);
    // console.log('[AuthScreen] Login Button pressed', user);
    // console.log('[AuthScreen] isAuthenticating line 44', isAuthenticating);
    const userEmailIsValid = isValidEmail(user.email.value);
    // console.log('[AuthScreen] userEmailIsValid line 44', userEmailIsValid);

    if (!userEmailIsValid) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setUser({
        email: { value: user.email.value, isValid: userEmailIsValid },
        password: {
          value: user.password.value,
          isValid: user.password.isValid,
        },
      });
      console.log('[AuthScreen] userEmailIsValid line 44', userEmailIsValid);
      setIsAuthenticating(false);
      return;
    }
    try {
      await authCtx.signin(user.email.value, user.password.value);
      setIsAuthenticating(false);
    } catch (error) {
      Alert.alert(
        'Falha no login',
        'Verifique se o e-mail e a senha estão corretos.'
      );
    }
    setIsAuthenticating(false);
    // navigation.navigate('ManageDeliveryScreen');
  };

  function signupHandler() {
    console.log('SignUp ');
    navigation.navigate('SignupScreen');
  }

  async function forgetPasswordHandler() {
    const userEmailIsValid = isValidEmail(user.email.value);
    if (user.email.value && userEmailIsValid) {
      await authCtx.resetPassword(user.email.value);
      Alert.alert(
        'E-mail enviado',
        'Um e-mail foi enviado para você resetar sua password.'
      );
    } else {
      Alert.alert(
        'Dados incorretos',
        'Por favor, verifique se o campo e-mail foi digitado corretamente.'
      );
    }
  }

  // const formIsInvalid = !user.email.isValid || !user.password.isValid;

  return (
    <View style={styles.authScreenContainer}>
      <Image
        style={{ width: '100%' }}
        source={require('../../assets/images/Stripe.png')}
      />
      <Text style={styles.title}>CESTA</Text>
      <Text style={styles.subtitle}>AGROECOLÓGICA</Text>
      <View style={styles.formContainer}>
        <View style={styles.fieldContainer}>
          <Ionicons
            style={styles.iconEmail}
            name='mail'
            size={24}
            color={GlobalStyles.colors.secondary}
          />
          <Input
            style={[styles.input, !user.email.isValid && styles.invalidInput]}
            // label='E-mail'
            placeholder='e-mail'
            isValid={user.email.isValid}
            value={user.email.value}
            textInputConfig={{
              onChangeText: emailChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Ionicons
            style={styles.iconKey}
            name='key'
            size={24}
            color={GlobalStyles.colors.secondary}
          />
          <Input
            style={[
              styles.input,
              !user.password.isValid && styles.invalidInput,
            ]}
            placeholder='senha'
            label='Password'
            // secureTextEntry={!showPassword}
            value={user.password.value}
            isValid={user.password.isValid}
            textInputConfig={{
              secureTextEntry: !showPassword,
              onChangeText: passwordChangeHandler,
            }}
          />

          <Ionicons
            style={styles.iconEye}
            name='eye'
            size={24}
            color={GlobalStyles.colors.secondary}
            onPress={() => setShowPassword(!showPassword)}
          />
          {showPassword && (
            <Ionicons
              style={styles.iconEye}
              name='eye-off'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => setShowPassword(!showPassword)}
            />
          )}
        </View>
        <Text style={styles.forgetPassword} onPress={forgetPasswordHandler}>
          Esqueci minha senha
        </Text>
        <View style={styles.buttonsContainer}>
          <View styles={styles.buttonContainer}>
            <PrimaryButton onPress={signinHandler}>Login</PrimaryButton>
          </View>
        </View>
        <View styles={styles.buttonContainer}>
          <PrimaryButton onPress={signupHandler} style={styles.button}>
            Cadastre-se
          </PrimaryButton>
        </View>
      </View>
      {isAuthenticating && <Spinner />}
      <Image
        style={styles.image}
        source={require('../../assets/images/logo.png')}
      />
      <Label3 style={GlobalStyles.centerText}>
        Version {appJson.expo.version}
      </Label3>
      <Image
        style={{ width: '100%', position: 'absolute', bottom: 0 }}
        source={require('../../assets/images/Stripe.png')}
      />
    </View>
  );
}

export default AuthScreen;

const styles = StyleSheet.create({
  authScreenContainer: {
    flex: 1,
    // justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: GlobalStyles.colors.primary,
  },
  formContainer: {
    width: '90%',
    // backgroundColor:'red'
  },
  fieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    width: '90%',
    // backgroundColor: 'white',
    marginBottom: 15,
  },
  iconEmail: {
    position: 'absolute',
    top: 8,
    left: 3,
    zIndex: 1,
  },
  iconKey: {
    position: 'absolute',
    top: 8,
    left: 3,
    zIndex: 1,
  },
  iconEye: {
    position: 'absolute',
    top: 8,
    right: 3,
    zIndex: 1,
    opacity: 0.4,
  },
  input: {
    // flex: 1,
    // padding: 6,
    paddingLeft: 35,
    // borderBottomColor: '#ddb52f',
    // borderBottomWidth: 2,
    // color: GlobalStyles.colors.textColor,
    // fontSize: 18,
    // fontWeight: 'bold',
    // backgroundColor: 'white',
    // borderRadius: 6,
  },
  buttonsContainer: {
    justifyContent: 'space-around',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    backgroundColor: 'transparent',
    color: GlobalStyles.colors.secondary,
  },
  title: {
    fontFamily: 'CheapPotatoesBlackThin',
    fontSize: 20,
    alignSelf: 'flex-start',
    marginLeft: 40,
    color: GlobalStyles.colors.quinternary,
  },
  subtitle: {
    fontFamily: 'CheapPotatoesBlackThin',
    fontSize: 30,
    alignSelf: 'flex-start',
    marginLeft: 40,
    // marginBottom: 20,
    color: GlobalStyles.colors.secondary,
  },
  forgetPassword: {
    marginTop: -15,
    marginRight: 15,
    marginBottom: 20,
    textAlign: 'right',
    color: GlobalStyles.colors.secondary,
  },
  image: {
    // backgroundColor: 'red',
    alignSelf: 'center',
    width: 150,
    height: 150,
  },
  invalidLabel: {
    color: GlobalStyles.colors.error,
  },
  invalidInput: {
    backgroundColor: GlobalStyles.colors.error,
    opacity: 0.5,
  },
});
