import { useState, useContext } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Alert,
  Pressable,
} from 'react-native';
import Checkbox from 'expo-checkbox';
import { Ionicons } from '@expo/vector-icons';
import { isValidEmail, isValidPhoneNumber } from '../../util/validation';
import Input from '../../components/Input';
import PrimaryButton from '../../components/PrimaryButton';
import { GlobalStyles } from '../../constants/styles';
import { AuthContext } from '../../store/context/authcontext';
import Spinner from '../../components/Spinner';
import { UserContext } from '../../store/context/usercontext';
import GLOBALS from '../../Globals';
import {
  Label1,
  Label2,
  Label3,
  Label4,
  Label5,
} from '../../components/UI/Labels';
import Popup from '../../components/UI/Popup';
import { fetchAddressByCep } from '../../util/misc';

function SignupScreen({ navigation }) {
  console.log('[SignUp screen]');
  const [userInputs, setUserInputs] = useState({
    name: { value: '', isValid: true },
    email: { value: '', isValid: true },
    cep: { value: '', isValid: true },
    state: { value: '', isValid: true },
    city: { value: '', isValid: true },
    neighborhood: { value: '', isValid: true },
    street: { value: '', isValid: true },
    streetNumber: { value: '', isValid: true },
    note: { value: '', isValid: true },
    phone: { value: '', isValid: true },
    notificationToken: { value: '', isValid: true },
    elo: { value: false, isValid: true },
    password: { value: '', isValid: true },
    passwordConfirm: { value: '', isValid: true },
  });
  const [isAuthenticating, setIsAuthenticating] = useState(false);
  const [error, setError] = useState(null);
  const [showPassword, setShowPassword] = useState(false);

  const authCtx = useContext(AuthContext);
  const userCtx = useContext(UserContext);

  // console.log('[SignUp Screen isAuthenticated]', authCtx.isAuthenticated);

  // console.log('[SignUp Screen line 25]', JSON.stringify(userInputs, null, 2));

  function nameChangeHandler(value) {
    console.log('Name changed', value);
    const newUser = { ...userInputs, name: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function emailChangeHandler(value) {
    console.log('Email changed', value);
    const userEmailIsValid = isValidEmail(userInputs.email.value);
    const newUser = {
      ...userInputs,
      email: { value: value, isValid: userEmailIsValid },
    };
    setUserInputs(newUser);
  }

  function phoneChangeHandler(value) {
    // console.log('Phone changed', value);
    const userPhoneIsValid = isValidPhoneNumber(userInputs.phone.value);
    const newUser = {
      ...userInputs,
      phone: { value: value, isValid: userPhoneIsValid },
    };
    setUserInputs(newUser);
  }

  // function cityChangeHandler(value) {
  //   // console.log('City changed', value);
  //   const newUser = { ...userInputs, city: { value: value, isValid: true } };
  //   setUserInputs(newUser);
  // }

  async function cepChangeHandler(value) {
    console.log('Cep changed', value);
    // Remove caracteres não numéricos
    const numericCep = value.replace(/[^\d]/g, '');
    // Adiciona a máscara XXXXX-XXX
    const formattedCep = numericCep.replace(/(\d{5})(\d{3})/, '$1-$2');
    let newUser = {
      ...userInputs,
      cep: { value: formattedCep, isValid: false },
    };

    if (value.length === 8) {
      console.log('[signup screen] cep completed');
      try {
        const { bairro, localidade, logradouro, uf } = await fetchAddressByCep(
          value
        );
        newUser = {
          ...userInputs,
          cep: { value: formattedCep, isValid: true },
          state: { value: uf, isValid: true },
          city: { value: localidade, isValid: true },
          neighborhood: { value: bairro, isValid: true },
          street: { value: logradouro, isValid: true },
        };
      } catch (error) {
        console.log(error);
      }
    }

    setUserInputs(newUser);
  }

  function streetChangeHandler(value) {
    const newUser = {
      ...userInputs,
      street: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function streetNumberChangeHandler(value) {
    // console.log('Street number changed', value);
    const newUser = {
      ...userInputs,
      streetNumber: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function neighborhoodChangeHandler(value) {
    const newUser = {
      ...userInputs,
      neighborhood: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function cityChangeHandler(value) {
    const newUser = {
      ...userInputs,
      city: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function stateChangeHandler(value) {
    const newUser = {
      ...userInputs,
      state: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function noteChangeHandler(value) {
    // console.log('Street number changed', value);
    const newUser = {
      ...userInputs,
      note: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function passwordChangeHandler(value) {
    // console.log('Password changed');
    const userPasswordIsValid = userInputs.password.value.length > 5;
    const newUser = {
      ...userInputs,
      password: { value: value, isValid: userPasswordIsValid },
    };
    setUserInputs(newUser);
  }

  function eloClientChangeHandler() {
    // console.log('Elo client changed');
    const newUser = {
      ...userInputs,
      elo: { value: !userInputs.elo.value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function passwordConfirmChangeHandler(value) {
    // console.log('Confirm Password changed');
    const newUser = {
      ...userInputs,
      passwordConfirm: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  // console.log('[Signup Screen] user', JSON.stringify(userInputs, null, 2));
  // console.log('[Signup Screen] user', userInputs.notification.value ? true : false);

  const handleCheckNotificationToken = async () => {
    console.log('[Sigup Screen] check notification');
    if (!userInputs.notificationToken.value) {
      const pushNotificationToken = await userCtx.setPushNotificationToken();
      console.log(
        '[Signup screen] push notification',
        pushNotificationToken.error
      );
      if (pushNotificationToken.error) {
        Alert.alert(pushNotificationToken.error);
      } else {
        newUser = {
          ...userInputs,
          notificationToken: {
            value: pushNotificationToken.data,
            isValid: true,
          },
        };
        setUserInputs(newUser);
      }
    } else {
      newUser = {
        ...userInputs,
        notificationToken: { value: '', isValid: true },
      };
      setUserInputs(newUser);
    }
  };

  async function signupHandler() {
    console.log('[SignUp Screen] Button pressed');
    setIsAuthenticating(true);
    const userNameIsValid = userInputs.name.value.length >= 1;
    const userEmailIsValid = isValidEmail(userInputs.email.value);
    const userPhoneIsValid = isValidPhoneNumber(userInputs.phone.value);
    const userPasswordIsValid = userInputs.password.value.length > 5;
    // console.log('Validação phone linha 70', userPhoneIsValid);
    // const userCepIsValid = userInputs.cep.value.length === 9;
    const userStreetNumberIsValid = userInputs.streetNumber.value.length > 0;
    const userStreetIsValid = userInputs.street.value.length >= 1;
    const userNeighborhoodIsValid = userInputs.neighborhood.value.length >= 1;
    const userCityIsValid = userInputs.street.value.length >= 1;
    const userStateIsValid = userInputs.street.value.length >= 1;

    console.log(
      userEmailIsValid,
      userPasswordIsValid,
      userNameIsValid,
      userPhoneIsValid,
      userStreetNumberIsValid
    );
    if (
      !userNameIsValid ||
      !userEmailIsValid ||
      !userPasswordIsValid ||
      !userPhoneIsValid ||
      !userStreetNumberIsValid ||
      !userStreetIsValid ||
      !userNeighborhoodIsValid ||
      !userCityIsValid ||
      !userStateIsValid
    ) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setUserInputs({
        name: { value: userInputs.name.value, isValid: userNameIsValid },
        email: { value: userInputs.email.value, isValid: userEmailIsValid },
        phone: { value: userInputs.phone.value, isValid: userPhoneIsValid },
        // cep: { value: userInputs.cep.value, isValid: userCepIsValid },
        cep: { value: userInputs.cep.value, isValid: true },

        state: { value: userInputs.state.value, isValid: userStateIsValid },
        city: { value: userInputs.city.value, isValid: userCityIsValid },
        neighborhood: {
          value: userInputs.neighborhood.value,
          isValid: userNeighborhoodIsValid,
        },
        street: { value: userInputs.street.value, isValid: userStreetIsValid },
        streetNumber: {
          value: userInputs.streetNumber.value,
          isValid: userStreetNumberIsValid,
        },
        note: { value: userInputs.note.value, isValid: true },
        elo: { value: userInputs.elo.value, isValid: true },
        notificationToken: {
          value: userInputs.notificationToken.value,
          isValid: true,
        },
        password: {
          value: userInputs.password.value,
          isValid: userPasswordIsValid,
        },
        passwordConfirm: {
          value: userInputs.passwordConfirm.value,
          isValid: userPasswordIsValid,
        },
      });
      setIsAuthenticating(false);
      return;
    }
    if (userInputs.password.value !== userInputs.passwordConfirm.value) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se as senhas foram digitadas corretamente.'
      );
      setIsAuthenticating(false);
      return;
    }
    const userToCreate = {
      name: userInputs.name.value,
      email: userInputs.email.value,
      phone: userInputs.phone.value,
      role: GLOBALS.USER.ROLE.CONSUMER,
      address: {
        cep: userInputs.cep.value,
        state: userInputs.state.value,
        city: userInputs.city.value,
        neighborhood: userInputs.neighborhood.value,
        street: userInputs.street.value,
        streetNumber: userInputs.streetNumber.value,
        note: userInputs.note.value,
      },
      elo: userInputs.elo.value,
      notificationToken: userInputs.notificationToken.value,
      password: userInputs.password.value,
    };
    try {
      await authCtx.signupUser(userToCreate);
    } catch (error) {
      console.log('[SignUp Screen] error', JSON.stringify(error, null, 2));
      // const teste = error.includes('auth/email-already-in-use');
      // console.log(teste);
      if(error.message.includes('auth/email-already-in-use')){
        Alert.alert(
          'Erro ao cadastrar o usuário',
          'Este e-mail já está cadastrado.\nSe você esqueceu a senha, volte para a tela de login e clique no botão para redefinir sua senha.'
        );
      } else {
        Alert.alert(
          'Erro ao cadastrar o usuário',
          'Verifique se os dados estão corretos.'
        );
      }
    }
    setIsAuthenticating(false);
    console.log(
      '[SignUp Screen] Finalizing creation response -> ',
      isAuthenticating
    );
  }

  const formIsInvalid =
    !userInputs.email.isValid || !userInputs.password.isValid;

  // if (isAuthenticating) {
  //   return <Spinner />;
  // }

  return (
    <View style={styles.signupScreen}>
      <ScrollView>
        <Image
          style={{ width: '100%' }}
          source={require('../../assets/images/Stripe.png')}
        />
        <Text style={styles.title}>Novo Cadastro</Text>

        {/* <Text style={styles.label}>Nome</Text> */}
        <View style={styles.fieldContainer}>
          <Input
            label='Nome'
            placeholder={'Nome'}
            value={userInputs.name.value}
            isValid={userInputs.name.isValid}
            textInputConfig={{
              onChangeText: nameChangeHandler,
            }}
          />
        </View>
        {/* <Text style={styles.label}>E-mail</Text> */}
        <View style={styles.fieldContainer}>
          <Input
            label='E-mail'
            placeholder={'E-mail'}
            value={userInputs.email.value}
            isValid={userInputs.email.isValid}
            textInputConfig={{
              onChangeText: emailChangeHandler,
            }}
          />
        </View>
        {/* <Text style={styles.label}>Telefone 99 99999999</Text> */}
        <View style={styles.fieldContainer}>
          <Input
            label='Telefone'
            placeholder={'Celular'}
            value={userInputs.phone.value}
            isValid={userInputs.phone.isValid}
            textInputConfig={{
              onChangeText: phoneChangeHandler,
              placeholder: '99 999999999',
            }}
          />
        </View>
        {/* <Text style={styles.label}>CEP (xxxxx-xx)</Text> */}
        <View style={styles.fieldContainer}>
          <Input
            label='Cep'
            placeholder={'Cep'}
            value={userInputs.cep.value}
            isValid={userInputs.cep.isValid}
            textInputConfig={{
              onChangeText: cepChangeHandler,
              keyboardType: 'numeric',
              maxLength: 10,
              placeholder: '99999-99',
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='Rua'
            placeholder='Rua'
            value={userInputs.street.value}
            isValid={userInputs.street.isValid}
            textInputConfig={{
              onChangeText: streetChangeHandler,
            }}
          />
        </View>
        {/* <Text style={styles.label}>Número da casa</Text> */}
        <View style={styles.fieldContainer}>
          <Input
            label='Número da Casa'
            placeholder='Número da Casa'
            value={userInputs.streetNumber.value}
            isValid={userInputs.streetNumber.isValid}
            textInputConfig={{
              onChangeText: streetNumberChangeHandler,
              keyboardType: 'numeric',
              maxLength: 5,
              // placeholder: '999',
            }}
          />
        </View>
        {/* <Text style={styles.label}>Complemento (Opcional)</Text> */}
        <View style={styles.fieldContainer}>
          <Input
            label='Complemento'
            placeholder={'Complemento (Opcional)'}
            value={userInputs.note.value}
            isValid={userInputs.note.isValid}
            textInputConfig={{
              onChangeText: noteChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='Bairro'
            placeholder='Bairro'
            value={userInputs.neighborhood.value}
            isValid={userInputs.neighborhood.isValid}
            textInputConfig={{
              onChangeText: neighborhoodChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='Cidade'
            placeholder='Cidade'
            value={userInputs.city.value}
            isValid={userInputs.city.isValid}
            textInputConfig={{
              onChangeText: cityChangeHandler,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Input
            label='Estado'
            placeholder='Estado'
            value={userInputs.state.value}
            isValid={userInputs.state.isValid}
            textInputConfig={{
              onChangeText: stateChangeHandler,
            }}
          />
        </View>
        <Label2 style={{textAlign: 'center'}}>A senha deve ter mais que 5 caracteres</Label2>
        <View style={styles.fieldContainer}>
          <Input
            label='Password'
            placeholder={'Senha'}
            value={userInputs.password.value}
            isValid={userInputs.password.isValid}
            textInputConfig={{
              onChangeText: passwordChangeHandler,
              secureTextEntry: !showPassword,
            }}
          />
          <Ionicons
            style={{
              position: 'absolute',
              top: 8,
              right: 3,
              zIndex: 1,
              opacity: 0.4,
            }}
            name='eye'
            size={24}
            color={GlobalStyles.colors.secondary}
            onPress={() => setShowPassword(!showPassword)}
          />
          {showPassword && (
            <Ionicons
              style={{
                position: 'absolute',
                top: 8,
                right: 3,
                zIndex: 1,
                opacity: 0.4,
              }}
              name='eye-off'
              size={24}
              color={GlobalStyles.colors.secondary}
              onPress={() => setShowPassword(!showPassword)}
            />
          )}
        </View>
        {/* <Text style={styles.label}>Confirme a senha</Text> */}
        <View style={styles.fieldContainer}>
          <Input
            label='Confirme sua password'
            placeholder={'Confirme sua senha'}
            value={userInputs.passwordConfirm.value}
            isValid={userInputs.passwordConfirm.isValid}
            textInputConfig={{
              onChangeText: passwordConfirmChangeHandler,
              secureTextEntry: !showPassword,
            }}
          />
        </View>
        <View style={styles.fieldContainer}>
          <Checkbox
            style={styles.checkbox}
            value={userInputs.notificationToken.value ? true : false}
            // value={false}
            isValid={userInputs.notificationToken.isValid}
            onValueChange={() => handleCheckNotificationToken()}
            color={
              userInputs.notificationToken.value
                ? GlobalStyles.colors.secondary
                : undefined
            }
          />
          <Label3> Permitir notificação</Label3>
          <Pressable>
            <Popup message='Ao clicar você receberá uma notificação no celular toda vez que uma nova entrega for agendada.' />
          </Pressable>
        </View>
        <View style={styles.fieldContainer}>
          <Checkbox
            style={styles.checkbox}
            value={userInputs.elo.value ? true : false}
            // value={false}
            isValid={userInputs.elo.isValid}
            onValueChange={() => eloClientChangeHandler()}
            color={
              userInputs.elo.value ? GlobalStyles.colors.secondary : undefined
            }
          />
          <Label3> Quero ser Cliente ELO</Label3>
          <Pressable>
            {/* <Ionicons
              name='information-circle'
              size={24}
              color={GlobalStyles.colors.secondary}
            /> */}
            <Popup message='Ser um Cliente Elo é se comprometer a fazer o pedido toda semana e ter a vantagem de escolher um item a mais em sua cesta pagando o mesmo valor de quem não é Cliente Elo' />
          </Pressable>
        </View>

        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <PrimaryButton onPress={signupHandler}>Confirmar</PrimaryButton>
          </View>
        </View>
        <View styles={styles.buttonContainer}>
          <PrimaryButton
            onPress={() => navigation.navigate('AuthScreen')}
            style={styles.button}
          >
            Tela de login
          </PrimaryButton>
        </View>
        {isAuthenticating && <Spinner />}
      </ScrollView>
      <Image
        style={{ width: '100%' }}
        source={require('../../assets/images/Stripe.png')}
      />
    </View>
  );
}

export default SignupScreen;

const styles = StyleSheet.create({
  signupScreen: {
    flex: 1,
    backgroundColor: GlobalStyles.colors.primary,
    justifyContent: 'space-between',
    // alignItems: 'center',
  },
  title: {
    fontFamily: 'Antropos',
    fontSize: 30,
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15,
    color: GlobalStyles.colors.secondary,
  },
  fieldContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: 30,
    marginBottom: 5,
    // width: '80%',
    // backgroundColor: 'red',
  },
  label: {
    color: GlobalStyles.colors.secondary,
    marginHorizontal: 40,
    fontSize: 18,
    // fontWeight: 'bold',
  },
  addressContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 40,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 40,
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    backgroundColor: 'transparent',
    color: GlobalStyles.colors.secondary,
  },
});
