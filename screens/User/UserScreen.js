import { useState, useContext, useEffect } from 'react';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  Pressable,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Checkbox from 'expo-checkbox';
import { UserContext } from '../../store/context/usercontext';
import { GlobalStyles } from '../../constants/styles';
import UserCard from '../../components/UserCard';
import PrimaryButton from '../../components/PrimaryButton';
import Input from '../../components/Input';
import { isValidPhoneNumber } from '../../util/validation';
import HeaderTitle from '../../components/HeaderTitle';
import Popup from '../../components/UI/Popup';
import { Label2, Label3 } from '../../components/UI/Labels';
import { fetchAddressByCep } from '../../util/misc';

function UserScreen({ route, navigation }) {
  console.log('[User screen] started');
  const userCtx = useContext(UserContext);
  const user = route.params ? route.params.user : userCtx.user;

  // console.log('[User screen] ', JSON.stringify(user, null, 2));

  const [userInputs, setUserInputs] = useState({
    name: { value: user.name || '', isValid: true },
    phone: { value: user.phone || '19981380000', isValid: true },
    cep: { value: user.address.cep || '', isValid: true },
    state: { value: user.address.state , isValid: true },
    city: { value: user.address.city || '', isValid: true },
    neighborhood: { value: user.address.neighborhood, isValid: true },
    street: { value: user.address.street, isValid: true },
    streetNumber: { value: user.address.streetNumber, isValid: true },
    note: { value: user.address.note, isValid: true },

    email: { value: user.email },
    elo: { value: user.elo || false, isValid: true },
    notificationToken: { value: user.notificationToken || '', isValid: true },
  });
  const [isSubmitting, setIsSubmitting] = useState(false);

  function nameChangeHandler(value) {
    console.log('Name changed', value);
    const newUser = { ...userInputs, name: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  async function cepChangeHandler(value) {
    console.log('Cep changed', value);
    // Remove caracteres não numéricos
    const numericCep = value.replace(/[^\d]/g, '');
    // Adiciona a máscara XXXXX-XXX
    const formattedCep = numericCep.replace(/(\d{5})(\d{3})/, '$1-$2');
    let newUser = {
      ...userInputs,
      cep: { value: formattedCep, isValid: false },
    };
    console.log('Formatted cep', formattedCep);

    if (formattedCep.length === 9) {
      console.log('[signup screen] cep completed');
      try {
        const { bairro, localidade, logradouro, uf } = await fetchAddressByCep(
          value
        );
        newUser = {
          ...userInputs,
          cep: { value: formattedCep, isValid: true },
          state: { value: uf, isValid: true },
          city: { value: localidade, isValid: true },
          neighborhood: { value: bairro, isValid: true },
          street: { value: logradouro, isValid: true },
        };
      } catch (error) {
        console.log(error);
      }
    }

    setUserInputs(newUser);
  }

  function streetChangeHandler(value) {
    const newUser = {
      ...userInputs,
      street: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function streetNumberChangeHandler(value) {
    // console.log('Street number changed', value);
    const newUser = {
      ...userInputs,
      streetNumber: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function neighborhoodChangeHandler(value) {
    const newUser = {
      ...userInputs,
      neighborhood: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function cityChangeHandler(value) {
    const newUser = {
      ...userInputs,
      city: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function stateChangeHandler(value) {
    const newUser = {
      ...userInputs,
      state: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function noteChangeHandler(value) {
    // console.log('Street number changed', value);
    const newUser = {
      ...userInputs,
      note: { value: value, isValid: true },
    };
    setUserInputs(newUser);
  }

  function phoneChangeHandler(value) {
    console.log('Phone changed', value);
    const newUser = { ...userInputs, phone: { value: value, isValid: true } };
    setUserInputs(newUser);
  }

  function eloChangeHandler() {
    console.log('Elo changed');
    const newUser = {
      ...userInputs,
      elo: { value: !userInputs.elo.value, isValid: true },
    };
    setUserInputs(newUser);
  }

  const handleCheckNotificationToken = async () => {
    console.log('[User Screen] check notification');
    if (!userInputs.notificationToken.value) {
      const pushNotificationToken = await userCtx.setPushNotificationToken();
      console.log(
        '[Signup screen] push notification',
        pushNotificationToken.error
      );
      if (pushNotificationToken.error) {
        Alert.alert(pushNotificationToken.error);
      } else {
        newUser = {
          ...userInputs,
          notificationToken: {
            value: pushNotificationToken.data,
            isValid: true,
          },
        };
        setUserInputs(newUser);
      }
    } else {
      
      newUser = {
        ...userInputs,
        notificationToken: { value: '', isValid: true },
      };
      setUserInputs(newUser);
    }
  };

  async function onHandleUpdate() {
    console.log(
      '[UserScreen handle update user]',
      JSON.stringify(userInputs, null, 2)
    );
    console.log('[SignUp Screen] Button pressed');
    setIsSubmitting(true);
    const userNameIsValid = userInputs.name.value.length >= 1;
    const userPhoneIsValid = isValidPhoneNumber(userInputs.phone.value);
    // const userCityIsValid = userInputs.city.value.length >= 2;
    // const userCepIsValid = userInputs.cep.value.length === 9;
    const userStreetNumberIsValid = userInputs.streetNumber.value.length > 0;

    const userStreetIsValid = userInputs.street.value.length >= 1;
    const userNeighborhoodIsValid = userInputs.neighborhood.value.length >= 1;
    const userCityIsValid = userInputs.street.value.length >= 1;
    const userStateIsValid = userInputs.street.value.length >= 1;

    if (
      !userNameIsValid ||
      !userPhoneIsValid ||
      !userCityIsValid ||
      !userStreetNumberIsValid ||
      !userStreetIsValid ||
      !userNeighborhoodIsValid ||
      !userCityIsValid ||
      !userStateIsValid
    ) {
      Alert.alert(
        'Entrada de dados inválida',
        'Por favor, verifique se os campos foram digitados corretamente.'
      );
      setUserInputs({
        name: { value: userInputs.name.value, isValid: userNameIsValid },
        email: { value: user.email },
        phone: { value: user.phone, isValid: userPhoneIsValid },
        cep: { value: userInputs.cep.value, isValid: userCepIsValid },
        state: { value: userInputs.state.value, isValid: userStateIsValid },
        city: { value: userInputs.city.value, isValid: userCityIsValid },
        neighborhood: { value: userInputs.neighborhood.value, isValid: userNeighborhoodIsValid },
        street: { value: userInputs.street.value, isValid: userStreetIsValid },
        streetNumber: {
          value: userInputs.streetNumber.value,
          isValid: userStreetNumberIsValid,
        },
        note: { value: userInputs.note.value, isValid: true },
        city: { value: userInputs.city.value, isValid: userCityIsValid },
        elo: { value: userInputs.elo.value, isValid: true },
        notificationToken: {
          value: userInputs.notificationToken.value,
          isValid: true,
        },
      });
      setIsSubmitting(false);
      return;
    }
    const userToUpdate = {
      name: userInputs.name.value,
      email: user.email,
      role: user.role,
      phone: userInputs.phone.value,
      address: {
        cep: userInputs.cep.value,
        state: userInputs.state.value,
        city: userInputs.city.value,
        neighborhood: userInputs.neighborhood.value,
        street: userInputs.street.value,
        streetNumber: userInputs.streetNumber.value,
        note: userInputs.note.value,
      },
      elo: userInputs.elo.value,
      notificationToken: userInputs.notificationToken.value,
    };
    console.log('user', userToUpdate);
    try {
      await userCtx.updateUser(user.id, userToUpdate);
    } catch (error) {
      console.log('[SignUp Screen] error', error);
      Alert.alert(
        'Erro ao atualizar o usuário',
        'Verifique se os dados estão corretos.'
      );
    }
    setIsSubmitting(false);
    navigation.goBack();
  }

  function cancelButtonHandler() {
    navigation.goBack();
  }

  return (
    <ScrollView style={styles.screenContainer}>
      {/* <KeyboardAvoidingView behavior='height' style={styles.container}> */}
      {/* <Text style={styles.title}>Atualiza Cadastro</Text> */}
      <Text style={styles.label}>Nome</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Nome'
          placeholder={'Nome'}
          value={userInputs.name.value}
          isValid={userInputs.name.isValid}
          textInputConfig={{
            onChangeText: nameChangeHandler,
          }}
        />
      </View>
      <Label2 style={styles.label}>{userInputs.email.value}</Label2>

      <Text style={styles.label}>Celular</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Telefone'
          value={userInputs.phone.value}
          isValid={userInputs.phone.isValid}
          textInputConfig={{
            onChangeText: phoneChangeHandler,
          }}
        />
      </View>
      <Text style={styles.label}>CEP</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='CEP'
          value={userInputs.cep.value}
          isValid={userInputs.cep.isValid}
          textInputConfig={{
            onChangeText: cepChangeHandler,
            keyboardType: 'numeric',
            maxLength: 10,
            placeholder: '99999-99',
          }}
        />
      </View>
      <Text style={styles.label}>Rua</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Rua'
          placeholder='Rua'
          value={userInputs.street.value}
          isValid={userInputs.street.isValid}
          textInputConfig={{
            onChangeText: streetChangeHandler,
          }}
        />
      </View>
      <Text style={styles.label}>Número da casa</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Número da Casa'
          placeholder='Número da Casa'
          value={userInputs.streetNumber.value}
          isValid={userInputs.streetNumber.isValid}
          textInputConfig={{
            onChangeText: streetNumberChangeHandler,
            keyboardType: 'numeric',
            maxLength: 5,
            // placeholder: '999',
          }}
        />
      </View>
      <Text style={styles.label}>Complemento (Opcional)</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Complemento'
          placeholder={'Complemento (Opcional)'}
          value={userInputs.note.value}
          isValid={userInputs.note.isValid}
          textInputConfig={{
            onChangeText: noteChangeHandler,
          }}
        />
      </View>
      <Text style={styles.label}>Bairro</Text>
      <View style={styles.fieldContainer}>
        <Input
          label='Bairro'
          placeholder='Bairro'
          value={userInputs.neighborhood.value}
          isValid={userInputs.neighborhood.isValid}
          textInputConfig={{
            onChangeText: neighborhoodChangeHandler,
          }}
        />
      </View>
      <Text style={styles.label}>Cidade</Text>

      <View style={styles.fieldContainer}>
        <Input
          label='Cidade'
          placeholder='Cidade'
          value={userInputs.city.value}
          isValid={userInputs.city.isValid}
          textInputConfig={{
            onChangeText: cityChangeHandler,
          }}
        />
      </View>
      <Text style={styles.label}>Estado</Text>

      <View style={styles.fieldContainer}>
        <Input
          label='Estado'
          placeholder='Estado'
          value={userInputs.state.value}
          isValid={userInputs.state.isValid}
          textInputConfig={{
            onChangeText: stateChangeHandler,
          }}
        />
      </View>
      {/* {userInputs.cep.value.length === 9 && (
        <View>
          <View style={styles.addressContainer}>
            <Label2>Estado: {userInputs.state.value} </Label2>
            <Label2>Cidade: {userInputs.city.value} </Label2>
            <Label2>Bairro: {userInputs.neighborhood.value} </Label2>
            <Label2>Rua: {userInputs.street.value} </Label2>
            <Label2>Número: {userInputs.streetNumber.value} </Label2>
            {userInputs.note.value && (
              <Label2>Complemento: {userInputs.note.value} </Label2>
            )}
          </View>
          <View style={styles.fieldContainer}>
            <Input
              label='Número da Casa'
              value={userInputs.streetNumber.value}
              isValid={userInputs.streetNumber.isValid}
              textInputConfig={{
                onChangeText: streetNumberChangeHandler,
                keyboardType: 'numeric',
                maxLength: 5,
                // placeholder: '999',
              }}
            />
          </View>
          <Text style={styles.label}>Complemento (Opcional)</Text>
          <View style={styles.fieldContainer}>
            <Input
              label='Complemento'
              value={userInputs.note.value}
              isValid={userInputs.note.isValid}
              textInputConfig={{
                onChangeText: noteChangeHandler,
              }}
            />
          </View>
        </View>
      )} */}
      {/* <View style={styles.fieldContainer}>
        <Input
          label='Cidade'
          value={userInputs.city.value}
          isValid={userInputs.city.isValid}
          textInputConfig={{
            onChangeText: cityChangeHandler,
          }}
        />
      </View> */}
      <View style={styles.checkboxContainer}>
        <Checkbox
          style={styles.checkbox}
          value={userInputs.elo.value}
          onValueChange={() => eloChangeHandler()}
          color={
            userInputs.elo.value ? GlobalStyles.colors.secondary : undefined
          }
        />
        <Label3 style={styles.label}>Consumidor Elo</Label3>
        <Pressable>
          <Popup message='Ser um Cliente Elo é se comprometer a fazer o pedido toda semana e ter a vantagem de escolher um item a mais sem sua cesta pagando o mesmo valor de quem não é Cliente Elo' />
        </Pressable>
      </View>
      <View style={styles.checkboxContainer}>
        <Checkbox
          style={styles.checkbox}
          value={userInputs.notificationToken.value ? true : false}
          // value={false}
          isValid={userInputs.notificationToken.isValid}
          onValueChange={() => handleCheckNotificationToken()}
          color={
            userInputs.notificationToken.value
              ? GlobalStyles.colors.secondary
              : undefined
          }
        />
        <Label3 style={styles.label}>Permite notificação</Label3>
        <Pressable>
          <Popup message='Ao clicar você receberá uma notificação no celular toda vez que uma nova entrega for agendada.' />
        </Pressable>
      </View>
      <View style={styles.buttonsContainer}>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={onHandleUpdate}>Atualizar</PrimaryButton>
        </View>
        <View style={styles.buttonContainer}>
          <PrimaryButton onPress={cancelButtonHandler}>Cancel</PrimaryButton>
        </View>
      </View>
      {/* </KeyboardAvoidingView> */}
    </ScrollView>
  );
}

export default UserScreen;

export const userScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Meus Dados' />
    </View>
  ),
  // headerBackImage: () => <BackArrow />,
  // headerBackTitleVisible: false,
  // headerStyle: {
  //   backgroundColor: 'transparent',
  //   elevation: 0,
  //   shadowOpacity: 0,
  //   borderBottomWidth: 0,
  // },
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    // alignItems: 'center',
    padding: 15,
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  fieldContainer: {
    flexDirection: 'row',
  },
  label: {
    color: GlobalStyles.colors.secondary,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 5,
    // marginRight: 5,
  },
  checkbox: {
    borderColor: GlobalStyles.colors.secondary,
    borderWidth: 1,
    marginRight: 5,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 40,
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    backgroundColor: 'transparent',
    color: GlobalStyles.colors.secondary,
  },
  addressContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
