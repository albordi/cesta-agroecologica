import { useContext, useCallback, useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import {
  View,
  ScrollView,
  Text,
  FlatList,
  Pressable,
  StyleSheet,
  Platform
} from 'react-native';
import { UserContext } from '../../store/context/usercontext';
import { GlobalStyles } from '../../constants/styles';
import UserCard from '../../components/UserCard';
import { Label2 } from '../../components/UI/Labels';
import Spinner from '../../components/Spinner';
import HeaderTitle from '../../components/HeaderTitle';

function UsersScreen({ navigation }) {
  console.log('[UsersScreen rendering...]');
  const [isLoading, setIsLoading] = useState(false);

  const usersCtx = useContext(UserContext);

  useFocusEffect(
    useCallback(() => {
      setIsLoading(true);
      usersCtx.fetchUsers();
      setIsLoading(false);
    }, [])
  );

  function onUpdateUserHandle(user) {
    console.log('[Users Screen] handle update user', user);
    navigation.navigate('UserScreen', { user: user });
  }

  async function onDeleteUserHandle(userId) {
    try {
      setIsUpdating(true);
      await usersCtx.deleteUser(userId);
      console.log('[UsersScreen] User Deleted', userId);
    } catch (error) {
      console.log('[UsersScreen] Error to delete user', userId);
    }
    setIsUpdating(false);
    console.log('[Users Screen] handle delete user');
    // navigation.navigate('UserScreen', {user: user})
  }

  function onUpdateUserHandle(user) {
    console.log('[Users Screen] handle update user');
    navigation.navigate('UserScreen', { user: user });
  }

  if (isLoading) {
    console.log('[OrderScreen] isLoading...');
    return <Spinner />;
  }

  function renderUser(itemData) {
    // console.log('item', itemData);
    return (
      <UserCard
        style={styles.userContainer}
        user={itemData.item}
        onDeleteUserHandle={onDeleteUserHandle}
        onUpdateUserHandle={onUpdateUserHandle}
      />
    );
  }

  return (
    <View style={styles.screenContainer}>
      <Label2>Clique sobre a pessoa consumidora para alterar</Label2>

      <FlatList
        style={styles.flatListContainer}
        data={usersCtx.users}
        renderItem={renderUser}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

export const usersScreenOptions = {
  headerTitle: () => (
    <View style={styles.header}>
      <HeaderTitle title='Gerenciar Usuários' />
    </View>
  ),
};

export default UsersScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    paddingTop: 30,
    backgroundColor: GlobalStyles.colors.backGroundColor,
  },
  userContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginVertical: 4,
    paddingVertical: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  flatListContainer: {
    flexGrow: 1,
  },
});
