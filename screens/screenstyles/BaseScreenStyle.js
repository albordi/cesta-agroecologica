import { GlobalStyles } from '../../constants/styles';

export const baseScreenStyle = {
  flex: 1,
  marginTop: 4,
  backgroundColor: GlobalStyles.colors.backGroundColor,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
  shadowColor: 'black',
  shadowOpacity: 0.26,
  shadowOffset: { width: 4, height: -3 },
  shadowRadius: 8,
  elevation: 25,
};
