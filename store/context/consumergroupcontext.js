import { useState, useEffect, createContext } from 'react';
import {
  getCollection,
  updateDocument
} from '../../util/firebase';

export const ConsumerGroupContext = createContext({
  consumerGroup: {},
});

function ConsumerGroupContextProvider({ children }) {
  const [consumerGroup, setConsumerGroup] = useState({});
  const [error, setError] = useState(null);

  const getConsumerGroup = async () => {
    try {
      const consumerGroupAux = await getCollection('consumergroup');
      // console.log('Consumer Group Aux', JSON.stringify(consumerGroupAux[0], null, 2));
      setConsumerGroup({
        id: consumerGroupAux[0].id,
        information: consumerGroupAux[0].information,
        message: consumerGroupAux[0].message,
        normalBasketPrice: consumerGroupAux[0].normalBasketPrice.toFixed(2),
        littleBasketPrice: consumerGroupAux[0].littleBasketPrice.toFixed(2),
        specialBasketPrice: consumerGroupAux[0].specialBasketPrice.toFixed(2),
        donationBasketPrice: consumerGroupAux[0].donationBasketPrice.toFixed(2),
        quantityLittleBasket: consumerGroupAux[0].quantityLittleBasket,
        quantityNormalBasket: consumerGroupAux[0].quantityNormalBasket,
        images: consumerGroupAux[0].images,
      });
    } catch (error) { 
      console.log(error);
      setError('Houve um erro ao carregar o grupo de consumo !!!');
    }
  };

  const updateConsumerGroup = async (consumerGroupId, consumerGroupData) => {
    // console.log('[Consumer Group Context] update consumer group', JSON.stringify(consumerGroupData,null,2));
    try {
      updateDocument('consumergroup', consumerGroupId, {
        ...consumerGroupData,
        updatedAt: new Date().toString(),
      });
      await getConsumerGroup();
    } catch (e) {
      console.log('Erro ao atualizar o grupo de consumo', e);
    }
  }

  useEffect(() => {
    getConsumerGroup();
  }, []);

  const value = {
    consumerGroup: consumerGroup,
    getConsumerGroup: getConsumerGroup,
    updateConsumerGroup: updateConsumerGroup
  };

  return (
    <ConsumerGroupContext.Provider value={value}>
      {children}
    </ConsumerGroupContext.Provider>
  );
}

export default ConsumerGroupContextProvider;
