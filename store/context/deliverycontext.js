import { useState, useEffect, createContext, Alert } from 'react';
import {
  addDocument,
  getDocument,
  updateDocument,
  deleteDocument,
  getCollection,
  getLastDocument,
} from '../../util/firebase';
import { isValidDate } from '../../util/validation';
import { id } from 'date-fns/locale';

export const DeliveryContext = createContext({
  id: '',
  deliveries: [],
  addDelivery: (delivery) => {},
  removeDelivery: (id) => {},
  updateDelivery: (id, delivery) => {},
  getLastDelivery: () => {},
});
/*
    id: '',
    message: '',
    date: null,
    products: [],
    // normalBasketProducts: [],
    donationBasketProducts: [],
    specialBasketProducts: [],
    extraProducts: [],
    productInAbundance: '',
*/

function DeliveryContextProvider({ children }) {
  console.log('[delivery context] started');
  const [isLoading, setIsLoading] = useState(true);
  const [isStartingNewDelivery, setIsStartingNewDelivery] = useState(true);
  const [error, setError] = useState(null);
  const [deliveries, setDeliveries] = useState([]);
  const [delivery, setDelivery] = useState({});
  const [nextDelivery, setNextDelivery] = useState();

  useEffect(() => {
    getDeliveries();
  }, []);

  // const setCurrentDelivery = async (deliveryId) => {
  //   const deliveryAux = deliveries.find(delivery => delivery.id === deliveryId);
  //   setDelivery(deliveryAux);
  // }

  async function startNewDelivery() {
    console.log('[deliverycontext] Starting new delivery');
    setIsStartingNewDelivery(true);
    try {
      const products = await getCollection('products');
      const newProducts = [];
      products.map((product) => {
        newProduct = {
          id: product.id,
          name: product.name,
          description: product.description,
          imageUrl: product.imageUrl,
          isNew: product.isNew,
          isVegan: product.isVegan,
          isGlutenFree: product.isGlutenFree,
          isProcessed: product.isProcessed || false,
          price: product.price,
          availableProducts: 40,
        };
        newProducts.push(newProduct);
      });
      // console.log('[deliverycontext] Starting new delivery', JSON.stringify(newProducts, null, 1));
      const currentDate = new Date();
      const dateToOrder = currentDate.setDate(currentDate.getDate() + 4);
      const deliveryDate = currentDate.setDate(currentDate.getDate() + 2);

      //Populate the extraProducts array
      const extraProductsAux = [];
      newProducts.map((product) => {
        extraProductsAux.push(product.id);
      });
      // console.log('==>', JSON.stringify(extraProductsAux, null, 2));
      newDelivery = {
        message: '',
        donationBasketMessage: '',
        specialBasketMessage: '',
        date: currentDate.toString(),
        dateToOrder: currentDate.toString(),
        products: newProducts,
        baseBasketProducts: [],
        donationBasketProducts: [],
        specialBasketProducts: [],
        extraProducts: extraProductsAux,
        productInAbundance: { id: '', name: '' },
      };
      setDelivery(newDelivery);
      setIsStartingNewDelivery(false);
    } catch (error) {
      // console.log('[Create Delivery Screen] error ', error);
      setError('Houve um erro ao carregar os produtos !!!');
      setIsStartingNewDelivery(false);
    }
  }

  async function getDelivery(deliveryId) {
    console.log('[delivery context] Get Delivery to Update', deliveryId);
    setIsStartingNewDelivery(true);
    const delivery = await getDocument('deliveries', deliveryId);
    const products = await getCollection('products');
    products.map((product) => {
      if (delivery.products.find((item) => item.id === product.id)) {
        return;
      }
      newProduct = {
        id: product.id,
        name: product.name,
        // description: product.description,
        imageUrl: product.imageUrl,
        price: product.price,
        availableProducts: 0,
      };
      delivery.products.push(newProduct);
    });

    const deliveryToUpdate = {
      id: deliveryId,
      message: delivery.message,
      donationBasketMessage: delivery.donationBasketMessage,
      specialBasketMessage: delivery.specialBasketMessage,
      date: delivery.date,
      dateToOrder: delivery.dateToOrder,
      products: delivery.products,
      baseBasketProducts: delivery.baseBasketProducts,
      donationBasketProducts: delivery.donationBasketProducts,
      specialBasketProducts: delivery.specialBasketProducts,
      extraProducts: delivery.extraProducts,
      productInAbundance: delivery.productInAbundance,
    };
    setDelivery(deliveryToUpdate);
    setIsStartingNewDelivery(false);
    console.log('[delivery context] Finished get the delivery to update');
    // console.log('[Delivery Context] delivery get from firebase', JSON.stringify(deliveryToUpdate, null, 2));
  }

  async function getDeliveries() {
    // console.log('[Delivery Context] getDeliveries');
    setIsLoading(true);
    const deliveriesAux = [];
    const deliveriesFromFirebase = await getCollection('deliveries');
    // console.log('[Delivery Context] getDeliveries deliveriesFromFirebase', deliveriesFromFirebase);

    setDeliveries(deliveriesFromFirebase);
    const currentDate = new Date();
    const nextDeliveryAux = deliveriesFromFirebase.filter((item) => {
      // Assuming the date property in the object is named 'date'
      const itemDate = new Date(item.date);
      return itemDate > currentDate;
    });
    console.log(
      '[Delivery Context] getDeliveries deliveriesFromFirebase',
      nextDeliveryAux.length
    );
    if (nextDeliveryAux.length > 0) {
      setNextDelivery(nextDeliveryAux[0]);
    }
    // console.log('[Delivery Context] getDeliveries next delivery', nextDelivery);
    setIsLoading(false);
  }

  function validateDelivery() {
    console.log('[Delivery Context] Validate Delivery');
    const isDeliveryDateValid = delivery.date !== '';
    const isProductInAbundanceValid =
      delivery.productInAbundance.name !== '' ? true : false;
    const isDonationBasketValid = delivery.donationBasketProducts.length > 0;
    let error = [];
    !isDeliveryDateValid ? error.push('Data de entrega é inválida.') : null;
    !isProductInAbundanceValid ? error.push('Preencha o campo Produto em Abundância.') : null;
    !isDonationBasketValid ? error.push('Cesta de doação é obrigatória.') : null;
    // console.log(isProductInAbundanceValid);

    // if (
    //   !isDeliveryDateValid ||
    //   !isProductInAbundanceValid ||
    //   !isDonationBasketValid
    // ) {
    //   console.log('[delivery context] delivery not valid');
    //   // Alert.alert(
    //   //   'ERRO',
    //   //   'Erro ao criar a nova entrega! Verifique se os dados estão corretos.',
    //   // );
    // }
    // console.log(isDeliveryDateValid && isProductInAbundanceValid && isDonationBasketValid);
    return error;
  }

  async function addDelivery() {
    console.log('[Delivery Context] add delivery to the database');
    const error = validateDelivery();
    if (error.length > 0) {
      console.log('basket not valid');
      return error;
    }
    try {
      //Remove products with 0 availability
      const newProducts = delivery.products.filter(
        (product) => product.availableProducts > 0
      );
      const newDelivery = { ...delivery, products: newProducts };
      console.log('[Delivery Context] newDelivery', JSON.stringify(newDelivery, null, 2));
      newDelivery.createdAt = new Date().toString();
      const id = await addDocument('deliveries', newDelivery);
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
      setError('Ocorreu um erro inesperado', e);
    }
    return [];
  }

  async function updateDelivery(deliveryId) {
    console.log('[Delivery Context] ***UPDATE*** delivery to the database');
    const error = validateDelivery();
    if (error.length > 0) {
      console.log('basket not valid');
      return error;
    }
    try {
      //Remove products with 0 availability
      const newProducts = delivery.products.filter(
        (product) => product.availableProducts > 0
      );
      const updatedDelivery = { ...delivery, products: newProducts };
      updatedDelivery.updateAt = new Date().toString();

      const id = await updateDocument(
        'deliveries',
        deliveryId,
        updatedDelivery
      );
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
      setError('Ocorreu um erro inesperado', e);
    }
    return [];
  }

  async function removeDelivery(deliveryId) {
    console.log('[Delivery Context] ***REMOVE*** delivery from the database');
    try {
      const id = await deleteDocument('deliveries', deliveryId);
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
      setError('Ocorreu um erro inesperado', e);
    }
    return;
  }

  // function setDonationBasketMessage(message) {
  //   const newDelivery = {
  //     ...delivery,
  //     donationBasketMessage: message,
  //   };
  //   setDelivery(newDelivery);
  // }

  // function setSpecialBasketMessage(message) {
  //   const newDelivery = {
  //     ...delivery,
  //     specialBasketMessage: message,
  //   };
  //   setDelivery(newDelivery);
  // }

  // function setDeliveryDate(date) {
  //   // console.log('delivery received on delivery context', date);const dateString = "dd/mm/yyyy";
  //   const dateParts = date.split('/');
  //   const deliveryDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
  //   const newDelivery = { ...delivery, date: deliveryDate };
  //   setDelivery(newDelivery);
  //   // console.log('[Delivery Context] delivery', delivery);
  // }

  // function setProductInAbundance(product) {
  //   const newDelivery = { ...delivery, productInAbundance: product };
  //   setDelivery(newDelivery);
  //   // console.log('[Delivery Context] delivery', JSON.stringify(delivery));
  // }
  function setMaxProd(productId, value) {
    //This function includes the product in the delivery object.
    console.log('[Delivery Context] setting maxprod');
    // console.log(
    //   '[Delivery Context] delivery before change',
    //   JSON.stringify(delivery, null, 2)
    // );
    let index = delivery.products.findIndex((obj) => obj.id === productId);
    const newArray = delivery.products.map((item, i) => {
      if (i === index) {
        item = { ...item, availableProducts: value };
      }
      return item;
    });
    //Remove os produtos que já foram adicionados nas cestas mas que voltaram a ter número de produtos disponíveis 0
    let changeToZero = false;
    let newDelivery = { ...delivery };
    if (value == 0) {
      changeToZero = true;
      //Remove from donation, special and extra
      let ind = delivery.donationBasketProducts.findIndex(
        (prodId) => prodId === productId
      );
      if (ind >= 0) {
        console.log('Encontrei o produto na donation');
        newDonationBasketProducts = [...newDelivery.donationBasketProducts];
        newDonationBasketProducts.splice(ind, 1);
        newDelivery = {
          ...newDelivery,
          donationBasketProducts: newDonationBasketProducts,
        };
      }
      ind = delivery.specialBasketProducts.findIndex(
        (prodId) => prodId === productId
      );
      if (ind >= 0) {
        console.log('Encontrei o produto na special');
        newSpecialnBasketProducts = [...newDelivery.specialBasketProducts];
        newSpecialBasketProducts.splice(ind, 1);
        newDelivery = {
          ...newDelivery,
          specialBasketProducts: newSpecialBasketProducts,
        };
      }
      ind = delivery.extraProducts.findIndex((prodId) => prodId === productId);
      if (ind >= 0) {
        console.log('Encontrei o produto na extra');
        newExtraProducts = [...newDelivery.extraProducts];
        newExtraProducts.splice(ind, 1);
        newDelivery = {
          ...newDelivery,
          extraProducts: newExtraProducts,
        };
      }
    }
    if (changeToZero) {
      newDelivery = { ...newDelivery, products: newArray };
      setDelivery(newDelivery);
    } else {
      // const extraProductsAux = [];
      // newArray.map((product) => {
      //   extraProductsAux.push(product.id)
      // });
      // console.log('==>',JSON.stringify(extraProductsAux, null, 2));
      setDelivery({
        ...delivery,
        products: newArray,
      });
    }
    // console.log(
    //   '[Delivery Context] delivery after change',
    //   JSON.stringify(delivery, null, 2)
    // );
  }

  function setBasketProduct(basket, productId) {
    // console.log(
    //   '[Delivery Context] delivery set basket product',
    //   JSON.stringify(delivery, null, 2)
    // );
    // console.log(basket, productId);
    if (basket === 'base') {
      // const isIncluded = delivery.donationBasketProducts.includes(productId);
      const newBaseBasketProducts = [...delivery.baseBasketProducts];
      const index = newBaseBasketProducts.findIndex(
        (item) => item === productId
      );
      if (index >= 0) {
        // console.log('Product exists======================');
        console.log(newBaseBasketProducts);
        newBaseBasketProducts.splice(index, 1);
        console.log(newBaseBasketProducts);
      } else {
        console.log('Product does not exists');
        newBaseBasketProducts.push(productId);
      }
      setDelivery({
        ...delivery,
        baseBasketProducts: newBaseBasketProducts,
      });
    }
    if (basket === 'donation') {
      // const isIncluded = delivery.donationBasketProducts.includes(productId);
      const newDonationBasketProducts = [...delivery.donationBasketProducts];
      const index = newDonationBasketProducts.findIndex(
        (item) => item === productId
      );
      if (index >= 0) {
        // console.log('Product exists======================');
        console.log(newDonationBasketProducts);
        newDonationBasketProducts.splice(index, 1);
        console.log(newDonationBasketProducts);
      } else {
        console.log('Product does not exists');
        newDonationBasketProducts.push(productId);
      }
      setDelivery({
        ...delivery,
        donationBasketProducts: newDonationBasketProducts,
      });
    }
    if (basket === 'special') {
      // const isIncluded = delivery.donationBasketProducts.includes(productId);
      const newSpecialBasketProducts = [...delivery.specialBasketProducts];
      const index = newSpecialBasketProducts.findIndex(
        (item) => item === productId
      );
      console.log('index', index);
      if (index >= 0) {
        // console.log('Product exists======================');
        console.log(newSpecialBasketProducts);
        newSpecialBasketProducts.splice(index, 1);
        console.log(newSpecialBasketProducts);
      } else {
        console.log('Product does not exists');
        newSpecialBasketProducts.push(productId);
      }
      setDelivery({
        ...delivery,
        specialBasketProducts: newSpecialBasketProducts,
      });
    }
    if (basket === 'extraproduct') {
      // const isIncluded = delivery.donationBasketProducts.includes(productId);
      const newExtraProducts = [...delivery.extraProducts];
      const index = newExtraProducts.findIndex((item) => item === productId);
      console.log('index', index);
      if (index >= 0) {
        // console.log('Product exists======================');
        console.log(newExtraProducts);
        newExtraProducts.splice(index, 1);
        console.log(newExtraProducts);
      } else {
        console.log('Product does not exists');
        newExtraProducts.push(productId);
      }
      setDelivery({
        ...delivery,
        extraProducts: newExtraProducts,
      });
    }
  }

  const value = {
    delivery: delivery,
    deliveries: deliveries,
    nextDelivery: nextDelivery,
    isLoading: isLoading,
    isStartingNewDelivery: isStartingNewDelivery,
    startNewDelivery: startNewDelivery,
    addDelivery: addDelivery,
    updateDelivery: updateDelivery,
    getDelivery: getDelivery,
    removeDelivery: removeDelivery,

    setDelivery: setDelivery,

    // removeProduct: removeDelivery,
    getDeliveries: getDeliveries,
    // updateProduct: updateDelivery,
    // setDonationBasketMessage: setDonationBasketMessage,
    // setSpecialBasketMessage: setSpecialBasketMessage,
    // setDeliveryDate: setDeliveryDate,
    // setProductInAbundance: setProductInAbundance,
    setMaxProd: setMaxProd,
    setBasketProduct: setBasketProduct,
  };

  return (
    <DeliveryContext.Provider value={value}>
      {children}
    </DeliveryContext.Provider>
  );
}

export default DeliveryContextProvider;
