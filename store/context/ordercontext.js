import { useState, createContext, useContext, useEffect } from 'react';
import {
  collection,
  query,
  where,
  getDocs,
  runTransaction,
  doc,
  writeBatch,
} from 'firebase/firestore';

import {
  addDocument,
  updateDocument,
  deleteDocument,
  getCollection,
  getDocsByField,
  getDocsByFields,
} from '../../util/firebase';
// import { getAuth } from 'firebase/auth';
import { db } from './../../firebaseConfig';

/*
order = {
  id : id,
  consumerId: id,
  consumerName: name,
  normalBasketProducts : [],
  littleBasketProducts : [],
  specialBasket : true/false,
  extraProducts: [],
  totalPrice: 99.99
}
*/

export const OrderContext = createContext({
  order: {},
});

function OrderContextProvider({ children }) {
  const [order, setOrder] = useState({});
  const [orders, setOrders] = useState([]);
  const [error, setError] = useState();

  const startNewOrder = (deliveryId, userId) => {
    setOrder({
      deliveryId: deliveryId,
      userId: userId,
      normalBasketProducts: [],
      littleBasketProducts: [],
      extraProducts: [],
      specialBasket: false,
      totalPrice: 0,
    });
  };

 
  async function getOrder(deliveryId, userId) {
    console.log('[Order Context] get user order of a delivery');
    try {
      const orderFromFirebase = await getDocsByFields(
        'orders',
        'deliveryId',
        deliveryId,
        'userId',
        userId
      );
      // console.log('[Order Context] order from firebase', orderFromFirebase);
      if (orderFromFirebase.length > 0) {
        setOrder(orderFromFirebase[0]);
      } else {
        setOrder({});
      }
      return orderFromFirebase;
    } catch (error) {
      console.log('Ocorreu um erro inesperado', error);
      setError('Ocorreu um erro inesperado', error);
      return error;
    }
    return;
  }

  async function createOrderAndUpdateDelivery(
    orderData,
    deliveryId,
    deliveryUpdateData
  ) {
    orderData.createdAt = new Date().toString();

    // Inicia um novo batch
    const batch = writeBatch(db);

    // Referência ao novo documento na coleção 'orders' (ID automático)
    const orderRef = doc(collection(db, 'orders'));

    // Adiciona a operação de gravação do novo documento em 'orders'
    batch.set(orderRef, orderData);

    // Referência ao documento específico na coleção 'deliveries' que será atualizado
    const deliveryRef = doc(db, 'deliveries', deliveryId);

    // Adiciona a operação de atualização no documento da coleção 'deliveries'
    batch.update(deliveryRef, deliveryUpdateData);

    // Executa o batch
    try {
      await batch.commit();
      console.log('Batch operation completed successfully.');
    } catch (error) {
      console.error('Error executing batch operation: ', error);
    }
  }

  // This function were used before run transaction.
  // async function addOrder(order, deliveryId, delivery) {
  //   console.log('[Order Context] add order to the database');
  //   order.createdAt = new Date().toString();

  //   if (!validateOrder()) {
  //     console.log('Order not valid');
  //     const error =
  //       'Problema ao criar o novo pedido. Verifique se os dados estão corretos!';
  //     return error;
  //   }

  //   try {
  //     const batch = writeBatch(db);
  //     const ordersRef = doc(db, "orders", "NYC");
  //     const docRef = await addDoc(collection(db, collectionToStore), document);
  //     console.log('Document written with ID: ', docRef.id);

  //   } catch (e) {
  //     console.log('Ocorreu um erro inesperado', e);
  //     setError('Ocorreu um erro inesperado', e);
  //     throw e;
  //   }
  // }

  // async function addOrder(order, deliveryId, delivery) {
  //   console.log('[Order Context] add order to the database');
  //   // console.log('[Order context] order to add]', JSON.stringify(order, null, 2));
  //   order.createdAt = new Date().toString();
  //   if (!validateOrder()) {
  //     console.log('Order not valid');
  //     const error =
  //       'Problema ao criar o novo pedido. Verifique se os dados estão corretos!';
  //     return error;
  //   }
  //   try {

  //     const id = await addDocument('orders', order);
  //   } catch (e) {
  //     console.log('Ocorreu um erro inesperado', e);
  //     setError('Ocorreu um erro inesperado', e);
  //   }
  //   return;
  // }

  async function removeOrder(orderId, delivery) {
    console.log(
      '[Order Context] ***REMOVE*** order from the database',
      orderId
    );

    // Start a new batch
    const batch = writeBatch(db);
    const orderRef = doc(db, 'orders', orderId);
    const deliveryRef = doc(db, 'deliveries', delivery.id);

    // Remove the order document
    batch.delete(orderRef);


    // Adiciona a operação de atualização no documento da coleção 'deliveries'
    batch.update(deliveryRef, delivery);

    // Executa o batch
    try {
      await batch.commit();
      console.info('Batch operation completed successfully.');
    } catch (error) {
      console.error('Error executing batch operation: ', error);
    }

    //=======================

    // try {
    //   const id = await deleteDocument('orders', orderId);
    //   console.log('[Order Context] order removed.');
    // } catch (e) {
    //   console.log('Ocorreu um erro inesperado', e);
    //   setError('Ocorreu um erro inesperado', e);
    // }
    return;
  }

  async function updateOrder(orderId, order) {
    // console.log('[Order Context] ***UPDATE*** order to the database');
    // console.log(
    //   '[Order Context] ***UPDATE*** order to the database',
    //   JSON.stringify(order, null, 2)
    // );
    order.updatedAt = new Date().toString();
    try {
      const id = await updateDocument('orders', orderId, order);
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
      setError('Ocorreu um erro inesperado', e);
    }
    return;
  }

  //Get all the orders of one delivery
  async function getOrders(deliveryId) {
    const ordersAux = [];
    try {
      const ordersFromFirebase = await getDocsByField(
        'orders',
        'deliveryId',
        deliveryId
      );
      // console.log('============================');
      // console.log(JSON.stringify(ordersFromFirebase, null, 2));
      // console.log('============================');
      setOrders(ordersFromFirebase);
      return ordersFromFirebase;
    } catch (error) {
      console.log('Ocorreu um erro inesperado', error);
      setError('Ocorreu um erro inesperado', error);
      return error;
    }
    // console.log(
    //   '[Order Context] get Orders by deliveryId',
    //   JSON.stringify(ordersFromFirebase, null, 2)
    // );
  }

  // async function fetchUserOrder(userId, deliveryId) {
  //   console.log('[Order Context] userId', userId);
  //   console.log('[Order Context] deliveryId', deliveryId);
  //   orderAux = {};
  //   try {
  //     orderAux = await getDocumentByFields(
  //       'orders',
  //       'deliveryId',
  //       'userId',
  //       deliveryId,
  //       userId
  //     );
  //     // setOrder(orderAux);
  //   } catch (e) {
  //     console.log('Ocorreu um erro inesperado', e);
  //     setError('Ocorreu um erro inesperado', e);
  //   }
  //   // console.log('-----', JSON.stringify(orderAux, null, 2));
  //   return orderAux;
  // }

  function setBasketProduct(basket, productId) {
    console.log('[Order Context] order set basket product');
    console.log(basket, productId);
    if (basket === 'little') {
      const newLittleBasketProducts = [...order.littleBasketProducts];
      const index = newLittleBasketProducts.findIndex(
        (product) => product === productId
      );
      console.log('index', index);
      if (index >= 0) {
        console.log('Product exists======================');
        console.log(newLittleBasketProducts);
        newLittleBasketProducts.splice(index, 1);
        console.log(newLittleBasketProducts);
      } else {
        console.log('Product does not exists');
        newLittleBasketProducts.push(productId);
      }
      setOrder({
        ...order,
        littleBasketProducts: newLittleBasketProducts,
      });
    }
    if (basket === 'special') {
      // const isIncluded = delivery.donationBasketProducts.includes(productId);
      const newSpecialBasketProducts = [...delivery.specialBasketProducts];
      const index = newSpecialBasketProducts.findIndex(
        (item) => item === productId
      );
      console.log('index', index);
      if (index >= 0) {
        console.log('Product exists======================');
        console.log(newSpecialBasketProducts);
        newSpecialBasketProducts.splice(index, 1);
        console.log(newSpecialBasketProducts);
      } else {
        console.log('Product does not exists');
        newSpecialBasketProducts.push(productId);
      }
      setDelivery({
        ...delivery,
        specialBasketProducts: newSpecialBasketProducts,
      });
    }
    if (basket === 'extraproduct') {
      // const isIncluded = delivery.donationBasketProducts.includes(productId);
      const newExtraProducts = [...delivery.extraProducts];
      const index = newExtraProducts.findIndex((item) => item === productId);
      console.log('index', index);
      if (index >= 0) {
        console.log('Product exists======================');
        console.log(newExtraProducts);
        newExtraProducts.splice(index, 1);
        console.log(newExtraProducts);
      } else {
        console.log('Product does not exists');
        newExtraProducts.push(productId);
      }
      setDelivery({
        ...delivery,
        extraProducts: newExtraProducts,
      });
    }
  }

  const value = {
    order: order,
    orders: orders,
    // addOrder: addOrder,
    createOrderAndUpdateDelivery: createOrderAndUpdateDelivery,
    removeOrder: removeOrder,
    updateOrder: updateOrder,
    startNewOrder: startNewOrder,
    getOrders: getOrders,
    getOrder,
    // fetchUserOrder: fetchUserOrder,

    // setBasketProduct: setBasketProduct,
  };

  return (
    <OrderContext.Provider value={value}>{children}</OrderContext.Provider>
  );
}

export default OrderContextProvider;
