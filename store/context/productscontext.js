import { createContext, useReducer } from 'react';

// const PRODUCTS = [
//   {
//     id: 'p1',
//     name: 'Cebolinha',
//     description: '',
//     price: 3.00,
//     imageUrl:
//       'https://static.itatiaia.com.br/admin/Conteudo/noticias/151716/original/cebolinha23-10-2020_.jpg',
//   },
//   {
//     id: 'p2',
//     name: 'Salsinha',
//     description: '',
//     price: 3.00,
//     imageUrl:
//       'https://st.depositphotos.com/1100313/1228/i/600/depositphotos_12282066-stock-photo-green-parsley.jpg',
//   },
// {
//   id: 'p3',
//   name: 'Tomate',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p4',
//   name: 'Tomate',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p5',
//   name: 'Tomate',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p6',
//   name: 'Tomate',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p7',
//   name: 'Tomate',
//   description: '',
//   price: 7.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p8',
//   name: 'Tomate',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p9',
//   name: 'Cenoura',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://www.maisquitanda.com.br/image/cache/1-verduras-legumes/cenoura%20rama-800x800.png',
// },
// {
//   id: 'p10',
//   name: 'Tomate',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p11',
//   name: 'Cenoura',
//   description: '',
//   price: 6.00,
//   imageUrl:
//     'https://www.maisquitanda.com.br/image/cache/1-verduras-legumes/cenoura%20rama-800x800.png',
// },
// {
//   id: 'p12',
//   name: 'Tomate',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://img.freepik.com/fotos-premium/tomate-vermelho-com-corte-isolado-no-branco_80510-576.jpg',
// },
// {
//   id: 'p13',
//   name: 'Cenoura',
//   description: '',
//   price: 3.00,
//   imageUrl:
//     'https://www.maisquitanda.com.br/image/cache/1-verduras-legumes/cenoura%20rama-800x800.png',
// },
// ];

export const ProductsContext = createContext({
  products: [],
  setProducts: (products) => {},
  addProduct: ({ name, description, price, imageUrl }) => {},
  removeProduct: (id) => {},
  updateProduct: (id, { name, description, price, imageUrl }) => {},
});

function productsReducer(state, action) {
  switch (action.type) {
    case 'SET':
      // console.log('[Products Context] Set', action);
      const productsSorted = action.payload.sort((a, b) => {
        let fa = a.name.toLowerCase(),
          fb = b.name.toLowerCase();

        if (fa < fb) {
          return 11;
        }
        if (fa > fb) {
          return -1;
        }
        return 0;
      });
      return productsSorted;
    case 'ADD':
      return [{ ...action.payload }, ...state];
    case 'UPDATE':
      const updatableProductIndex = state.findIndex(
        (product) => product.id === action.payload.id
      );
      const updatableProduct = state[updatableProductIndex];
      const updateItem = { ...updatableProduct, ...action.payload.data };
      const updatedProducts = [...state];
      updatedProducts[updatableProductIndex] = updateItem;
      return updatedProducts;
    case 'REMOVE':
      return state.filter((product) => product.id !== action.payload);
    default:
      return state;
  }
}

function ProductsContextProvider({ children }) {
  const [productsState, dispatch] = useReducer(productsReducer, []);

  function setProducts(products) {
    dispatch({ type: 'SET', payload: products });
  }

  function addProduct(product) {
    console.log('[Product Context - add new product]', product);
    dispatch({ type: 'ADD', payload: product });
  }

  function removeProduct(id) {
    dispatch({ type: 'REMOVE', payload: id });
  }

  function updateProduct(id, product) {
    dispatch({ type: 'UPDATE', payload: { id: id, data: product } });
  }

  const value = {
    products: productsState,
    setProducts: setProducts,
    addProduct: addProduct,
    removeProduct: removeProduct,
    updateProduct: updateProduct,
  };

  return (
    <ProductsContext.Provider value={value}>
      {children}
    </ProductsContext.Provider>
  );
}

export default ProductsContextProvider;
