import { useState, createContext } from 'react';
import { Platform } from 'react-native';
import * as Notifications from 'expo-notifications';
import * as Device from 'expo-device';
import Constants from 'expo-constants';

// import { collection, query, where, getDocs } from 'firebase/firestore';
import {
  getDocument,
  deleteDocument,
  getCollection,
  updateDocument,
} from '../../util/firebase';

export const UserContext = createContext();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

// export const UserContext = createContext({
//   user: {},
//   users: [],
//   fetchUser: (id) => {},
//   fetchUsers: () => {},
//   createUser: (user) => {},
//   updateUser: (id, user) => {},
//   deleteUser: (id) => {},
// });

function UserContextProvider({ children }) {
  console.log('[User Context Provider] started');
  const [user, setUser] = useState({});
  const [users, setUsers] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  async function deleteUser(userId) {
    try {
      const response = await deleteDocument('users', userId);
      fetchUsers();
    } catch (error) {
      console.log('[User context] delete user error', error);
    }
  }

  async function fetchUser(userId) {
    console.log('[User Context] starting fetch user');
    try {
      let user = await getDocument('users', userId);
      // console.log('===', user);
      user = { ...user, id: userId };
      setUser(user);
    } catch (error) {
      console.log('[User Context] fetch User - error', error);
    }
  }

  async function getUserData(userId) {
    // console.log('[User Context] starting getting userName');
    try {
      const user = await getDocument('users', userId);
      console.log('\x1b[31m%s\x1b[0m','[User Context] getuser', user);

      return user;
    } catch (error) {
      console.log('\x1b[31m%s\x1b[0m','[User Context] getuser - error', error);
    }
  }

  async function clearUser() {
    console.log('[User Context] starting to clear user');
    setUser(null);
  }

  async function fetchUsers() {
    console.log('[User Context] fetch users');
    try {
      const usersAux = await getCollection('users');
      // console.log('[User context] users (response)', JSON.stringify(usersAux, null, 2));
      setUsers(usersAux);
      return usersAux;
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
    }
  }

  async function updateUser(userId, user) {
    console.log('[User Context] update user');
    try {
      updateDocument('users', userId, {
        ...user,
        updatedAt: new Date().toString(),
      });
    } catch (e) {
      console.log('Ocorreu um erro inesperado', e);
    }
  }

  const setPushNotificationToken = async () => {
    console.log('[User Context - setPushNotification]');

    let token = { error: null, data: null };
    if (Platform.OS === 'android') {
      await Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
    if (Device.isDevice) {
      const { status: existingStatus } =
        await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        // alert('Failed to get push token for push notification!');
        console.log(
          '[User context] Failed to get push token for push notification!'
        );
        token.error = 'Failed to get push token for push notification!';
      } else {
        token = await Notifications.getExpoPushTokenAsync({
          // projectId: '8674b252-8c79-49a1-99a3-d910389040a1',
          projectId: Constants.expoConfig.extra.API_KEY,
        });
        console.log(token);
      }
    } else {
      console.log(
        '[User context] Must use physical device for Push Notifications'
      );
      token.error = 'Simuladores não funcionam para Push Notifications';
    }
    return token;
  };

  const sendNotification = async (
    pushNotificationToken,
    messageTitle,
    messageBody
  ) => {
    // console.log('Constants',JSON.stringify(Constants.expoConfig.extra.API_KEY,null, 2));

    const message = {
      to: pushNotificationToken,
      sound: 'default',
      title: messageTitle,
      body: messageBody,
    };
    console.log(
      '[User Context] sending push notification',
      JSON.stringify(message, null, 2)
    );
    try {
      await fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Accept-encoding': 'gzip, deflate',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(message),
      });
    } catch (error) {
      console.log('[User Context] erro ao enviar notificação', error);
    }
  };

  const value = {
    user: user,
    users: users,
    isLoading: isLoading,
    updateUser: updateUser,
    fetchUser: fetchUser,
    getUserData: getUserData,
    clearUser: clearUser,
    fetchUsers: fetchUsers,
    deleteUser: deleteUser,
    setPushNotificationToken: setPushNotificationToken,
    sendNotification: sendNotification,
  };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
}

export default UserContextProvider;
