import {
  collection,
  addDoc,
  getDocs,
  doc,
  deleteDoc,
  setDoc,
  getDoc,
  query,
  where,
} from 'firebase/firestore';
import { db } from '../firebaseConfig';

export async function addDocument(collectionToStore, document) {
  console.log(
    '[Firebase function storeData]',
    collectionToStore,
    JSON.stringify(document, null, 2)
  );
  try {
    const docRef = await addDoc(collection(db, collectionToStore), document);
    console.log('Document written with ID: ', docRef.id);
    return docRef.id;
  } catch (e) {
    console.log('[Firebase function storeData] Erro ao gravar o arquivo', e);
    return null;
  }
}

export async function addDocumentWithCustomId(
  collectionToStore,
  document,
  docId
) {
  console.log(
    '[Firebase function storeData]',
    collectionToStore,
    document,
    docId
  );
  await setDoc(doc(db, collectionToStore, docId), document);
  console.log(
    '[Firebase function storeData] Document written with ID: ',
    docId
  );
  return;
}

export async function getCollection(collectionToGet) {
  // console.log('[firebase util getCollection] ', collectionToGet);
  const response = [];
  const querySnapshot = await getDocs(collection(db, collectionToGet));
  querySnapshot.forEach((doc) => {
    // console.log(`[Firebase componente ]${doc.id} => ${doc.data()}`);
    response.push({ id: doc.id, ...doc.data() });
  });
  // console.log('[Firebase function getData]', JSON.stringify(response, null, 2));
  return response;
}

export async function getDocument(collectionToGet, id) {
  try {
    // console.log('[Fireabse util module] getting document', id);
    const response = await getDoc(doc(db, collectionToGet, id));
    // console.log(
    //   '[Firebase component] Document read with ID: ',
    //   response.data()
    // );
    return response.data();
  } catch (e) {
    console.error(
      'Error getting document: collection:',
      collectionToGet,
      'error:',
      e
    );
  }
}

//Get the last included document
// export async function getLastDocument(collectionToGet, date) {
//   try {
//     console.log('[Fireabse util module] getting last document', date);
//     const collectionRef = collection(db, collectionToGet);
//     const currentDate = new Date();
//     const q = query(collectionRef, where(date, '>=', currentDate));
//     // const q = query(collectionRef, where(date, '!=', null));
//     let lastDocument;
//     const response = await getDocs(q);
//     response.forEach((doc) => {
//       console.log(doc.id, ' => ', doc.data());
//       lastDocument = doc.data();
//       lastDocument.id=doc.id;
//       lastDocument.date = doc.data().date.toDate();
//     });
//     return lastDocument;

//   } catch (e) {
//     console.error('Error getting document: ', e);
//   }
// }

export async function updateDocument(collectionToUpdate, id, data) {
  try {
    const docRef = doc(db, collectionToUpdate, id);
    // await updateDoc(docRef, data);
    await setDoc(docRef, data);
    console.log('Document updated with ID: ', id);
  } catch (error) {
    console.error('Error updating document: ', error);
    return error;
  }
}

export async function deleteDocument(collectionToDelete, id) {
  try {
    await deleteDoc(doc(db, collectionToDelete, id));
    console.log('[Firebase component] Document deleted with ID: ', id);
  } catch (e) {
    console.error('Error deleting document: ', e);
  }
}

export async function getDocsByField(collectionToGet, field1, value1) {
  console.log('[Firebase - getDocsByField]', collectionToGet, field1, value1);
  try {
    const response = [];
    const colRef = collection(db, collectionToGet);
    const q = query(colRef, where(field1, '==', value1));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      // console.log(`[Firebase componente ]${doc.id} => ${doc.data()}`);
      response.push({ id: doc.id, ...doc.data() });
    });
    // console.log(
    //   '[Firebase component] Orders: ',
    //   JSON.stringify(response, null, 2)
    // );
    return response;
  } catch (e) {
    console.error(
      'Error getting document: collection:',
      collectionToGet,
      'error:',
      e
    );
  }
}

export async function getDocsByFields(
  collectionToGet,
  field1,
  value1,
  field2,
  value2
) {
  // console.log(
  //   '[Firebase - getDocsByFields]',
  //   'Collection to get:',collectionToGet,
  //   'Field 1:',field1,
  //   'Value 1',value1,
  //   'Field 2',field2,
  //   'Value 2',value2
  // );
  try {
    const response = [];
    const colRef = collection(db, collectionToGet);
    const q = query(
      colRef,
      where(field1, '==', value1),
      where(field2, '==', value2)
    );
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      // console.log(`[Firebase componente ]${doc.id} => ${doc.data()}`);
      response.push({ id: doc.id, ...doc.data() });
    });
    // console.log(
    //   '[Firebase component] Orders: ',
    //   JSON.stringify(response, null, 2)
    // );
    return response;
  } catch (e) {
    console.error(
      'Error getting document: collection:',
      collectionToGet,
      'error:',
      e
    );
    r;
  }
}

// export async function getDocumentByFields(
//   collectionToGet,
//   field1,
//   field2,
//   value1,
//   value2
// ) {
//   console.log(collectionToGet, field1, field2, value1, value2);
//   try {
//     const response = [];
//     const colRef = collection(db, collectionToGet);
//     const q = query(colRef,where(field1, '==', value1), where(field2, '==', value2));
//     const querySnapshot = await getDocs(q);
//     querySnapshot.forEach((doc) => {
//       // console.log(`[Firebase componente ]${doc.id} => ${doc.data()}`);
//       response.push({ id: doc.id, ...doc.data() });
//     });
//     // console.log(
//     //   '[Firebase component] Orders: ',
//     //   JSON.stringify(response, null, 2)
//     // );
//     return response[0];
//   } catch (e) {
//     console.error('Error getting document: ', e);
//   }
// }



