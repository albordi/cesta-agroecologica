// import * as FileSystem from 'expo-file-system'

// export const getFileInfo = async (fileURI) => {
//   const fileInfo = await FileSystem.getInfoAsync(fileURI)
//   return fileInfo
// }

export const fetchAddressByCep = async (cep) => {
  try {
    // Make a GET request to the ViaCEP API
    const response = await fetch(`https://viacep.com.br/ws/${cep}/json/`);

    if (!response.ok) {
      throw new Error('Failed to fetch address data.');
    }

    // Parse the JSON response
    const data = await response.json();

    // Check if the response has an error message
    if (data.erro) {
      throw new Error('CEP not found.');
    }
    return data;
  } catch (error) {
    console.error('Error:', error.message);
    // Handle the error (e.g., display an error message)
    return null;
  }
};

