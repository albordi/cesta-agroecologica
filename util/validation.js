export function getFormattedDate(date) {
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
}

export function getDateMinusDays(date, days) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - days);
}

export const isValidUrl = (urlString) => {
  var urlPattern = new RegExp(
    '^(https?:\\/\\/)?' + // validate protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // validate domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // validate OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // validate port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // validate query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  ); // validate fragment locator
  return !!urlPattern.test(urlString);
};

export const isValidPhoneNumber = (phoneString) => {
  console.log(phoneString);
  // var re = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
  let re = /^\s*(\d{2}|\d{0})[-. ]?(\d{5}|\d{4})[-. ]?(\d{4})[-. ]?\s*$/;

  return re.test(phoneString);
};

export const isValidEmail = (emailString) => {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailString)) {
    return true;
  }
  return false;
};

// export const isValidDate = (dateString) => {
//   console.log(dateString);
//   const dateRegex = /^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0-2])\/\d{4}$/;
//   if (!dateRegex.test(dateString)) {
//     console.log('Invalid date!');
//     return false;
//   } 
//   return true;
// };

export const isValidDate = (dateString) => {
  console.log('---',dateString);
  const regex = /^(Sun|Mon|Tue|Wed|Thu|Fri|Sat)\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s\d{1,2}\s\d{4}\s\d{2}:\d{2}:\d{2}\s(GMT|UTC)(\+|\-)\d{4}$/;
  return regex.test(dateString);
};

import GLOBALS from "../Globals";

export const validateProductsAmount = (basketType, basket, userElo, quantity) => {
  console.log('[Validation: Validation Products Amount]', JSON.stringify(basket, null, 2));
  let productsAmount = 0;
  if (basketType === GLOBALS.BASKET.TYPE.LITTLE) {
    basket.map((product) => {
      product.checked ? productsAmount++ : null;
    });
    if (
      (productsAmount != quantity && !userElo) ||
      (productsAmount != (quantity+1) && userElo)
    ) {
      return `Para cestas pequenas você deve selecionar ${quantity} produtos ou ${quantity+1} produtos se for consumidor Elo`;
    }
  }
  if (basketType === GLOBALS.BASKET.TYPE.NORMAL) {
    productsAmount = 0;
    basket.map((product) => {
      product.checked ? productsAmount++ : null;
    });
    if (
      (productsAmount != quantity && !userElo) ||
      (productsAmount != (quantity+1) && userElo)
    ) {
      return `Para cestas normais você deve selecionar ${quantity} produtos ou ${quantity+1} produtos se for consumidor Elo`;
    }
  }
  return null;
};